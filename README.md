


## bpsi-test-event-gen

Purpose to build this test pack is to help generate and test the BPSI functionality.  The main feature of this test pack is 

 - Generate mocked domain data that complies to the schema specified in BPSI. For example, legal entity, address as a domain, organization name etc.
 - Able to ingest generated data directly into a BPSI end point triggering BPSI processes to be tested.
 - Ability to generate json and xml format version of data.
 - Other testing features will be released as the BPSI adds more functions.


## Setting up

**

 1. Check if you have JDK 11.0.6 or above set as default java binary in your environment path. It can be validated by executing on cmd,

		\target>java –version
		java 11.0.7 2020-04-14 LTS
		Java(TM) SE Runtime Environment 18.9 (build 11.0.7+8-LTS)
		Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.7+8-LTS, mixed mode)

2.	Clone the repo into say ${HOME}/dev/sb/bpsi-test-gen
git clone https://{user credentials}/ bpsi-test-event-gen.git
3.	Assuming you are at the above location
cd target
**

## Usage


    $java -jar bpsi-test-event-gen.jar --filetype {json|xml} --schema {schemaId} --num {number of events} --targettype {FILE|URI} --target {File Path| URI Path}

 - --**filetype:** mandatory, type of data to be generated, json or xml structure
 - --**schema:** mandatory, schemaId that is registered and onboarded in bpsi
 - --**targettype**: mandatory,specify target type can be file or restful URI 
 - --**num:** mandatory,integer that specifies number of events to be generated
 - --**target:** mandatory, specify the target location, can be local path or URI 

**For example,**

The below command will generate for schema com.scb.bpsi.schema.cc.legalentity 5 messages as 5 separate files in specified location given by –targetloc. 

    $java –jar bpsi-test-event-gen.jar --filetype xml --schema b89DLic2mP9bzlwP --num 5 --targettype FILE --target d:\json_output

**For example,**

The below command will generate for schema com.scb.bpsi.schema.cc.legalentity and ingest it as a bpsi payload specified by –targetloc. 

    $java –jar bpsi-test-event-gen.jar --filetype json --schema b89DLic2mP9bzlwP --num 1 --targettype URI --target http://ap-southeast-1.compute.amazonaws.com:32323/ingest

**Limitations**

 - If new schema is registered in BPSI and needs testing, then the bpsi-test-event-gen requires rebuild of source with addition of the schema name into the event factory. The limitation is because domain data is configured based on registered topic in kafka schema registry.
 -  Ingestion payload and header information is subject to changes based on BPSI evolution. Until stable service contracts are available, can expect the source to require rebuilds of the test pack.
 -  It doesn't run as a service therefore more of a test data generation and ingestion features for a given num of events. 
 - Certain limitation is around hardwired models in the binary. For example, additional or modification of SchemaId, BPSI end point interface change, adding support for other filetype like csv or flat file.
