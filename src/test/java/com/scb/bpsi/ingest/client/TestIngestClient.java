package com.scb.bpsi.ingest.client;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.scb.bpsi.messagegenerator.avro.schema.util.AvroDataIO;
import com.scb.bpsi.messagegenerator.enums.FileFormatType;
import com.scb.bpsi.messagegenerator.event.EventsProductionFactory;
import com.scb.bpsi.messagegenerator.ingestgrpc.IngestGrpc;
import com.scb.bpsi.messagegenerator.ingestgrpc.PayloadGrpc;
import com.scb.bpsi.schema.cc.legalentity.v4.eventgen.EventsGeneratorv4;
import com.scb.bpsi.schema.vega.legalentitydetails.eventgen.EventsGeneratorLegalEntityDetails;
import com.scb.bpsi.schema.vt.company.eventgen.EventsGeneratorCompany;
import com.scb.bpsi.services.engines.common.DateTimeWithZone;



public class TestIngestClient {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		
		 EventsProductionFactory.register("wiqpJZEChkL5H3Hz", new EventsGeneratorCompany());

		
		 String hostname = "ec2-54-169-229-117.ap-southeast-1.compute.amazonaws.com:32253";
		 String schemaName = "wiqpJZEChkL5H3Hz";
		 String sourceIdPath = "company";
		 
		 BPSIIngestClient client = new BPSIIngestClient(hostname, "certs/aws/cert.pem");
		 System.out.println(IngestGrpc.ingestEvents(client, 1, schemaName , FileFormatType.JSON, sourceIdPath));
		 
	}
	
	
	@Test
	public void testEmptyArray() throws JsonMappingException, JsonProcessingException {
		String data = "{\"sourceId\":\"Crist, Crist and Crist\",\"numberMeta\":[],\"dateMeta\":[],\"textMeta\":[],\"intMeta\":[]}";
		//{\"value0\" : \"key0\"}
		
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode metaDataNode = mapper.readTree(data);
		
		
		List<Map<String, String>> textMeta = null;
		List<Map<String, DateTimeWithZone>> dateMeta = null;
		List<Map<String, Long>> intMeta = null;
		List<Map<String, Double>> numberMeta = null;
		
		
		textMeta =	mapper.convertValue(metaDataNode.get("textMeta"), new TypeReference<List<Map<String,String>>>(){});
		intMeta =	mapper.convertValue(metaDataNode.get("intMeta"), new TypeReference<List<Map<String,Long>>>(){});
		numberMeta =	mapper.convertValue(metaDataNode.get("numberMeta"), new TypeReference<List<Map<String,Double>>>(){});
		
		
		//TypeReference<List<Map<String, DateTimeWithZone>>> typeRef = new TypeReference<List<Map<String, DateTimeWithZone>>>() {};
		// dateMeta =	mapper.convertValue(metaDataNode.get("dateMeta"), typeRef);
		  
		System.out.println("textMeta:"+ textMeta.toString());
		System.out.println("intMeta:"+ intMeta.toString());
		System.out.println("numberMeta:"+ numberMeta.toString());
		
		}

}
