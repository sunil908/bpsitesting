import java.io.IOException;

import org.apache.log4j.PropertyConfigurator;

import com.beust.jcommander.JCommander;
import com.scb.bpsi.config.Configuration;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainExecutor {

	{
		PropertyConfigurator.configure("./log4j.properties");
		//PropertyConfigurator.configure("D:\\git\\bpsi-test-event-gen\\src\\main\\resources\\log4j.properties");
	}
	public static void main( String... argv) throws InterruptedException, IOException, RestClientException
    {
		
		String configFile = "";
		//configFile = "D:\\git\\bpsi-test-event-gen\\src\\main\\resources\\bpsi.properties";
		
		MainExecutor mainExecutor = new MainExecutor();
		MessageGenerator messageGenerator = new MessageGenerator();
		TestPack testPack = new TestPack();
        
		JCommander jc = JCommander.newBuilder()
            .addObject(mainExecutor)
            .addCommand("gen-event", messageGenerator)
            .addCommand("test-exe", testPack)
            .build();
		
		jc.parse(argv);
        

	   	log.info("\n==========================================================================================================="
	   			+ "\n===============================================New Run  ==================================================" 
	   			+ "\n===========================================================================================================");

		// Reading the configuration file
		Configuration config = Configuration.getInstance();
		
		if(!config.loadConfig(!(configFile.isEmpty())?configFile:"./bpsi.properties")) {
			System.exit(1);
			log.error("Unable to load config file.");
			log.info("Exiting");
		};
		
 	    //config.addKeyProperties("authToken", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJicHNpdXNlcjEiLCJleHAiOjE5MDc5NjA4ODR9.MJMXOaAB5QNIETRQoeSmQmAZat1EU-myjvXowqD-lJ-7ZROvqVtdi2MB1ZrE_Qg0Vc414AXlH3IZydllc4rIVQ");
		//log.info("cert Loc2:"+ config.getKeyProperties("certLoc2"));
 	    //config.addKeyProperties("certLoc", "D:\\git\\bpsi-test-event-gen\\certs-SIT\\aws\\cert.pem");
 	    
 	    
		String parsedCmdStr = jc.getParsedCommand();
		
		switch (parsedCmdStr) {
	    case "gen-event":
	    				MessageGenerator.runMessageGenerator();
	    				break;
	 
	    case "test-exe":  
	    				  TestPack.executeTestPack();
	    				  break;
	    default:
	        System.err.println("Invalid command: " + parsedCmdStr);
	}
		
    }
	
}
