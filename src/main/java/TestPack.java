import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.beust.jcommander.Parameters;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mashape.unirest.http.HttpResponse;
import com.scb.bpsi.config.*;
import com.scb.bpsi.ingest.client.BPSIIngestClient;
import com.scb.bpsi.messagegenerator.enums.TestResult;
import com.scb.bpsi.messagegenerator.enums.TestValidationType;
import com.scb.bpsi.messagegenerator.ingestgrpc.IngestGrpc;
import com.scb.bpsi.messagegenerator.ingestgrpc.PayloadGrpc;
import com.scb.bpsi.messagegenerator.ingestrest.IngestRest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Parameters(commandNames = {"executeTestPack"},
			commandDescription = "Execute all the test cases on the environment")
public class TestPack {

	private static Configuration config = 	Configuration.getInstance();
	private static JsonNode testSchemaOnboarded = null;
	private static ObjectMapper mapper = new ObjectMapper();
	
	public static Map<String, String> testResults = new HashMap<>();
	public static Map<String, TestResult> testSummary = new HashMap<>();
	
	public static void executeTestPack() throws IOException {

		log.info("\n====================================== Test Case Executor ======================================");
		
		executeTestCase(new TestPack().new TestInferSchema(), "inferSchema", config.getExeTestcase("inferSchema"));
		executeTestCase(new TestPack().new TestOnboardingByInfer(), "onboardingByInfer", config.getExeTestcase("onboardingByInfer"));
		executeTestCase(new TestPack().new TestOnboardingByDef(), "onboardingByDef", config.getExeTestcase("onboardingByDef"));

		executeTestCase(new TestPack().new TestSchemaGetByPrefix(), "schemaGetByPrefix", config.getExeTestcase("schemaGetByPrefix"));
		executeTestCase(new TestPack().new TestSchemaGetFull(), "schemaGetFull", config.getExeTestcase("schemaGetFull"));
		
		executeTestCase(new TestPack().new TestIngestSingleRest(), "ingestSingleRest", config.getExeTestcase("ingestSingleRest"));
		executeTestCase(new TestPack().new TestIngestSingleGrpc(), "ingestSingleGrpc", config.getExeTestcase("ingestSingleGrpc"));
		executeTestCase(new TestPack().new TestIngestFastSingleRest(), "ingestFast", config.getExeTestcase("ingestFast"));
		executeTestCase(new TestPack().new TestIngestBatchRest(), "ingestBatch", config.getExeTestcase("ingestBatch"));
		
		executeTestCase(new TestPack().new TestJournalByPages(), "journalByPages", config.getExeTestcase("journalByPages"));

		
		postTestEndLog();
	}
	

	public static TestResult executeTestCase(TestCase callTestCase, String testCase, Boolean flag) 
    {
		TestResult result = callTestCase.execute(testCase, flag);
		log.info("\nResult =>\n" + testResults.get(testCase));
		testSummary.put(testCase, result);
		return TestResult.SUCCESS;
    }
    

	/**
     * TestCase TC-1: Infer Schema
     *
     * 
     * @return boolean did this succeed ?
     */
    public class TestInferSchema implements TestCase {
		
    	public TestResult execute(String testCase, Boolean flag) {
			
			
			if(!flag) {
				
				log.info("Skipped testcase " + testCase);
				return TestResult.SKIPPED;
			}

			putTestStartLog("TC : => " + testCase);
			HttpResponse<String> resultString = IngestRest.bpsiRestPost(config.getUrlProperties("inferSchema"), config.getSchemaProperties("sampleData"));
			
			try {
				testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(resultString.getBody())));
			} catch (JsonProcessingException e) {
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
				return TestResult.FAILURE;
			}
			return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");
		}
	}
	
	
	/**
     * TestCase TC-2: Onboarding Schema
     *
     * 
     * @return boolean did this succeed ?
     */
	 public class TestOnboardingByInfer implements TestCase {
			
	    	public TestResult execute(String testCase, Boolean flag) {
				
				if(!flag) {
					log.info("Skipped testcase " + testCase);
					return TestResult.SKIPPED;
				}
				
				putTestStartLog("TC : => " + testCase);
				// Main Test Execution goes here
				
				ObjectMapper mapper = new ObjectMapper();
				
				ObjectNode schemaFullNode = mapper.createObjectNode();
				ObjectNode schemaCatalogNode = null;
				ArrayNode schemaUserRoles = null;

				if(!config.getSchemaProperties("schemaCatalog").isEmpty()){
					try {
						schemaCatalogNode = (ObjectNode)mapper.readTree(config.getSchemaProperties("schemaCatalog"));
					} catch (JsonProcessingException e) {
						log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
						return TestResult.FAILURE;
					}
				}
				if(!config.getSchemaProperties("userRoles").isEmpty()) {
					try {
						schemaUserRoles = (ArrayNode) mapper.readTree(config.getSchemaProperties("userRoles"));
					} catch (JsonProcessingException e) {
						log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
						return TestResult.FAILURE;
					}
				}
				
				schemaFullNode.set("schemaCatalog", schemaCatalogNode);
				schemaFullNode.putArray("userRoles").addAll(schemaUserRoles);
				schemaFullNode.put("schemaDefinition",testResults.get("inferSchema"));
				schemaFullNode.put("schemaDescription", "Got you");
				schemaFullNode.put("schemaDomainType", config.getSchemaProperties("schemaDomainType"));
				schemaFullNode.put("schemaDefinitionType", "JSON");
				schemaFullNode.put("schemaDotNotation", config.getSchemaProperties("schemaDotNotation"));
				schemaFullNode.put("schemaName", config.getSchemaProperties("schemaName"));
				
				//log.info("Schema Payload: " + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schemaFullNode));
				
				HttpResponse<String> resultString  = null;
				try {
					resultString = IngestRest.bpsiRestPut(config.getUrlProperties("onboarding"), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schemaFullNode));
					testSchemaOnboarded = new ObjectMapper().readTree(resultString.getBody());
					testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(testSchemaOnboarded));
				} catch (JsonProcessingException e) {
					log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
					return TestResult.FAILURE;
				}
				
				return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");
		}
	 }

		/**
	     * TestCase TC-2: Onboarding Schema
	     *
	     * 
	     * @return boolean did this succeed ?
	     */
		 public class TestOnboardingByDef implements TestCase {
				
		    	public TestResult execute(String testCase, Boolean flag) {
					
					if(!flag) {
						log.info("Skipped testcase " + testCase);
						return TestResult.SKIPPED;
					}
					
					putTestStartLog("TC : => " + testCase);
					// Main Test Execution goes here
					
					ObjectMapper mapper = new ObjectMapper();
					
					ObjectNode schemaFullNode = mapper.createObjectNode();
					ObjectNode schemaCatalogNode = null;
					String schemaDefinition = null;
					ArrayNode schemaUserRoles = null;

					
					if(!config.getSchemaDefProperties("schemaDefinition").isEmpty()){
						try {
							schemaDefinition = config.getSchemaDefProperties("schemaDefinition");
						} catch (Exception e) {
							log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
							return TestResult.FAILURE;
						}
					}
					if(!config.getSchemaDefProperties("schemaCatalog").isEmpty()){
						try {
							schemaCatalogNode = (ObjectNode)mapper.readTree(config.getSchemaDefProperties("schemaCatalog"));
						} catch (JsonProcessingException e) {
							log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
							return TestResult.FAILURE;
						}
					}
					if(!config.getSchemaDefProperties("userRoles").isEmpty()) {
						try {
							schemaUserRoles = (ArrayNode) mapper.readTree(config.getSchemaDefProperties("userRoles"));
						} catch (JsonProcessingException e) {
							log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
							return TestResult.FAILURE;
						}
					}
					
					schemaFullNode.set("schemaCatalog", schemaCatalogNode);
					schemaFullNode.putArray("userRoles").addAll(schemaUserRoles);
					schemaFullNode.put("schemaDefinition",schemaDefinition);
					schemaFullNode.put("schemaDescription", config.getSchemaDefProperties("schemaDescription"));
					schemaFullNode.put("schemaDomainType", config.getSchemaDefProperties("schemaDomainType"));
					schemaFullNode.put("schemaDefinitionType", "JSON");
					schemaFullNode.put("schemaDotNotation", config.getSchemaDefProperties("schemaDotNotation"));
					schemaFullNode.put("schemaName", config.getSchemaDefProperties("schemaName"));
					
					try {
						log.info("Schema Payload: " + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schemaFullNode));
					} catch (JsonProcessingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					HttpResponse<String> resultString  = null;
					try {
						resultString = IngestRest.bpsiRestPut(config.getUrlProperties("onboarding"), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schemaFullNode));
						testSchemaOnboarded = new ObjectMapper().readTree(resultString.getBody());
						testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(testSchemaOnboarded));
					} catch (JsonProcessingException e) {
						log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
						return TestResult.FAILURE;
					}
					
					return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");
			}
		 }


	/**
     * TestCase TC-2a: Schema Get by Prefix
     *
     * 
     * @return boolean did this succeed ?
	 * @throws JsonProcessingException 
	 * @throws JsonMappingException 
     */
	 public class TestSchemaGetByPrefix implements TestCase {
			
	    	public TestResult execute(String testCase, Boolean flag) {
				
				if(!flag) {
					log.info("Skipped testcase " + testCase);
					return TestResult.SKIPPED;
				}
				
			// Main Test Execution goes here
			putTestStartLog("TC : => " + testCase);

			String schemaDotNotation = testSchemaOnboarded.get("schemaRegistrySubject").asText();
			String url = config.getUrlProperties("schemaGetByPrefix") + "/" + schemaDotNotation;
			
			log.info("Url formed: " + url);
			
			
			HttpResponse<String> resultString  = IngestRest.bpsiRestGet(url);

			try {
				testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(resultString.getBody())));
			} catch (JsonMappingException e) {
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
				return TestResult.FAILURE;
			} catch (JsonProcessingException e) {
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
				return TestResult.FAILURE;
			}
			return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");
		}
	}

	/**
     * TestCase TC-2b: Schema Get By Full
     *
     * 
     * @return boolean did this succeed ?
	 * @throws JsonProcessingException 
	 * @throws JsonMappingException 
     */
	 public class TestSchemaGetFull implements TestCase {
			
	    	public TestResult execute(String testCase, Boolean flag) {
				
				if(!flag) {
					log.info("Skipped testcase " + testCase);
					return TestResult.SKIPPED;
				}
				
			// Main Test Execution goes here
			String schemaId = testSchemaOnboarded.get("generatedSchemaId").asText();
			String url = config.getUrlProperties("schemaGetByFull") + "/" + schemaId;
			
			if(schemaId.isEmpty()) {
				return TestResult.FAILURE;
			};

			putTestStartLog("TC : => " + testCase);
			
			log.info("Url formed: " + url);
			
			HttpResponse<String> resultString = IngestRest.bpsiRestGet(url);
			try {
				testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(resultString.getBody())));
			} catch (JsonProcessingException e) {
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
				return TestResult.FAILURE;
			}
			
			return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");
		}
	}
	 
	 
	/**
     * TestCase TC-3: InferSingleRestTesting Using Rest API
     *
     * 
     * @return boolean did this succeed ?
     */
	 public class TestIngestSingleRest implements TestCase {
			
	    	public TestResult execute(String testCase, Boolean flag) {
				
				if(!flag) {
					log.info("Skipped testcase " + testCase);
					return TestResult.SKIPPED;
				}
				
			putTestStartLog("TC : => " + testCase);
				
			// Main Test Execution goes here
			ObjectMapper mapper = new ObjectMapper();
			
			ObjectNode schemaFullNode = mapper.createObjectNode();
			ObjectNode metaData = null;
			

			if(!config.getingestSingleTest("metaData").isEmpty()){
				try {
					metaData = (ObjectNode)mapper.readTree(config.getingestSingleTest("metaData"));
				} catch (JsonMappingException e) {
					log.error("Unable to map the config metaData");
					log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
					return TestResult.FAILURE;
				} catch (JsonProcessingException e) {
					log.error("Unable to map the config metaData");
					log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
					return TestResult.FAILURE;
				}
			}	
			
			schemaFullNode.set("metaData", metaData);
			schemaFullNode.put("data", config.getingestSingleTest("data"));
			schemaFullNode.put("schemaId",config.getingestSingleTest("schemaId"));
			
			String payload = null;
			
			try {
				payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schemaFullNode);
			} catch (JsonProcessingException e) {
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
				return TestResult.FAILURE;
			}
			
			HttpResponse<String> resultString = IngestRest.bpsiRestPut(config.getUrlProperties("ingest"), payload);
			
			try {
				testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(resultString.getBody())));
			} catch (JsonProcessingException e) {
				log.error("Unable to parse the reponse from API request");
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
			}
		
			return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");	
		}
	}


		/**
	     * TestCase TC-XX3: InferBatchRestTesting Using Rest API
	     *
	     * 
	     * @return boolean did this succeed ?
	     */
		 public class TestIngestBatchRest implements TestCase {
				
		    	public TestResult execute(String testCase, Boolean flag) {
					
					boolean result = true;
					if(!flag) {
						log.info("Skipped testcase " + testCase);
						return TestResult.SKIPPED;
					}

					putTestStartLog("TC : => " + testCase);

				// Main Test Execution goes here
				ObjectMapper mapper = new ObjectMapper();
				
				ArrayNode events = mapper.createArrayNode();
				
				log.info("Read params:"+config.getIngestBatchRest().toString());
					config.getIngestBatchRest().stream().forEach(map -> {
					// Code for stream comes here
							ObjectNode schemaFullNode = mapper.createObjectNode();
							try {
								schemaFullNode.set("metaData",mapper.readTree(map.get("metaData")));
							} catch (JsonProcessingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							schemaFullNode.put("data", map.get("data"));
							schemaFullNode.put("schemaId",map.get("schemaId"));
							events.add(schemaFullNode);
							
					});
				
				
				
				String payload = null;
				
				try {
					payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(events);
					//log.info("Transformed batch event json=>\n" + payload);
				} catch (JsonProcessingException e) {
					log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
					return TestResult.FAILURE;
				}
				
				
				HttpResponse<String> resultString = IngestRest.bpsiRestPut(config.getUrlProperties("ingestBatch"), payload);
				try {
					testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(resultString.getBody())));
				} catch (JsonProcessingException e) {
					log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
					return TestResult.FAILURE;
				}
				
				return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");
			}
		}
	 
	 
	/**
     * TestCase TC-4: Ingest Single event using GRPC Client
     *
     * 
     * @return boolean did this succeed ?
     */
	 public class TestIngestSingleGrpc implements TestCase {
			
	    	public TestResult execute(String testCase, Boolean flag) {
				
				if(!flag) {
					log.info("Skipped testcase " + testCase);
					return TestResult.SKIPPED;
				}

				
			putTestStartLog("TC : => " + testCase);

			// Main Test Execution goes here
			AtomicInteger counter = new AtomicInteger(1);
			
			
			// "cheers"+ counter.getAndIncrement() + System.currentTimeMillis()
			PayloadGrpc payload = null;
			
			try {
				payload = new PayloadGrpc("cheers"+ counter.getAndIncrement() + System.currentTimeMillis(), 
														config.getingestSingleTest("schemaId"), 
														config.getingestSingleTest("data"), 
														config.getingestSingleTest("metaData")
													);
			} catch (JsonProcessingException e) {
				log.error(e.getStackTrace().toString());
				return TestResult.FAILURE;
			}
			catch (IllegalArgumentException e) {
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
				return TestResult.FAILURE;
			}
			
			BPSIIngestClient client = null;
			try {
				client = new BPSIIngestClient(config.getUrlProperties("grpc"), config.getKeyProperties("certLoc"));
			} catch (IOException e) {
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
				return TestResult.FAILURE;
			}
			
			String resultString = null;
			try {
				 resultString = IngestGrpc.ingestBPSI(client,payload.getEvent());
			}
			catch (Exception e) {
				log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
				return TestResult.FAILURE;
			}
			
			testResults.put(testCase,resultString);
			return TestResult.SUCCESS;
		}
	}
	 
	 
	 /**
	     * TestCase TC-5: ingestFastSingleTestRestAPI
	     *
	     * 
	     * @return boolean did this succeed ?
	     */
		 public class TestIngestFastSingleRest implements TestCase {
				
		    	public TestResult execute(String testCase, Boolean flag) {
					
					if(!flag) {
						log.info("Skipped testcase " + testCase);
						return TestResult.SKIPPED;
					}
				putTestStartLog("TC : => " + testCase);
				
				// Main Test Execution goes here
				ObjectMapper mapper = new ObjectMapper();
				
				ObjectNode schemaFullNode = mapper.createObjectNode();
				ObjectNode metaData = null;
				
					
				if(!config.getingestSingleTest("metaData").isEmpty()){
					//System.out.println("is not null");
					try {
						metaData = (ObjectNode)mapper.readTree(config.getingestSingleTest("metaData"));
					} catch (JsonMappingException e) {
						log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
						return TestResult.FAILURE;
					} catch (JsonProcessingException e) {
						log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
						return TestResult.FAILURE;
					}
				}	
				
				schemaFullNode.set("metaData", metaData);
				schemaFullNode.put("data", config.getingestSingleTest("data"));
				schemaFullNode.put("schemaId",config.getingestSingleTest("schemaId"));
				
				String payload = null;
				
				try {
					payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schemaFullNode);
				} catch (JsonProcessingException e) {
					log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
					return TestResult.FAILURE;
				}
				
				putTestStartLog("TC: [" + testCase + "] => ");
				
				HttpResponse<String> resultString = IngestRest.bpsiRestPut(config.getUrlProperties(testCase), payload);
				try {
					testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(resultString.getBody())));
				} catch (JsonProcessingException e) {
					log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
					return TestResult.FAILURE;
				}
				
				return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");
			}
		}

		 

		/**
	     * TestCase TC: Journal Pages
	     *
	     * 
	     * @return boolean did this succeed ?
	     */
	    public class TestJournalByPages implements TestCase {
			
	    	public TestResult execute(String testCase, Boolean flag) {
				
				if(!flag) {
					log.info("Skipped testcase " + testCase);
					return TestResult.SKIPPED;
				}

				putTestStartLog("TC : => " + testCase);

				HttpResponse<String> resultString = IngestRest.bpsiRestGet(config.getUrlProperties("journalByPages"));
				try {
					testResults.put(testCase, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(resultString.getBody())));
				} catch (JsonProcessingException e) {
					log.error("Unable to execute test case" + testCase + " \n" + e.getStackTrace());
					return TestResult.FAILURE;
				}
				
				return validate(resultString, "IS_NONEMPTY", "IS_HTTPOK");
			}
		}
		
	
	public static void putTestStartLog(String testCaseDetails) {
		log.info("======================================= Test Execution => " + testCaseDetails + "=============================================");
	}
	
	public static void postTestEndLog() {
		log.info("======================================= Test Execution Summary " + "=============================================");
		testSummary.forEach((k,v)->log.info("Test Case: " + k + " \t" + "Status: " + v));
	}
	

	
	/**
     * Execute the test case supplied
     *
     * 
     * @return boolean did this load succeed ?
     */
	public interface TestCase {
		
		public TestResult execute(String testCase, Boolean flag);
	}
	

	public TestResult validate(HttpResponse<String> testResult, String... argValidators) {
		
		List<String> validateRuleList = Arrays.asList(argValidators);
		
		//log.info("validateRuleList: " + validateRuleList);
		//log.info("Content:" + testResult.getBody().toString());
		
		TestResult result = TestResult.SUCCESS;
		
		for(String validateRuleItem : validateRuleList) {
			TestResult ruleResult  = ((TestValidationType.testValidationTypes.contains(validateRuleItem))? validateRule(testResult, validateRuleItem) : TestResult.SUCCESS);
			result = (!result.equals(TestResult.FAILURE) && ruleResult.equals(TestResult.FAILURE))? TestResult.FAILURE : TestResult.SUCCESS;
		}
		
		return result;
	}
		
	public TestResult validateRule(HttpResponse<String> data, String rule) {
			
		boolean ruleResult = true;
		
		if(rule.equalsIgnoreCase("IS_HTTPOK")) {
				ruleResult = !(data.getStatus() >= 200 && data.getStatus() <=299)? false : true;
			 	log.info("IS_HTTPOK validation (status=" + data.getStatus() +"): " + ruleResult);
		}
		if(rule.equalsIgnoreCase("IS_NONEMPTY")) {
			ruleResult = (!data.getBody().isEmpty()? true : false);
		 	log.info("IS_NONEMPTY validation: " + ruleResult);
		}
		if(rule.equalsIgnoreCase("IS_STATUSOK")) {
		 	try {
		 		ruleResult = ((mapper.readTree(data.getBody()).get("status")).toString().equalsIgnoreCase("success")? false : true);
			 	log.info("IS_STATUSOK validation: " + ruleResult);
			} catch (JsonProcessingException e) {
				log.error("Unable to read the response body");
				return TestResult.FAILURE;
			}
		}

		if (!ruleResult) {
			log.info("Failed Test Validation:" + rule);
			return TestResult.FAILURE;
		}
		
		return TestResult.SUCCESS;
	}
    
    
    
    
	
}



