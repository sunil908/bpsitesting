package com.scb.bpsi.schema.companies.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import com.scb.bpsi.messagegenerator.event.EventGenerator;
import com.scb.bpsi.schema.companies.*;



public class EventGenerationImpl implements EventGenerator {
	
	private Faker faker = new Faker();
	
	public test test_generateMessage()	{
		
	List<companies> records = new ArrayList<companies>();
	IntStream.range(0,(new Random()).nextInt(6)+1)
	.forEach(i -> records.add(companies_record_generateMessage()));
	
	return test.newBuilder()
			.setCompanies(records)
			.build();
}
	
	
	public companies companies_record_generateMessage() {
		
		return companies.newBuilder()
				.setCompany(faker.company().name())
				.setLocation(faker.address().country())
				.setProduct(faker.beer().name())
				.build();
	}
	
}


