/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.scb.bpsi.schema.companies;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class companies extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -1190216899481520419L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"companies\",\"namespace\":\"com.scb.bpsi.schema.companies\",\"fields\":[{\"name\":\"company\",\"type\":\"string\",\"doc\":\"\"},{\"name\":\"location\",\"type\":\"string\",\"doc\":\"\"},{\"name\":\"product\",\"type\":\"string\",\"doc\":\"\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<companies> ENCODER =
      new BinaryMessageEncoder<companies>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<companies> DECODER =
      new BinaryMessageDecoder<companies>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<companies> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<companies> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<companies> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<companies>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this companies to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a companies from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a companies instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static companies fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.lang.CharSequence company;
   private java.lang.CharSequence location;
   private java.lang.CharSequence product;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public companies() {}

  /**
   * All-args constructor.
   * @param company The new value for company
   * @param location The new value for location
   * @param product The new value for product
   */
  public companies(java.lang.CharSequence company, java.lang.CharSequence location, java.lang.CharSequence product) {
    this.company = company;
    this.location = location;
    this.product = product;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return company;
    case 1: return location;
    case 2: return product;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: company = (java.lang.CharSequence)value$; break;
    case 1: location = (java.lang.CharSequence)value$; break;
    case 2: product = (java.lang.CharSequence)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'company' field.
   * @return The value of the 'company' field.
   */
  public java.lang.CharSequence getCompany() {
    return company;
  }


  /**
   * Sets the value of the 'company' field.
   * @param value the value to set.
   */
  public void setCompany(java.lang.CharSequence value) {
    this.company = value;
  }

  /**
   * Gets the value of the 'location' field.
   * @return The value of the 'location' field.
   */
  public java.lang.CharSequence getLocation() {
    return location;
  }


  /**
   * Sets the value of the 'location' field.
   * @param value the value to set.
   */
  public void setLocation(java.lang.CharSequence value) {
    this.location = value;
  }

  /**
   * Gets the value of the 'product' field.
   * @return The value of the 'product' field.
   */
  public java.lang.CharSequence getProduct() {
    return product;
  }


  /**
   * Sets the value of the 'product' field.
   * @param value the value to set.
   */
  public void setProduct(java.lang.CharSequence value) {
    this.product = value;
  }

  /**
   * Creates a new companies RecordBuilder.
   * @return A new companies RecordBuilder
   */
  public static com.scb.bpsi.schema.companies.companies.Builder newBuilder() {
    return new com.scb.bpsi.schema.companies.companies.Builder();
  }

  /**
   * Creates a new companies RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new companies RecordBuilder
   */
  public static com.scb.bpsi.schema.companies.companies.Builder newBuilder(com.scb.bpsi.schema.companies.companies.Builder other) {
    if (other == null) {
      return new com.scb.bpsi.schema.companies.companies.Builder();
    } else {
      return new com.scb.bpsi.schema.companies.companies.Builder(other);
    }
  }

  /**
   * Creates a new companies RecordBuilder by copying an existing companies instance.
   * @param other The existing instance to copy.
   * @return A new companies RecordBuilder
   */
  public static com.scb.bpsi.schema.companies.companies.Builder newBuilder(com.scb.bpsi.schema.companies.companies other) {
    if (other == null) {
      return new com.scb.bpsi.schema.companies.companies.Builder();
    } else {
      return new com.scb.bpsi.schema.companies.companies.Builder(other);
    }
  }

  /**
   * RecordBuilder for companies instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<companies>
    implements org.apache.avro.data.RecordBuilder<companies> {

    private java.lang.CharSequence company;
    private java.lang.CharSequence location;
    private java.lang.CharSequence product;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(com.scb.bpsi.schema.companies.companies.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.company)) {
        this.company = data().deepCopy(fields()[0].schema(), other.company);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.location)) {
        this.location = data().deepCopy(fields()[1].schema(), other.location);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (isValidValue(fields()[2], other.product)) {
        this.product = data().deepCopy(fields()[2].schema(), other.product);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
    }

    /**
     * Creates a Builder by copying an existing companies instance
     * @param other The existing instance to copy.
     */
    private Builder(com.scb.bpsi.schema.companies.companies other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.company)) {
        this.company = data().deepCopy(fields()[0].schema(), other.company);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.location)) {
        this.location = data().deepCopy(fields()[1].schema(), other.location);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.product)) {
        this.product = data().deepCopy(fields()[2].schema(), other.product);
        fieldSetFlags()[2] = true;
      }
    }

    /**
      * Gets the value of the 'company' field.
      * @return The value.
      */
    public java.lang.CharSequence getCompany() {
      return company;
    }


    /**
      * Sets the value of the 'company' field.
      * @param value The value of 'company'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.companies.companies.Builder setCompany(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.company = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'company' field has been set.
      * @return True if the 'company' field has been set, false otherwise.
      */
    public boolean hasCompany() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'company' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.companies.companies.Builder clearCompany() {
      company = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'location' field.
      * @return The value.
      */
    public java.lang.CharSequence getLocation() {
      return location;
    }


    /**
      * Sets the value of the 'location' field.
      * @param value The value of 'location'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.companies.companies.Builder setLocation(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.location = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'location' field has been set.
      * @return True if the 'location' field has been set, false otherwise.
      */
    public boolean hasLocation() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'location' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.companies.companies.Builder clearLocation() {
      location = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'product' field.
      * @return The value.
      */
    public java.lang.CharSequence getProduct() {
      return product;
    }


    /**
      * Sets the value of the 'product' field.
      * @param value The value of 'product'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.companies.companies.Builder setProduct(java.lang.CharSequence value) {
      validate(fields()[2], value);
      this.product = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'product' field has been set.
      * @return True if the 'product' field has been set, false otherwise.
      */
    public boolean hasProduct() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'product' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.companies.companies.Builder clearProduct() {
      product = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public companies build() {
      try {
        companies record = new companies();
        record.company = fieldSetFlags()[0] ? this.company : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.location = fieldSetFlags()[1] ? this.location : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.product = fieldSetFlags()[2] ? this.product : (java.lang.CharSequence) defaultValue(fields()[2]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<companies>
    WRITER$ = (org.apache.avro.io.DatumWriter<companies>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<companies>
    READER$ = (org.apache.avro.io.DatumReader<companies>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeString(this.company);

    out.writeString(this.location);

    out.writeString(this.product);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.company = in.readString(this.company instanceof Utf8 ? (Utf8)this.company : null);

      this.location = in.readString(this.location instanceof Utf8 ? (Utf8)this.location : null);

      this.product = in.readString(this.product instanceof Utf8 ? (Utf8)this.product : null);

    } else {
      for (int i = 0; i < 3; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.company = in.readString(this.company instanceof Utf8 ? (Utf8)this.company : null);
          break;

        case 1:
          this.location = in.readString(this.location instanceof Utf8 ? (Utf8)this.location : null);
          break;

        case 2:
          this.product = in.readString(this.product instanceof Utf8 ? (Utf8)this.product : null);
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










