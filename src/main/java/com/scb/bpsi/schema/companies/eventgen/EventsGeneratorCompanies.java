package com.scb.bpsi.schema.companies.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.schema.companies.*;

public class EventsGeneratorCompanies implements EventsProduction {

	
	@SuppressWarnings("unchecked")
	@Override
	public List<test> eventsGenerator(int numOfEvents) {
		List<test> events=new ArrayList<>();
		
		EventGenerationImpl event = new EventGenerationImpl();
	
    	IntStream.range(0,numOfEvents)
		.forEach(i -> {
						events.add(event.test_generateMessage());
						});
    	return events;
	}
	
}
