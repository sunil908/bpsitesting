package com.scb.bpsi.schema.vega.legalentitydetails.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.messagegenerator.event.EventsProductionFactory;
import com.scb.bpsi.schema.vega.legalentitydetails.AnonType_LEDetailsLegalEntityMessage;
import com.scb.bpsi.schema.vega.legalentitydetails.eventgen.*;

public class EventsGeneratorLegalEntityDetails implements EventsProduction {

	
	@SuppressWarnings("unchecked")
	@Override
	public List<AnonType_LEDetailsLegalEntityMessage> eventsGenerator(int numOfEvents) {
		List<AnonType_LEDetailsLegalEntityMessage> events=new ArrayList<>();
		
		EventGenerationImpl event = new EventGenerationImpl();
	
    	IntStream.range(0,numOfEvents)
		.forEach(i -> {
						events.add(event.generateLEDetailsLegalEntityMessage());
						});
    	return events;
	}
	
}
