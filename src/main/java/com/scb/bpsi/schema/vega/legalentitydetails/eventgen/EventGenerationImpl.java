package com.scb.bpsi.schema.vega.legalentitydetails.eventgen;

import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import com.scb.bpsi.messagegenerator.event.EventGenerator;
import com.scb.bpsi.messagegenerator.refdata.enums.*;
import com.scb.bpsi.schema.vega.legalentitydetails.*;



public class EventGenerationImpl implements EventGenerator {
	
	private Faker faker = new Faker();
	
	
	
	
	/////////  LEDetails /////////////
	
	public AnonType_LEDetailsLegalEntityMessage generateLEDetailsLegalEntityMessage() {
		return AnonType_LEDetailsLegalEntityMessage.newBuilder()
			.setLegalEntityID(faker.number().digits(5))
			.setLegalEntityName(faker.company().name())
			.setLegalEntityNameNonRoman(None.NONE.name())
			.setLegalEntityType(faker.options().option(EntityType.entityType))
			.setCountryOfIncorporation(faker.address().country())
			.setDateOfIncorporation(faker.date().past(3650, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
			.setIncorporationNumber(faker.business().creditCardNumber())
			.setOperationalRegistrationNumber(faker.business().creditCardNumber())
			.setClientSegment(faker.options().option(SegmentCode.class).name())
			.setSegmentCode(faker.options().option(SegmentCode.class).name())
			.setSubSegmentCode(faker.options().option(SegmentCode.class).name())
			.setGlobalDataSharingClientOptIn("N")
			.setEntityType(faker.options().option(EntityType.entityType))
			.setOrganisationUnitType(faker.options().option(OrganizationUnitType.class).name())
			.setCountryOfDomicile(faker.address().country())
			.setCountryOfPrimaryOperations(faker.address().country())
			.setLegalFormationType("NONE")
			.setTradingStatus(None.NONE.name())
			.setEntityStatus(faker.options().option(GeneralStatus.class).name())
			.setClientStatus(faker.options().option(GeneralStatus.class).name())
			.setClientPriority(faker.options().option(Priority.class).name())
			.setRelationshipStartDate(faker.date().past(3650, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
			.setRelationshipEndDate(faker.date().future(365,TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
			.setConsentForInformationDisclosureAcrossSCBGroup(faker.options().option(YesNo.class).name())
			.setWebsite(faker.company().url())
			.setPreviousNames("NONE")
			.setPreviousNameDate(0)
			.setShortName("NONE")
			.setMaskedName("NONE")
			.setTradingAsName("NONE")
			.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
			.setCreatedBy(faker.options().option(UserId.class).name())
			.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
			.setLastUpdatedBy(faker.options().option(UserId.class).name())
			.build();
	}
	
	
		
	
}


