package com.scb.bpsi.schema.H05Gs6Qgflop0dWD.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import com.scb.bpsi.messagegenerator.event.EventGenerator;
import com.scb.bpsi.messagegenerator.refdata.enums.*;
import com.scb.bpsi.schema.H05Gs6Qgflop0dWD.*;



public class EventGenerationImpl implements EventGenerator {
	
	private Faker faker = new Faker();
	
	
	public Legal_Entity Legal_Entity_generateMessage() {
		return Legal_Entity.newBuilder()
				.setCountryOfDomicile(faker.address().countryCode())
				.setEntityType(faker.options().option(EntityType.entityType))
				.setLegalEntityID(faker.number().digits(10))
				.setLegalEntityName(faker.company().name())
				.build();
	}
	
	public Mail_Addresses Mail_Addresses_generateMessage() {
		return Mail_Addresses.newBuilder()
				.setCountryCode(faker.address().countryCode())
				.setLine12(faker.address().streetAddress())
				.setState(faker.address().state())
				.setTownRegCityTown(faker.address().cityName())
				.setZipCode(faker.random().nextInt(100000))
				.build();
	}

	
	public Official_Addresses Official_Addresses_generateMessage() {
		return Official_Addresses.newBuilder()
				.setCountryCode(faker.address().countryCode())
				.setLine12(faker.address().streetAddress())
				.setState(faker.address().state())
				.setTownRegCityTown(faker.address().cityName())
				.setZipCode(faker.random().nextInt(100000))
				.build();
	}
	
		
	
	public Registered_Addresses Registered_Addresses_generateMessage() {
		return Registered_Addresses.newBuilder()
				.setCountryCode(faker.address().countryCode())
				.setLine12(faker.address().streetAddress())
				.setState(faker.address().state())
				.setTownRegCityTown(faker.address().cityName())
				.setZipCode(faker.random().nextInt(100000))
				.build();
	}

	
	
	
	
	public Test_Addresses Test_Addresses_generateMessage() {
		
		// List of Legal Entities 
		List<Legal_Entity> LegalEntities = new ArrayList<Legal_Entity>();
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> LegalEntities.add(Legal_Entity_generateMessage()));
		
		// List of Mail_Addresses
		List<Mail_Addresses> MailAddresses = new ArrayList<Mail_Addresses>();
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> MailAddresses.add(Mail_Addresses_generateMessage()));
		
		// List of Official_Addresses
		List<Official_Addresses> OfficialAddresses = new ArrayList<Official_Addresses>();
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> OfficialAddresses.add(Official_Addresses_generateMessage()));
		
		// List of Registered_Addresses
		List<Registered_Addresses> RegisteredAddresses = new ArrayList<Registered_Addresses>();
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> RegisteredAddresses.add(Registered_Addresses_generateMessage()));
		
		return Test_Addresses.newBuilder()
				.setAddressDetails(LegalEntities)
				.setMailAddressesDetails(MailAddresses)
				.setOfficialAddressesDetails(OfficialAddresses)
				.setRegisteredAddressesDetails(RegisteredAddresses)
				.build();
	}
	
}


