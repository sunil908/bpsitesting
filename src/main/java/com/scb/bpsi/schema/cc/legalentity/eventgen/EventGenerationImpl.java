package com.scb.bpsi.schema.cc.legalentity.eventgen;

import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import com.scb.bpsi.messagegenerator.event.EventGenerator;
import com.scb.bpsi.messagegenerator.refdata.enums.*;
import com.scb.bpsi.schema.cc.legalentity.*;



public class EventGenerationImpl implements EventGenerator {
	
	private Faker faker = new Faker();

	/*
	/////////   Signature ///////////////
	
	public AnonType_Signature generateSignature() {
		return AnonType_Signature.newBuilder()
				.setId("MyFirstSignature")
				.setSignedInfo(generateSignedInfoSignature())
				.setSignatureValue("...")
				.setKeyInfo(generateKeyInfoSignature())
				.build();
	}
	
	public AnonType_KeyInfoSignature generateKeyInfoSignature() {
		
		AnonType_X509DataKeyInfoSignature infoSig 
							= AnonType_X509DataKeyInfoSignature.newBuilder()
								.setX509Certificate("MIICXTCCA..")
								.build();
	
		return AnonType_KeyInfoSignature.newBuilder()
				.setId("RSAKey")
				.setX509Data(infoSig)
				.build();
	}
	
	public AnonType_SignedInfoSignature generateSignedInfoSignature() {
		
		AnonType_CanonicalizationMethodSignedInfoSignature canonMethod = new AnonType_CanonicalizationMethodSignedInfoSignature();
		canonMethod.setAlgorithm("http://www.w3.org/2006/12/xml-c14n11");
	
		AnonType_SignatureMethodSignedInfoSignature sigMethod = new AnonType_SignatureMethodSignedInfoSignature();
		sigMethod.setAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
		
		
		
		return AnonType_SignedInfoSignature.newBuilder()
				.setCanonicalizationMethod(canonMethod)
				.setSignatureMethod(sigMethod)
				.setReference(generateReferenceSignedInfoSignature())
				
				.build();
	}
	
	public List<AnonType_ReferenceSignedInfoSignature> generateReferenceSignedInfoSignature() {

		AnonType_DigestMethodReferenceSignedInfoSignature digestMethod = new AnonType_DigestMethodReferenceSignedInfoSignature();
		digestMethod.setAlgorithm("http://www.w3.org/2001/04/xmlenc#sha256");
		
		final AnonType_ReferenceSignedInfoSignature reference = AnonType_ReferenceSignedInfoSignature.newBuilder()
					.setURI("http://www.w3.org/TR/2000/REC-xhtml1-20000126/")
					.setDigestMethod(digestMethod)
					.setDigestValue("dGhpcyBpcyBub3QgYSBzaWduYXR1cmUK")
					.setTransforms(generateTransformsReferenceSignedInfoSignature())
					.build();
		
		List<AnonType_ReferenceSignedInfoSignature> references = new ArrayList<AnonType_ReferenceSignedInfoSignature>();
		
		IntStream.range(0,1)
			.forEach(i -> references.add(reference));
		
		return references;
	
	
	}
	
	
	public AnonType_TransformsReferenceSignedInfoSignature generateTransformsReferenceSignedInfoSignature() {
		
		
			final AnonType_TransformTransformsReferenceSignedInfoSignature transform = 
					AnonType_TransformTransformsReferenceSignedInfoSignature.newBuilder()
						.setAlgorithm("http://www.w3.org/2006/12/xml-c14n11")
						.build();
	
			List<AnonType_TransformTransformsReferenceSignedInfoSignature> transforms= new ArrayList<AnonType_TransformTransformsReferenceSignedInfoSignature>();
		
			IntStream.range(0,1)
				.forEach(i -> transforms.add(transform));
			
			return AnonType_TransformsReferenceSignedInfoSignature.newBuilder()
					.setTransform(transforms)
					.build();
					
	}
	*/
	
	////////// Signature Ends ///////////////
	
	
	/////////  LegalEntity /////////////
	
	public Message generateMessage() {
		return Message.newBuilder()
				.setHeader(generateHeaderMessage())
				.setLegalEntity(generateLegalEntityMessage())
				.setBusinessAcceptance(generateBusinessAcceptanceMessage())
				.setEAF(generateEAFMessage())
				.setESRA(generateESRAMessage())
				//.setTaxInfo(generateTaxInfoMessage())
				.setRegulatoryClassifications(generateRegulatoryClassificationsMessage())
				.setTradingEntities(generateTradingEntitiesMessage())
				.setCases(generateCasesMessage())
				.setRelatedParties(generateRelatedPartiesMessage())
				//.setSignature(generateSignature())
				.build();
	}
	
	
							//////////  Header Message ///////////
	public AnonType_HeaderMessage generateHeaderMessage()
	{   
		return AnonType_HeaderMessage.newBuilder()
				.setGUID(UUID.randomUUID().toString())
				.setMessageType("data")
				.setMessageVersion("1")
				.setSchemaName("legalentity")
				.setSchemaVersion("1")
				.setCreatedDate(faker.date().past(3650, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.build();
	}
	
	
	
							//////////  Header Message Ends //////////
	
	////////////  Legal Entity //////////////////////
	
	public AnonType_LegalEntityMessage generateLegalEntityMessage() {
		return AnonType_LegalEntityMessage.newBuilder()
				.setLEDetails(generateLEDetailsLegalEntityMessage())
				.setAddresses(generateAddressesLegalEntityMessage())
				.setHierarchy(generateHierarchyLegalEntityMessage())
				.setExternalIdentifier(generateExternalIdentifierLegalEntityMessage())
				.setInternalIdentifier(generateInternalIdentifierLegalEntityMessage())
				.setIndustryClassifications(generateIndustryClassificationsLegalEntityMessage())
				.setProducts(generateProductsLegalEntityMessage())
				.setAssociatedExchanges(generateAssociatedExchangesLegalEntityMessage())
				.setDocuments(generateDocumentsLegalEntityMessage())
				.setRoles(generateRolesLegalEntityMessage())
				.setJurisdictions(generateJurisdictionsLegalEntityMessage())
				.setSCBContacts(generateSCBContactsLegalEntityMessage())
				.setClientContacts(generateClientContactsLegalEntityMessage())
				.build();
	}
	
	//////////// Legal Entity Ends ////////////////////
	
	
							/////////  LEDetails /////////////
	
	public AnonType_LEDetailsLegalEntityMessage generateLEDetailsLegalEntityMessage() {
		return AnonType_LEDetailsLegalEntityMessage.newBuilder()
						.setLegalEntityID(faker.number().digits(5))
						.setLegalEntityName(faker.company().name())
						.setLegalEntityNameNonRoman(None.NONE.name())
						.setLegalEntityType(faker.options().option(EntityType.entityType))
						.setCountryOfIncorporation(faker.address().country())
						.setDateOfIncorporation(faker.date().past(3650, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
						.setIncorporationNumber(faker.business().creditCardNumber())
						.setOperationalRegistrationNumber(faker.business().creditCardNumber())
						.setClientSegment(faker.options().option(SegmentCode.class).name())
						.setSegmentCode(faker.options().option(SegmentCode.class).name())
						.setSubSegmentCode(faker.options().option(SegmentCode.class).name())
						.setGlobalDataSharingClientOptIn("N")
						.setEntityType(faker.options().option(EntityType.entityType))
						.setOrganisationUnitType(faker.options().option(OrganizationUnitType.class).name())
						.setCountryOfDomicile(faker.address().country())
						.setCountryOfPrimaryOperations(faker.address().country())
						.setLegalFormationType("NONE")
						.setTradingStatus(None.NONE.name())
						.setEntityStatus(faker.options().option(GeneralStatus.class).name())
						.setClientStatus(faker.options().option(GeneralStatus.class).name())
						.setClientPriority(faker.options().option(Priority.class).name())
						.setRelationshipStartDate(faker.date().past(3650, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
						.setRelationshipEndDate(faker.date().future(365,TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
						.setConsentForInformationDisclosureAcrossSCBGroup(faker.options().option(YesNo.class).name())
						.setWebsite(faker.company().url())
						.setPreviousNames("NONE")
						.setPreviousNameDate(0)
						.setShortName("NONE")
						.setMaskedName("NONE")
						.setTradingAsName("NONE")
						.setAdditionalAliases(generateAdditionalAliasesLEDetailsLegalEntityMessage())
						.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
						.setCreatedBy(faker.options().option(UserId.class).name())
						.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
						.setLastUpdatedBy(faker.options().option(UserId.class).name())
						.setRowVersion(faker.app().version())
						.build();
	}
	
	public  AnonType_AdditionalAliasesLEDetailsLegalEntityMessage generateAdditionalAliasesLEDetailsLegalEntityMessage()
	{
		List<AnonType_AdditionalAliasAdditionalAliasesLEDetailsLegalEntityMessage> aliases= new ArrayList<AnonType_AdditionalAliasAdditionalAliasesLEDetailsLegalEntityMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> aliases.add(generateAdditionalAliasAdditionalAliasesLEDetailsLegalEntityMessage()));
		
		return AnonType_AdditionalAliasesLEDetailsLegalEntityMessage.newBuilder()
				.setAdditionalAlias(aliases)
				.build();
		
	}
	
		
	public AnonType_AdditionalAliasAdditionalAliasesLEDetailsLegalEntityMessage generateAdditionalAliasAdditionalAliasesLEDetailsLegalEntityMessage() {
		return AnonType_AdditionalAliasAdditionalAliasesLEDetailsLegalEntityMessage.newBuilder()
				.setSequenceNumber("1")
				.setAdditionalAliasName("AliasName")
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
							/////////  LEDetails Ends /////////////
	
							////////////   Addresses ////////////////
	
	public AnonType_AddressesLegalEntityMessage generateAddressesLegalEntityMessage() {
		
		List<AnonType_AddressAddressesLegalEntityMessage> Addresses = new ArrayList<AnonType_AddressAddressesLegalEntityMessage>();
		
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> Addresses.add(generateAddressAddressesLegalEntityMessage()));
		
		return AnonType_AddressesLegalEntityMessage.newBuilder()
				.setAddress(Addresses)
				.build();
	}
	
	public AnonType_AddressAddressesLegalEntityMessage generateAddressAddressesLegalEntityMessage() {
		return AnonType_AddressAddressesLegalEntityMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setAddressType(faker.options().option(AddressType.class).name())
				.setLine1(faker.address().streetAddress())
				.setLine2(faker.address().secondaryAddress())
				.setTown(faker.address().streetName())
				.setZipCode(faker.address().zipCode())
				.setState(faker.address().state())
				.setCountryCode(faker.address().countryCode())
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())				
				.build();
	}
							////////////   Addresses Ends ////////////////
		
							//////////// Hierarchy //////////////////////
	
	public AnonType_AssociationHierarchyLegalEntityMessage generateAssociationHierarchyLegalEntityMessage() {
		return AnonType_AssociationHierarchyLegalEntityMessage.newBuilder()
			.setAssociationID(faker.number().digits(5).toString())
			.setFromEntityID(faker.number().digits(5).toString())
			.setToEntityID(faker.number().digits(5).toString())
			.setToEntityName(faker.company().name())
			.setAssociationType(faker.options().option(AssociationType.class).name())
			.setOwnershipPercentage(faker.number().numberBetween(1,100))
			.setNotesOnShareholding("Notes on Share Holding")
			.setTaxRelatedPartyType("NONE")
			.build();
	}

	
	public AnonType_HierarchyLegalEntityMessage generateHierarchyLegalEntityMessage() {
		
		List<AnonType_AssociationHierarchyLegalEntityMessage> associations = new ArrayList<AnonType_AssociationHierarchyLegalEntityMessage>();
		
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> associations.add(generateAssociationHierarchyLegalEntityMessage()));
		
		return AnonType_HierarchyLegalEntityMessage.newBuilder()
				.setAssociation(associations)
				.build();
	}
	
				//////////// Hierarchy Ends //////////////////////


	
	
				////////////  ExternalIdentifier //////////////////////
	
	public AnonType_ExternalIdentifierLegalEntityMessage generateExternalIdentifierLegalEntityMessage() {
		return AnonType_ExternalIdentifierLegalEntityMessage.newBuilder()
			.setLEI("LEI0000001")
			.setSwiftBIC("SWFT00001")
			.setCIK("CIK0000001")
			.setFundCodeType(10001.09)
			.setFundCodeValue("NONE")
			.setIdentifiers(generateIdentifiersExternalIdentifierLegalEntityMessage())
			.build();
	}

	public AnonType_IdentifiersExternalIdentifierLegalEntityMessage generateIdentifiersExternalIdentifierLegalEntityMessage() {
		
		List<AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> externalIdentifiers = new ArrayList<AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage>();
		
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> externalIdentifiers.add(generateIdentifierIdentifiersExternalIdentifierLegalEntityMessage()));
				
		return AnonType_IdentifiersExternalIdentifierLegalEntityMessage.newBuilder()
				.setIdentifier(externalIdentifiers)
				.build();
	}
	
	public AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage generateIdentifierIdentifiersExternalIdentifierLegalEntityMessage() {
		return AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage.newBuilder()
				.setSequenceNumber("1")
				.setSystemName("BLOOMBERG")
				.setSystemRefID("PRM"+faker.number().digits(5))
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
	
				//////////// ExternalIdentifier Ends //////////////////////
				
				////////////InternalIdentifier //////////////////////
	
	public AnonType_InternalIdentifierLegalEntityMessage generateInternalIdentifierLegalEntityMessage() {
		return AnonType_InternalIdentifierLegalEntityMessage.newBuilder()
				.setIdentifiers(generateIdentifiersInternalIdentifierLegalEntityMessage())
				.setDMUIdentifiers(generateDMUIdentifiersInternalIdentifierLegalEntityMessage())
				.build();
	}
	
	
	public AnonType_IdentifiersInternalIdentifierLegalEntityMessage generateIdentifiersInternalIdentifierLegalEntityMessage() {
		
		List<AnonType_IdentifierIdentifiersInternalIdentifierLegalEntityMessage> internalIdentifiers = new ArrayList<AnonType_IdentifierIdentifiersInternalIdentifierLegalEntityMessage>();
		
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> internalIdentifiers.add(generateIdentifierIdentifiersInternalIdentifierLegalEntityMessage()));
				
		return AnonType_IdentifiersInternalIdentifierLegalEntityMessage.newBuilder()
				.setIdentifier(internalIdentifiers)
				.build();
	}
	
	public AnonType_IdentifierIdentifiersInternalIdentifierLegalEntityMessage generateIdentifierIdentifiersInternalIdentifierLegalEntityMessage() {
		return AnonType_IdentifierIdentifiersInternalIdentifierLegalEntityMessage.newBuilder()
				.setSequenceNumber("1")
				.setSystemName("LEI")
				.setSystemRefID("LEI"+faker.number().digits(5))
				.setCreatedDate("NONE")
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
	
public AnonType_DMUIdentifiersInternalIdentifierLegalEntityMessage generateDMUIdentifiersInternalIdentifierLegalEntityMessage() {
		
		List<AnonType_DMUIdentifierDMUIdentifiersInternalIdentifierLegalEntityMessage> DMUIdentifiers= new ArrayList<AnonType_DMUIdentifierDMUIdentifiersInternalIdentifierLegalEntityMessage>();
		
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> DMUIdentifiers.add(generateDMUIdentifierDMUIdentifiersInternalIdentifierLegalEntityMessage()));
		
		return AnonType_DMUIdentifiersInternalIdentifierLegalEntityMessage.newBuilder()
				.setDMUIdentifier(DMUIdentifiers)
				.build();
	}
	

	public AnonType_DMUIdentifierDMUIdentifiersInternalIdentifierLegalEntityMessage generateDMUIdentifierDMUIdentifiersInternalIdentifierLegalEntityMessage() {
		return AnonType_DMUIdentifierDMUIdentifiersInternalIdentifierLegalEntityMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setSourceSystem("SourceSystem")
				.setTargetSystem("TargetSystem")
				.setBusinessEntityType(faker.options().option(EntityType.entityType))
				.setTargetSystemID("T001")
				.setTargetSystemSubID("TSUB001")
				.setIsPrimary("1")
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
	
	/////////////////// IndustryClassifications ////////////////////////
	
	public AnonType_IndustryClassificationsLegalEntityMessage generateIndustryClassificationsLegalEntityMessage() {
		return AnonType_IndustryClassificationsLegalEntityMessage.newBuilder()
				.setUKSICCode("UNISIC"+faker.number().digits(5))
				.setISCCode("ISIC"+faker.number().digits(5))
				.setSICCode("SIC"+faker.number().digits(5))
				.setNAICCode("NAIC"+faker.number().digits(5))
				.setNAICS2012Code("NAICS2012"+faker.number().digits(5))
				.setNACECode("NACE"+faker.number().digits(5))
				.setNACE2Code("NACE2"+faker.number().digits(5))
				.setUSSICCode("USSIC"+faker.number().digits(5))
				.setUKSICCode("UKSIC"+faker.number().digits(5))
				.setUNISICCode("UNISIC"+faker.number().digits(5))
				.build();
	}
	/////////////////// IndustryClassifications Ends ////////////////////////
	
	/////////////////// Products //////////////////////////////
	
	public AnonType_ProductsLegalEntityMessage generateProductsLegalEntityMessage() {
		
		List<AnonType_ProductProductsLegalEntityMessage> products = new ArrayList<AnonType_ProductProductsLegalEntityMessage>();
		
		IntStream.range(0,2)
		.forEach(i -> products.add(generateProductProductsLegalEntityMessage()));
		return AnonType_ProductsLegalEntityMessage.newBuilder()
				.setProduct(products)
				.build();
	}
	
	public AnonType_ProductProductsLegalEntityMessage generateProductProductsLegalEntityMessage() {
		return AnonType_ProductProductsLegalEntityMessage.newBuilder()
			 .setSequenceNumber(Integer.toString(faker.number().randomDigit()))
			 .setSCBBookingEntity(faker.address().country())
			 .setSCBCountry(faker.address().country())
			 .setProductGroup("PRODGROUP001")
			 .setProductType(faker.options().option(ProductType.class).name())
			 .setProductCategory("Others")
			 .setProductCurrency(faker.currency().code())
			 .setPurposeOfAccount("NONE")
			 .setMultiCurrencyTransactionFlag("Yes")
			 .setTradingLocation("NONE")
			 .setSellingLocation("NONE")
			 .setProductStatus(faker.options().option(GeneralStatus.class).name())
			 .setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
			 .setCreatedBy(faker.options().option(UserId.class).name())
			 .setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
			 .setLastUpdatedBy(faker.options().option(UserId.class).name())
			 .setRowVersion(faker.app().version())
			 .setPSGLCodes(generatePSGLCodesProductProductsLegalEntityMessage())
			 .setAnticipatedTransactions(generateAnticipatedTransactionsProductProductsLegalEntityMessage())
			 .build();
	}
	
	public AnonType_PSGLCodesProductProductsLegalEntityMessage generatePSGLCodesProductProductsLegalEntityMessage() {
		
		List<AnonType_PSGLCodePSGLCodesProductProductsLegalEntityMessage> PSGLCodes = new ArrayList<AnonType_PSGLCodePSGLCodesProductProductsLegalEntityMessage>();
		
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> PSGLCodes.add(AnonType_PSGLCodePSGLCodesProductProductsLegalEntityMessage.newBuilder()
														.setPSGLCode("CODE" + faker.number().digits(1))
														 .build()));
		return AnonType_PSGLCodesProductProductsLegalEntityMessage.newBuilder()
				.setPSGLCode(PSGLCodes)
				.build();
	}
	
	public AnonType_AnticipatedTransactionsProductProductsLegalEntityMessage generateAnticipatedTransactionsProductProductsLegalEntityMessage() {
		
		List<AnonType_AnticipatedTransactionAnticipatedTransactionsProductProductsLegalEntityMessage> anticipatedTransactions = new ArrayList<AnonType_AnticipatedTransactionAnticipatedTransactionsProductProductsLegalEntityMessage>();
		
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> anticipatedTransactions.add(generateAnticipatedTransactionAnticipatedTransactionsProductProductsLegalEntityMessage()));
				
		return AnonType_AnticipatedTransactionsProductProductsLegalEntityMessage.newBuilder()
				.setAnticipatedTransaction(anticipatedTransactions)
				.build();
	}
	
	
	public AnonType_AnticipatedTransactionAnticipatedTransactionsProductProductsLegalEntityMessage generateAnticipatedTransactionAnticipatedTransactionsProductProductsLegalEntityMessage() {
		return AnonType_AnticipatedTransactionAnticipatedTransactionsProductProductsLegalEntityMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setTransactionType("TRANS_EQUITY")
				.setMonthlyAnticipatedLevelOfTransactionValue(faker.number().randomDouble(2, 0, 10000000))
				.setMonthlyVolume(faker.number().randomDouble(2, 0, 10000))
				.setAnticipatedCrossBorderTransactionVolumePercentage("40")
				.setAnticipatedCrossBorderTransactionVolume(faker.number().digits(5))
				.setAnticipatedCrossBorderTransactionValuePercentage("40")
				.setAnticipatedCrossBorderTransactionValue("190000")
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
	/////////////////// Products Ends //////////////////////////////
	
	/////////////////// AssociatedExchange //////////////////////////////
	
	public AnonType_AssociatedExchangeAssociatedExchangesLegalEntityMessage generateAssociatedExchangeAssociatedExchangesLegalEntityMessage() {
		return AnonType_AssociatedExchangeAssociatedExchangesLegalEntityMessage.newBuilder()
				.setRegulatedBy("REGULATOR1")
				.setPrimaryListedExchange("SP100")
				.setPrimaryListedTicker("NSE")
				.setRegulatoryStatus(faker.options().option(GeneralStatus.class).name())
				.build();
	}
	
	/////////////   AssociatedExchanges ///////////////
	public AnonType_AssociatedExchangesLegalEntityMessage generateAssociatedExchangesLegalEntityMessage() {
		
		List<AnonType_AssociatedExchangeAssociatedExchangesLegalEntityMessage> associatedExchanges = new ArrayList<AnonType_AssociatedExchangeAssociatedExchangesLegalEntityMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> associatedExchanges.add(generateAssociatedExchangeAssociatedExchangesLegalEntityMessage()));
				
		return AnonType_AssociatedExchangesLegalEntityMessage.newBuilder()
				.setAssociatedExchange(associatedExchanges)
				.build();
	}

	/////////////   AssociatedExchanges End ///////////////
	
	///////////// Documents ///////////////////////
	public AnonType_DocumentsLegalEntityMessage generateDocumentsLegalEntityMessage() {
		
		List<AnonType_DocumentDocumentsLegalEntityMessage> documents = new ArrayList<AnonType_DocumentDocumentsLegalEntityMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> documents.add(generateDocumentDocumentsLegalEntityMessage()));
				
		return AnonType_DocumentsLegalEntityMessage.newBuilder()
				.setDocument(documents)
				.build();
	}
	
	public AnonType_DocumentDocumentsLegalEntityMessage generateDocumentDocumentsLegalEntityMessage() {
	
		return AnonType_DocumentDocumentsLegalEntityMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setDocumentIdentificationNumber("DOC00001")
				.setDocumentSource("website")
				.setDocumentDirection("NONE")
				.setFileLocation("\\bpsi\\documents")
				.setDocumentName(faker.file().fileName())
				.setDocumentCategory("NONE")
				.setDocumentType("NONE")
				.setCurrentDocument("1")
				.setSignedAndFull("1")
				.setIsLegible("1")
				.setDocumentNonEnglishLanguageIndicator("1")
				.setDocumentEffectiveDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setDocumentExpirationDate(faker.date().future(365,TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setIssuerOfTheDocument("ISSUER1")
				.setDateOfIssuance(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setPlaceOfIssuance(faker.address().countryCode())
				.setDocumentClass("DocumentClass")
				.setDocumentCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))				
				.setDocumentStatus("NONE")
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
		
	}
						///////////// Documents Ends ///////////////////////
	
						///////////// Roles ///////////////////////
	
	public AnonType_RolesLegalEntityMessage generateRolesLegalEntityMessage() {
		
		List<AnonType_RoleRolesLegalEntityMessage> roles = new ArrayList<AnonType_RoleRolesLegalEntityMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> roles.add(generateRoleRolesLegalEntityMessage()));
				
		return AnonType_RolesLegalEntityMessage.newBuilder()
				.setRole(roles)
				.build();
	}
	
	public AnonType_RoleRolesLegalEntityMessage generateRoleRolesLegalEntityMessage() {
		return AnonType_RoleRolesLegalEntityMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setLegalEntityRole("ROLE1")
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
				
	}
	
						///////////// Roles Ends///////////////////////
	
						///////////// Jurisdictions ///////////////////////
	
	public AnonType_JurisdictionsLegalEntityMessage generateJurisdictionsLegalEntityMessage() {
		
		List<AnonType_JurisdictionJurisdictionsLegalEntityMessage> jurisdictions = new ArrayList<AnonType_JurisdictionJurisdictionsLegalEntityMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> jurisdictions.add(generateJurisdictionJurisdictionsLegalEntityMessage()));
				
		return AnonType_JurisdictionsLegalEntityMessage.newBuilder()
				.setJurisdiction(jurisdictions)
				.build();
	}
	
	
	public AnonType_JurisdictionJurisdictionsLegalEntityMessage generateJurisdictionJurisdictionsLegalEntityMessage() {
		return AnonType_JurisdictionJurisdictionsLegalEntityMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setJurisdiction(faker.address().country())
				.setCreatedDate(0)
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
	
						///////////// Jurisdictions Ends///////////////////////
	
	///////////// SCB Contacts ///////////////////////
	
	public AnonType_SCBContactsLegalEntityMessage generateSCBContactsLegalEntityMessage() {
		
		List<AnonType_SCBContactSCBContactsLegalEntityMessage> contacts = new ArrayList<AnonType_SCBContactSCBContactsLegalEntityMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> contacts.add(generateSCBContactSCBContactsLegalEntityMessage()));
				
		return AnonType_SCBContactsLegalEntityMessage.newBuilder()
				.setSCBContact(contacts)
				.build();
	}
	
	
	public AnonType_SCBContactSCBContactsLegalEntityMessage generateSCBContactSCBContactsLegalEntityMessage() {
		return AnonType_SCBContactSCBContactsLegalEntityMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setRelationshipType("CRM")
				.setBankID(faker.number().digits(5))
				.setIsPrimaryRelationship("1")
				.setCreatedDate(0)
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
	
	/////////////  SCB Contacts Ends///////////////////////
	
	/////////////  SCB Client Contact ///////////////////////
	
	public AnonType_ClientContactsLegalEntityMessage generateClientContactsLegalEntityMessage() {
		
		List<AnonType_ClientContactClientContactsLegalEntityMessage> contacts = new ArrayList<AnonType_ClientContactClientContactsLegalEntityMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> contacts.add(generateClientContactClientContactsLegalEntityMessage()));
				
		return AnonType_ClientContactsLegalEntityMessage.newBuilder()
				.setClientContact(contacts)
				.build();
	}
	
	public AnonType_ClientContactClientContactsLegalEntityMessage generateClientContactClientContactsLegalEntityMessage() {
		return AnonType_ClientContactClientContactsLegalEntityMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setContactPersonType("CRM")
				.setTitle(faker.name().title())
				.setBusinessTitle(faker.job().title())
				.setFirstName(faker.superhero().name()) 
				.setLastName(faker.name().lastName())
				.setContactStatus("ACTIVE") 
				.setEmailAddress(faker.internet().emailAddress()) 
				.setPrimaryPhoneType("OFFICE") 
				.setPhoneNumberAreaCode(faker.number().digits(2)) 
				.setPhoneNumberCountry(faker.phoneNumber().phoneNumber().toString())  
				.setWorkPhone(faker.phoneNumber().phoneNumber().toString()) 
				.setHomePhone(faker.phoneNumber().phoneNumber().toString())  
				.setMobile(faker.phoneNumber().phoneNumber().toString())
				.setFax(faker.phoneNumber().phoneNumber().toString())
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setCreatedDate(0)
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
	
	/////////////  SCB Client Contact Ends ///////////////////////
	
	/////////////  SCB Contacts Ends///////////////////////
	
	
	///////////// Legal Entity Ends Here //////////////////
	
	
	public AnonType_EAFMessage generateEAFMessage() {
	   	return AnonType_EAFMessage.newBuilder()
	   	 .setEAFFinalAssessmentRating("2")
	   	 .setEAFStatus(faker.options().option(GeneralStatus.class).name())
	   	 .setEAFNextReviewDate(faker.date().future(365,TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
	   	 .build(); 	
   }
	
	
	
					/////////////  BusinessAcceptance //////////////////
	
	public AnonType_BusinessAcceptanceMessage generateBusinessAcceptanceMessage() {
	   	return AnonType_BusinessAcceptanceMessage.newBuilder()
	   	 .setReviewOutcome(faker.options().option(WorkflowStatus.class).name())
	   	 .setReferralReason(faker.options().option(Reason.class).name())
	   	 .setReviewNotes("Review Notes will go here")
	   	 .build(); 	
    }
	
				/////////////  BusinessAcceptance Ends //////////////////

	public AnonType_ESRAMessage generateESRAMessage() {
		return AnonType_ESRAMessage.newBuilder()
				 .setESRANextReviewDate(faker.date().future(365,TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
			   	 .setESRAOverallOutcome("2")
			   	 .setESRAStatus(faker.options().option(GeneralStatus.class).name())
				 .build();
	}
	
	// Omitted
	////////  TaxInfoMessage /////////////
	//	public AnonType_TaxInfoMessage generateTaxInfoMessage() {
	//		return null;
	//	}
		
	///////// TaxInfoMessage Ends ////////// 

	////////  RegulatoryClassifications /////////
	
	public AnonType_MIFIDRegulatoryClassificationsMessage generateMIFIDRegulatoryClassificationsMessage() {
		return AnonType_MIFIDRegulatoryClassificationsMessage.newBuilder()
				.setMIFIDEntityType(faker.options().option(EntityType.entityType))
				.setIsTheBalanceSheetAtleastEUR20Mio(faker.options().option(YesNo.class).name())
				.setIsTheNetTurnoverAtleastEUR40Mio(faker.options().option(YesNo.class).name())
				.setAreOwnFundsAtleastEUR2Mio(faker.options().option(YesNo.class).name())
				.setMIFIDClassification(faker.options().option(Classification.class).name())
				.setOverrideMIFIDClassification(None.NONE.name())
				.setClientHasExpertiseAndKnowledge(None.NONE.name())
				.setClientCapableOnInvestmentDecisionAndRisk(None.NONE.name())
				.setClientFinancialPortfolioExceedsGBP10Mio(None.NONE.name())
				.setQnAClientAverageTransactionFreqMoreThan10PerQtr(None.NONE.name())
				.setQnAAuthorizedPersonHasRequiredKnowledge(None.NONE.name())
				.setQnAIsClientAdministeringAuthority(None.NONE.name())
				.setQnBClientFinancialPortfolioExceedsEUR500K(None.NONE.name())
				.setQnBClientAverageTransactionFreqMoreThan10PerQtr(None.NONE.name())
				.setQnBAuthorizedPersonHasRequiredKnowledge(None.NONE.name())
				.setMIFIDStatus(None.NONE.name())
				.build();
	}
	public AnonType_RegulatoryClassificationsMessage generateRegulatoryClassificationsMessage() {
		return AnonType_RegulatoryClassificationsMessage.newBuilder()
				.setMIFID(generateMIFIDRegulatoryClassificationsMessage())
				.build();
	}

	////////  RegulatoryClassifications Ends /////////
	
	
	////// Trading Entity //////
	
	public AnonType_TradingEntitiesMessage generateTradingEntitiesMessage() {
		
		List<AnonType_TradingEntityTradingEntitiesMessage> tradingEntities= new ArrayList<AnonType_TradingEntityTradingEntitiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> tradingEntities.add(generateTradingEntityTradingEntitiesMessage()));
		
		return AnonType_TradingEntitiesMessage.newBuilder()
				.setTradingEntity(tradingEntities)
				.build();
	}
	
	public AnonType_TradingEntityTradingEntitiesMessage generateTradingEntityTradingEntitiesMessage() {
		return AnonType_TradingEntityTradingEntitiesMessage.newBuilder()
				.setTradingEntityID(faker.number().digits(5))
				.setTradingEntityName(faker.company().name())
				.setIMLegalEntityID(faker.number().digits(5))
				.setLegalEntityNameNonRoman(None.NONE.name())
				.setLegalEntityType(faker.options().option(EntityType.entityType))
				.setRequestType(faker.options().option(Request.class).name())
				.setPriority(faker.options().option(Priority.class).name())
				.setRequestDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setTradingAgreementCategory(None.NONE.name())
				.setTradingAgreementCategory(None.NONE.name())
				.setTradingAgreementType(None.NONE.name())
				.setTradingAgreementDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setTradingAgreementCatalogNumber(None.NONE.name())
				.setTradingAgreementSupplement(None.NONE.name())
				.setTradingAgreementContractingEntity(None.NONE.name())
				.setTradingAgreementStatus(None.NONE.name())
				.setFundCodeType("3")
				.setFundCodeValue(None.NONE.name())
				.setAssociation(generateAssociationTradingEntityTradingEntitiesMessage())
				.setProducts(generateProductsTradingEntityTradingEntitiesMessage())
				.setDMUIdentifiers(generateDMUIdentifiersTradingEntityTradingEntitiesMessage())
				.setIMDetails(generateIMDetailsTradingEntityTradingEntitiesMessage())
				.build();
	}

	public AnonType_AssociationTradingEntityTradingEntitiesMessage generateAssociationTradingEntityTradingEntitiesMessage() {
		return AnonType_AssociationTradingEntityTradingEntitiesMessage.newBuilder()
				.setAssociationID(faker.number().digits(5).toString())
				.setFromEntityID(faker.number().digits(5).toString())
				.setToEntityID(faker.number().digits(5).toString())
				.setToEntityName(faker.company().name())
				.setAssociationType(faker.options().option(AssociationType.class).name())
				.build();
	}

	public AnonType_ProductProductsTradingEntityTradingEntitiesMessage generateProductProductsTradingEntityTradingEntitiesMessage() {
		return AnonType_ProductProductsTradingEntityTradingEntitiesMessage.newBuilder()
				 .setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				 .setSCBBookingEntity(faker.address().country())
				 .setSCBCountry(faker.address().country())
				 .setProductGroup(faker.number().numberBetween(101, 999))
				 .setProductType(faker.options().option(ProductType.class).name())
				 .setProductCategory("Others")
				 .setTradingLocation(faker.number().numberBetween(101, 999))
				 .setSellingLocation(faker.number().numberBetween(101, 999))
				 .setProductStatus(faker.options().option(GeneralStatus.class).name())
				 .setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				 .setCreatedBy(faker.options().option(UserId.class).name())
				 .setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				 .setLastUpdatedBy(faker.options().option(UserId.class).name())
				 .setRowVersion(faker.app().version())
				 .build();
	}
	
	
	public AnonType_ProductsTradingEntityTradingEntitiesMessage generateProductsTradingEntityTradingEntitiesMessage() {
		
		List<AnonType_ProductProductsTradingEntityTradingEntitiesMessage> products= new ArrayList<AnonType_ProductProductsTradingEntityTradingEntitiesMessage>();
		
		IntStream.range(0,(new Random()).nextInt(1)+1)
		.forEach(i -> products.add(generateProductProductsTradingEntityTradingEntitiesMessage()));
		
		return AnonType_ProductsTradingEntityTradingEntitiesMessage.newBuilder()
				.setProduct(products)
				.build();
	}
	
	public AnonType_DMUIdentifiersTradingEntityTradingEntitiesMessage generateDMUIdentifiersTradingEntityTradingEntitiesMessage() {
		
		List<AnonType_DMUIdentifierDMUIdentifiersTradingEntityTradingEntitiesMessage> DMUIdentifiers= new ArrayList<AnonType_DMUIdentifierDMUIdentifiersTradingEntityTradingEntitiesMessage>();
		
		IntStream.range(0,(new Random()).nextInt(1)+1)
		.forEach(i -> DMUIdentifiers.add(generateDMUIdentifierDMUIdentifiersTradingEntityTradingEntitiesMessage()));
		
		return AnonType_DMUIdentifiersTradingEntityTradingEntitiesMessage.newBuilder()
				.setDMUIdentifier(DMUIdentifiers)
				.build();
	}
	

	public AnonType_DMUIdentifierDMUIdentifiersTradingEntityTradingEntitiesMessage generateDMUIdentifierDMUIdentifiersTradingEntityTradingEntitiesMessage() {
		return AnonType_DMUIdentifierDMUIdentifiersTradingEntityTradingEntitiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setSourceSystem("SourceSystem")
				.setTargetSystem("TargetSystem")
				.setBusinessEntityType(faker.options().option(EntityType.entityType))
				.setTargetSystemID("T001")
				.setTargetSystemSubID("TSUB001")
				.setIsPrimary("1")
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
	public AnonType_IMDetailsTradingEntityTradingEntitiesMessage generateIMDetailsTradingEntityTradingEntitiesMessage() {
		return AnonType_IMDetailsTradingEntityTradingEntitiesMessage.newBuilder()
				.setLegalEntityType(faker.options().option(EntityType.entityType))
				.setSegmentCode(faker.options().option(SegmentCode.class).name())
				.setSubSegmentCode(faker.options().option(SegmentCode.class).name())
				.setAddresses(generateAddressesIMDetailsTradingEntityTradingEntitiesMessage())
				.build();
	}
	
	public AnonType_AddressesIMDetailsTradingEntityTradingEntitiesMessage generateAddressesIMDetailsTradingEntityTradingEntitiesMessage() {
		
		List<AnonType_AddressAddressesIMDetailsTradingEntityTradingEntitiesMessage> Addresses = new ArrayList<AnonType_AddressAddressesIMDetailsTradingEntityTradingEntitiesMessage>();
		
		IntStream.range(0,(new Random()).nextInt(1)+1)
		.forEach(i -> Addresses.add(generateAddressAddressesIMDetailsTradingEntityTradingEntitiesMessage()));
		
		return AnonType_AddressesIMDetailsTradingEntityTradingEntitiesMessage.newBuilder()
				.setAddress(Addresses)
				.build();
	}
	
	public AnonType_AddressAddressesIMDetailsTradingEntityTradingEntitiesMessage generateAddressAddressesIMDetailsTradingEntityTradingEntitiesMessage() {
		return AnonType_AddressAddressesIMDetailsTradingEntityTradingEntitiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setAddressType(faker.options().option(AddressType.class).name())
				.setLine1(faker.address().streetAddress())
				.setLine2(faker.address().secondaryAddress())
				.setTown(faker.address().streetName())
				.setZipCode(faker.address().zipCode())
				.setState(faker.address().state())
				.setCountryCode(faker.address().countryCode())
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}

	//////// Trading Entity Ends ////////////////
	
	///////////  Cases ////////////////////////////////
	public AnonType_CasesMessage generateCasesMessage() {
		
		List<AnonType_CaseCasesMessage> cases = new ArrayList<AnonType_CaseCasesMessage>();
		
		IntStream.range(0,(new Random()).nextInt(1)+1)
		.forEach(i -> cases.add(generateCaseCasesMessage()));
		
		return AnonType_CasesMessage.newBuilder()
				.setCase$(cases)
				.build();
	}
	
	
	public AnonType_CaseCasesMessage generateCaseCasesMessage() {
		return AnonType_CaseCasesMessage.newBuilder()
				.setCaseID(faker.number().digits(5))
				.setCaseName("CaseName")
				.setPriority("MEDIUM")
				.setBusinessUnit("Business Unit1")
				.setCaseType("TYPE1")
				.setCaseStatus("ACTIVE")
				.setCaseStatusModifiedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreationDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setRequestDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCaseOriginalTargetCompletionDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setReason("REASON1")
				.setCancelReason("CANCEL_RSN1")
				.setComments("Case raised for website support")
				.setRequestType("REQTYPE1")
				.setOnBehalfOf("2")
				.setFromOfficeArea("8900")
				.setCreatedDate("DateIsString")
				.setCreatedBy(0)
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.setReferals(generateReferalsCaseCasesMessage())
				.setAreas(generateAreasCaseCasesMessage())
				.setManagerReviews(generateManagerReviewsCaseCasesMessage())
				.build();
	}
	
	public AnonType_ReferalsCaseCasesMessage generateReferalsCaseCasesMessage() {
		
		List<AnonType_ReferalReferalsCaseCasesMessage> referrals = new ArrayList<AnonType_ReferalReferalsCaseCasesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> referrals.add(generateReferalReferalsCaseCasesMessage()));
		
		return AnonType_ReferalsCaseCasesMessage.newBuilder()
				.setReferal(referrals)
				.build();
	}
	
	
	AnonType_ReferalReferalsCaseCasesMessage generateReferalReferalsCaseCasesMessage() {
		return AnonType_ReferalReferalsCaseCasesMessage.newBuilder()
		.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
		.setReferralReason("Referral Reason 1")
		.build();
	}
	
	

	public AnonType_AreasCaseCasesMessage generateAreasCaseCasesMessage() {
		
		List<AnonType_AreaAreasCaseCasesMessage> areas = new ArrayList<AnonType_AreaAreasCaseCasesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> areas.add(generateAreaAreasCaseCasesMessage()));
		
		return AnonType_AreasCaseCasesMessage.newBuilder()
				.setArea(areas)
				.build();
	}
	
	
	AnonType_AreaAreasCaseCasesMessage generateAreaAreasCaseCasesMessage() {
		return AnonType_AreaAreasCaseCasesMessage.newBuilder()
		.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
		.setAreaDescription("Area Description")
		.build();
	}
	
	public AnonType_ManagerReviewsCaseCasesMessage generateManagerReviewsCaseCasesMessage() {
		
		List<AnonType_ReviewManagerReviewsCaseCasesMessage> managerReviews = new ArrayList<AnonType_ReviewManagerReviewsCaseCasesMessage>();
		
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> managerReviews.add(generateReviewManagerReviewsCaseCasesMessage()));
		
		return AnonType_ManagerReviewsCaseCasesMessage.newBuilder()
				.setReview(managerReviews)
				.build();
	}
	
	
	AnonType_ReviewManagerReviewsCaseCasesMessage generateReviewManagerReviewsCaseCasesMessage() {
		return AnonType_ReviewManagerReviewsCaseCasesMessage.newBuilder()
		.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
		.setReviewer("Reviewer 1")
		.setReviewDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
		.setReviewOutcome("GOOD")
		.build();
	}
	
	
	///////////  Case End ////////////////////////////
	
	////////// Related Parties /////////////////
	
	public AnonType_RelatedPartiesMessage generateRelatedPartiesMessage() {
		return AnonType_RelatedPartiesMessage.newBuilder()
			.setIndividuals(generateIndividualsRelatedPartiesMessage())
			.setOrganizations(generateOrganizationsRelatedPartiesMessage())
			.build();
	}
	
	
	public AnonType_IndividualsRelatedPartiesMessage generateIndividualsRelatedPartiesMessage() {
		
		List<AnonType_IndividualIndividualsRelatedPartiesMessage> individuals = new ArrayList<AnonType_IndividualIndividualsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> individuals.add(generateIndividualIndividualsRelatedPartiesMessage()));
				
		return AnonType_IndividualsRelatedPartiesMessage.newBuilder()
				.setIndividual(individuals)
				.build();
	}
	
	
	public AnonType_OrganizationsRelatedPartiesMessage generateOrganizationsRelatedPartiesMessage() {
		
		List<AnonType_OrganizationOrganizationsRelatedPartiesMessage> organizations = new ArrayList<AnonType_OrganizationOrganizationsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> organizations.add(generateOrganizationOrganizationsRelatedPartiesMessage()));
				
		return AnonType_OrganizationsRelatedPartiesMessage.newBuilder()
				.setOrganization(organizations)
				.build();
	}
	 
	
					//////////////  Individual ////////////////////////
	
	public AnonType_IndividualIndividualsRelatedPartiesMessage generateIndividualIndividualsRelatedPartiesMessage() {
		return AnonType_IndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setLegalEntityID(faker.number().digits(5))
				.setIndividualName(faker.name().fullName())
				.setEntityType("INDIVIDUAL")
				.setTitle(faker.name().title())
				.setBusinessTitle(faker.job().title())
				.setFirstName(faker.superhero().name()) 
				.setMiddleName("midname")
				.setLastName(faker.name().lastName())
				.setAlias("Alias")
				.setDateofBirth(faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setNationality(faker.address().countryCode())
				.setTypeCode("Type")
				.setIdentifierValue(Integer.toString(faker.number().randomDigitNotZero()))
				.setCountryofBirth(faker.address().countryCode())
				.setPhoneNumberAreaCode(faker.number().digits(2)) 
				.setPhoneNumberCountry(faker.phoneNumber().phoneNumber().toString())  
				.setWorkPhone(faker.phoneNumber().phoneNumber().toString()) 
				.setHomePhone(faker.phoneNumber().phoneNumber().toString())  
				.setMobile(faker.phoneNumber().phoneNumber().toString())
				.setEntityStatus("ACTIVE")
				.setRoles(generateRolesIndividualIndividualsRelatedPartiesMessage())
				.setAdditionalAliases(generateAdditionalAliasesIndividualIndividualsRelatedPartiesMessage())
				.setAdditionalNationalities(generateAdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage())
				.setIdentifiers(generateIdentifiersIndividualIndividualsRelatedPartiesMessage())
				.setAddresses(generateAddressesIndividualIndividualsRelatedPartiesMessage())
				.setTaxIdentifiers(generateTaxIdentifiersIndividualIndividualsRelatedPartiesMessage())
				.setContacts(generateContactsIndividualIndividualsRelatedPartiesMessage())
				.build();
	}
	
	
	public AnonType_RolesIndividualIndividualsRelatedPartiesMessage generateRolesIndividualIndividualsRelatedPartiesMessage() {
		
		List<AnonType_RoleRolesIndividualIndividualsRelatedPartiesMessage> roles = new ArrayList<AnonType_RoleRolesIndividualIndividualsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> roles.add(generateRoleRolesIndividualIndividualsRelatedPartiesMessage()));
				
		return AnonType_RolesIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setRole(roles)
				.build();
	}
	
	public AnonType_RoleRolesIndividualIndividualsRelatedPartiesMessage generateRoleRolesIndividualIndividualsRelatedPartiesMessage() {
		return AnonType_RoleRolesIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setLegalEntityRole("ROLE1")
				.build();
				
	}
	
	public  AnonType_AdditionalAliasesIndividualIndividualsRelatedPartiesMessage generateAdditionalAliasesIndividualIndividualsRelatedPartiesMessage()
	{
		List<AnonType_AdditionalAliasAdditionalAliasesIndividualIndividualsRelatedPartiesMessage> aliases= new ArrayList<AnonType_AdditionalAliasAdditionalAliasesIndividualIndividualsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> aliases.add(generateAdditionalAliasAdditionalAliasesIndividualIndividualsRelatedPartiesMessage()));
		
		return AnonType_AdditionalAliasesIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setAdditionalAlias(aliases)
				.build();
		
	}
	
	public AnonType_AdditionalAliasAdditionalAliasesIndividualIndividualsRelatedPartiesMessage generateAdditionalAliasAdditionalAliasesIndividualIndividualsRelatedPartiesMessage() {
		return AnonType_AdditionalAliasAdditionalAliasesIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber("1")
				.setAdditionalAliasName("AliasName")
				.build();
	}
	
	public  AnonType_AdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage generateAdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage()
	{
		List<AnonType_AdditionalNationalityAdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage> nationalities= new ArrayList<AnonType_AdditionalNationalityAdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> nationalities.add(generateAdditionalNationalityAdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage()));
		
		return AnonType_AdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setAdditionalNationality(nationalities)
				.build();
		
	}
	
	public AnonType_AdditionalNationalityAdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage generateAdditionalNationalityAdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage() {
		return AnonType_AdditionalNationalityAdditionalNationalitiesIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber("1")
				.setAdditionalNationality("UK")
				.build();
	}
	
	public  AnonType_IdentifiersIndividualIndividualsRelatedPartiesMessage generateIdentifiersIndividualIndividualsRelatedPartiesMessage()
	{
		List<AnonType_IdentifierIdentifiersIndividualIndividualsRelatedPartiesMessage> identifiers= new ArrayList<AnonType_IdentifierIdentifiersIndividualIndividualsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> identifiers.add(generateIdentifierIdentifiersIndividualIndividualsRelatedPartiesMessage()));
		
		return AnonType_IdentifiersIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setIdentifier(identifiers)
				.build();
		
	}
	
	public AnonType_IdentifierIdentifiersIndividualIndividualsRelatedPartiesMessage generateIdentifierIdentifiersIndividualIndividualsRelatedPartiesMessage() {
		return AnonType_IdentifierIdentifiersIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber("1")
				.setTypeCode("TYPECODE1")
				.setValue("ID00001")
				.build();
	}
	
	public AnonType_AddressesIndividualIndividualsRelatedPartiesMessage generateAddressesIndividualIndividualsRelatedPartiesMessage() {
		
		List<AnonType_AddressAddressesIndividualIndividualsRelatedPartiesMessage> Addresses = new ArrayList<AnonType_AddressAddressesIndividualIndividualsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> Addresses.add(generateAddressAddressesIndividualIndividualsRelatedPartiesMessage()));
		
		return AnonType_AddressesIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setAddress(Addresses)
				.build();
	}
	
	public AnonType_AddressAddressesIndividualIndividualsRelatedPartiesMessage generateAddressAddressesIndividualIndividualsRelatedPartiesMessage() {
		return AnonType_AddressAddressesIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setAddressType(faker.options().option(AddressType.class).name())
				.setLine1(faker.address().streetAddress())
				.setLine2(faker.address().secondaryAddress())
				.setTown(faker.address().streetName())
				.setState(faker.address().state())
				.setZipCode(faker.address().zipCode())
				.setCountryCode(faker.address().countryCode())
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())				
				.build();
	}
	
	public AnonType_TaxIdentifiersIndividualIndividualsRelatedPartiesMessage generateTaxIdentifiersIndividualIndividualsRelatedPartiesMessage() {
		
		List<AnonType_TaxIdentifierTaxIdentifiersIndividualIndividualsRelatedPartiesMessage> taxIdentifiers = new ArrayList<AnonType_TaxIdentifierTaxIdentifiersIndividualIndividualsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> taxIdentifiers.add(generateTaxIdentifierTaxIdentifiersIndividualIndividualsRelatedPartiesMessage()));
		
		return AnonType_TaxIdentifiersIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setTaxIdentifier(taxIdentifiers)
				.build();
	}
	
	public AnonType_TaxIdentifierTaxIdentifiersIndividualIndividualsRelatedPartiesMessage generateTaxIdentifierTaxIdentifiersIndividualIndividualsRelatedPartiesMessage() {
		return AnonType_TaxIdentifierTaxIdentifiersIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber("1")
				.setTaxIdentifierStatus("100001")
				.setTaxIdentifierType(Integer.toString(faker.number().randomDigit()))
				.setTaxIDNumber(faker.number().digits(5))
				.setTaxCountry(faker.address().countryCode())
				.setTaxIdentifierStatus("ACTIVE")
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())				
				.build();
	}
	
	public AnonType_ContactsIndividualIndividualsRelatedPartiesMessage generateContactsIndividualIndividualsRelatedPartiesMessage() {
		
		List<AnonType_ContactContactsIndividualIndividualsRelatedPartiesMessage> contacts = new ArrayList<AnonType_ContactContactsIndividualIndividualsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> contacts.add(generateContactContactsIndividualIndividualsRelatedPartiesMessage()));
				
		return AnonType_ContactsIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setContact(contacts)
				.build();
	}
	
	public AnonType_ContactContactsIndividualIndividualsRelatedPartiesMessage generateContactContactsIndividualIndividualsRelatedPartiesMessage() {
		return AnonType_ContactContactsIndividualIndividualsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setContactPersonType("RELATIONS")
				.setTitle(faker.name().title())
				.setBusinessTitle(faker.job().title())
				.setFirstName(faker.superhero().name()) 
				.setLastName(faker.name().lastName())
				.setContactStatus("ACTIVE") 
				.setEmailAddress(faker.internet().emailAddress()) 
				.setPrimaryPhoneType("OFFICE") 
				.setPhoneNumberAreaCode(faker.number().digits(2)) 
				.setPhoneNumberCountry(faker.phoneNumber().phoneNumber().toString())  
				.setWorkPhone(faker.phoneNumber().phoneNumber().toString()) 
				.setHomePhone(faker.phoneNumber().phoneNumber().toString())  
				.setMobile(faker.phoneNumber().phoneNumber().toString())
				.setFax(faker.phoneNumber().phoneNumber().toString())
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))				
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
	
						//////////////// Organization ////////////////////////////
	
	public AnonType_OrganizationOrganizationsRelatedPartiesMessage generateOrganizationOrganizationsRelatedPartiesMessage() {
		return AnonType_OrganizationOrganizationsRelatedPartiesMessage.newBuilder()
						.setLegalEntityID(faker.number().digits(5))
						.setLegalEntityName(faker.company().name())
						.setPreviousNames("NONE")
						.setPreviousNameDate(0)
						.setShortName("NONE")
						.setMaskedName("NONE")
						.setTradingAsName("NONE")
						.setLEI("LEI0000001")
						.setCIK("CIK0000001")
						.setPrimaryListedExchange("SP100")
						.setOwnershipStatus("ACTIVE")
						.setEntityType(faker.options().option(EntityType.entityType))
						.setOrganisationUnitType(faker.options().option(OrganizationUnitType.class).name())
						.setLegalEntityType(faker.options().option(EntityType.entityType))
						.setCountryOfDomicile(faker.address().country())
						.setCountryOfIncorporation(faker.address().country())
						.setDateOfIncorporation(faker.date().past(3650, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
						.setIncorporationNumber(faker.business().creditCardNumber())
						.setOperationalRegistrationNumber(faker.business().creditCardNumber())
						.setPhoneNumberAreaCode(faker.number().digits(2)) 
						.setPhoneNumberCountry(faker.phoneNumber().phoneNumber().toString())  
						.setClientStatus(faker.options().option(GeneralStatus.class).name())
						.setEntityStatus(faker.options().option(GeneralStatus.class).name())
						.setCDDStatus("ACTIVE")
						.setAdditionalAliases(generateAdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage())
						.setRoles(generateRolesOrganizationOrganizationsRelatedPartiesMessage())
						.setExecutives(generateExecutivesOrganizationOrganizationsRelatedPartiesMessage())
						.setRepresentors(generateRepresentorsOrganizationOrganizationsRelatedPartiesMessage())
						.setAddresses(generateAddressesOrganizationOrganizationsRelatedPartiesMessage())
						.setTaxIdentifiers(generateTaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage())
						.setContacts(generateContactsOrganizationOrganizationsRelatedPartiesMessage())
						.build();
	}
						
	public  AnonType_AdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage generateAdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage()
	{
		List<AnonType_AdditionalAliasAdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage> aliases = new ArrayList<AnonType_AdditionalAliasAdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> aliases.add(generateAdditionalAliasAdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage()));
		
		return AnonType_AdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setAdditionalAlias(aliases)
				.build();
		
	}
	
	public AnonType_AdditionalAliasAdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage generateAdditionalAliasAdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage() {
		return AnonType_AdditionalAliasAdditionalAliasesOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber("1")
				.setAdditionalAliasName("AliasName")
				.build();
	}
	
	
	public AnonType_RolesOrganizationOrganizationsRelatedPartiesMessage generateRolesOrganizationOrganizationsRelatedPartiesMessage() {
		
		List<AnonType_RoleRolesOrganizationOrganizationsRelatedPartiesMessage> roles = new ArrayList<AnonType_RoleRolesOrganizationOrganizationsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> roles.add(generateRoleRolesOrganizationOrganizationsRelatedPartiesMessage()));
				
		return AnonType_RolesOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setRole(roles)
				.build();
	}
	
	public AnonType_RoleRolesOrganizationOrganizationsRelatedPartiesMessage generateRoleRolesOrganizationOrganizationsRelatedPartiesMessage() {
		return AnonType_RoleRolesOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setLegalEntityRole("ROLE1")
				.build();
	}
	
	public AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage generateExecutivesOrganizationOrganizationsRelatedPartiesMessage() {
		
		List<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> executives = new ArrayList<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> executives.add(generateExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage()));
				
		return AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setExecutive(executives)
				.build();
	}
	
	public AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage generateExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage() {
		return AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setNamesOfExecutiveManagers(faker.name().fullName())
				.build();
	}
	
	public AnonType_RepresentorsOrganizationOrganizationsRelatedPartiesMessage generateRepresentorsOrganizationOrganizationsRelatedPartiesMessage() {
		
		List<AnonType_RepresentorRepresentorsOrganizationOrganizationsRelatedPartiesMessage> representors = new ArrayList<AnonType_RepresentorRepresentorsOrganizationOrganizationsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> representors.add(generateRepresentorRepresentorsOrganizationOrganizationsRelatedPartiesMessage()));
				
		return AnonType_RepresentorsOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setRepresentor(representors)
				.build();
	}
	
	public AnonType_RepresentorRepresentorsOrganizationOrganizationsRelatedPartiesMessage generateRepresentorRepresentorsOrganizationOrganizationsRelatedPartiesMessage() {
		return AnonType_RepresentorRepresentorsOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setPersonsRepresentingTheEntity(faker.name().fullName())
				.build();
	}
	
public AnonType_AddressesOrganizationOrganizationsRelatedPartiesMessage generateAddressesOrganizationOrganizationsRelatedPartiesMessage() {
		
		List<AnonType_AddressAddressesOrganizationOrganizationsRelatedPartiesMessage> Addresses = new ArrayList<AnonType_AddressAddressesOrganizationOrganizationsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> Addresses.add(generateAddressAddressesOrganizationOrganizationsRelatedPartiesMessage()));
		
		return AnonType_AddressesOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setAddress(Addresses)
				.build();
	}
	
	public AnonType_AddressAddressesOrganizationOrganizationsRelatedPartiesMessage generateAddressAddressesOrganizationOrganizationsRelatedPartiesMessage() {
		return AnonType_AddressAddressesOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setAddressType(faker.options().option(AddressType.class).name())
				.setLine1(faker.address().streetAddress())
				.setLine2(faker.address().secondaryAddress())
				.setTown(faker.address().streetName())
				.setState(faker.address().state())
				.setZipCode(faker.address().zipCode())
				.setCountryCode(faker.address().countryCode())
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())				
				.build();
	}
	
public AnonType_TaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage generateTaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage() {
		
		List<AnonType_TaxIdentifierTaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage> taxIdentifiers = new ArrayList<AnonType_TaxIdentifierTaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> taxIdentifiers.add(generateTaxIdentifierTaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage()));
		
		return AnonType_TaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setTaxIdentifier(taxIdentifiers)
				.build();
	}
	
	public AnonType_TaxIdentifierTaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage generateTaxIdentifierTaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage() {
		return AnonType_TaxIdentifierTaxIdentifiersOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber("1")
				.setTaxIdentifierStatus("100001")
				.setTaxIdentifierType(Integer.toString(faker.number().randomDigit()))
				.setTaxIDNumber(faker.number().digits(5))
				.setTaxCountry(faker.address().countryCode())
				.setTaxIdentifierStatus("ACTIVE")
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())				
				.build();
	}
	
public AnonType_ContactsOrganizationOrganizationsRelatedPartiesMessage generateContactsOrganizationOrganizationsRelatedPartiesMessage() {
		
		List<AnonType_ContactContactsOrganizationOrganizationsRelatedPartiesMessage> contacts = new ArrayList<AnonType_ContactContactsOrganizationOrganizationsRelatedPartiesMessage>();
		
		IntStream.range(0,1)
		.forEach(i -> contacts.add(generateContactContactsOrganizationOrganizationsRelatedPartiesMessage()));
				
		return AnonType_ContactsOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setContact(contacts)
				.build();
	}
	
	public AnonType_ContactContactsOrganizationOrganizationsRelatedPartiesMessage generateContactContactsOrganizationOrganizationsRelatedPartiesMessage() {
		return AnonType_ContactContactsOrganizationOrganizationsRelatedPartiesMessage.newBuilder()
				.setSequenceNumber(Integer.toString(faker.number().randomDigit()))
				.setContactPersonType("RELATIONS")
				.setTitle(faker.name().title())
				.setBusinessTitle(faker.job().title())
				.setFirstName(faker.superhero().name()) 
				.setLastName(faker.name().lastName())
				.setContactStatus("ACTIVE") 
				.setEmailAddress(faker.internet().emailAddress()) 
				.setPrimaryPhoneType("OFFICE") 
				.setPhoneNumberAreaCode(faker.number().digits(2)) 
				.setPhoneNumberCountry(faker.phoneNumber().phoneNumber().toString())  
				.setWorkPhone(faker.phoneNumber().phoneNumber().toString()) 
				.setHomePhone(faker.phoneNumber().phoneNumber().toString())  
				.setMobile(faker.phoneNumber().phoneNumber().toString())
				.setFax(faker.phoneNumber().phoneNumber().toString())
				.setCreatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setCreatedBy(faker.options().option(UserId.class).name())
				.setLastUpdatedDate(faker.date().past(365, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).getLong(ChronoField.EPOCH_DAY))
				.setLastUpdatedBy(faker.options().option(UserId.class).name())
				.setRowVersion(faker.app().version())
				.build();
	}
				//////////////   Organizations Ends /////////////////////////
	
	//////////Related Parties Ends /////////////////
	
	 
	
}


