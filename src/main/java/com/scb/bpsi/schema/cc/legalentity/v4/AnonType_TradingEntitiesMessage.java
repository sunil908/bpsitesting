/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.scb.bpsi.schema.cc.legalentity.v4;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class AnonType_TradingEntitiesMessage extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 202763508718136318L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"AnonType_TradingEntitiesMessage\",\"namespace\":\"com.scb.bpsi.schema.cc.legalentity.v4\",\"fields\":[{\"name\":\"TradingEntity\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"AnonType_TradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"TradingEntityID\",\"type\":\"string\",\"source\":\"element TradingEntityID\"},{\"name\":\"TradingEntityName\",\"type\":\"string\",\"source\":\"element TradingEntityName\"},{\"name\":\"IMLegalEntityID\",\"type\":\"string\",\"source\":\"element IMLegalEntityID\"},{\"name\":\"LegalEntityNameNonRoman\",\"type\":\"string\",\"source\":\"element LegalEntityNameNonRoman\"},{\"name\":\"LegalEntityType\",\"type\":\"string\",\"source\":\"element LegalEntityType\"},{\"name\":\"RequestType\",\"type\":\"string\",\"source\":\"element RequestType\"},{\"name\":\"Priority\",\"type\":\"string\",\"source\":\"element Priority\"},{\"name\":\"RequestDate\",\"type\":\"long\",\"source\":\"element RequestDate\"},{\"name\":\"TradingAgreementCategory\",\"type\":\"string\",\"source\":\"element TradingAgreementCategory\"},{\"name\":\"TradingAgreementType\",\"type\":\"string\",\"source\":\"element TradingAgreementType\"},{\"name\":\"TradingAgreementDate\",\"type\":\"long\",\"source\":\"element TradingAgreementDate\"},{\"name\":\"TradingAgreementCatalogNumber\",\"type\":\"string\",\"source\":\"element TradingAgreementCatalogNumber\"},{\"name\":\"TradingAgreementSupplement\",\"type\":\"string\",\"source\":\"element TradingAgreementSupplement\"},{\"name\":\"TradingAgreementContractingEntity\",\"type\":\"string\",\"source\":\"element TradingAgreementContractingEntity\"},{\"name\":\"TradingAgreementStatus\",\"type\":\"string\",\"source\":\"element TradingAgreementStatus\"},{\"name\":\"FundCodeType\",\"type\":\"string\",\"source\":\"element FundCodeType\"},{\"name\":\"FundCodeValue\",\"type\":\"string\",\"source\":\"element FundCodeValue\"},{\"name\":\"Association\",\"type\":{\"type\":\"record\",\"name\":\"AnonType_AssociationTradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"AssociationID\",\"type\":\"string\",\"source\":\"element AssociationID\"},{\"name\":\"FromEntityID\",\"type\":\"string\",\"source\":\"element FromEntityID\"},{\"name\":\"ToEntityID\",\"type\":\"string\",\"source\":\"element ToEntityID\"},{\"name\":\"ToEntityName\",\"type\":\"string\",\"source\":\"element ToEntityName\"},{\"name\":\"AssociationType\",\"type\":\"string\",\"source\":\"element AssociationType\"}]},\"source\":\"element Association\"},{\"name\":\"Products\",\"type\":{\"type\":\"record\",\"name\":\"AnonType_ProductsTradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"Product\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"AnonType_ProductProductsTradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"SequenceNumber\",\"type\":\"string\",\"source\":\"element SequenceNumber\"},{\"name\":\"SCBBookingEntity\",\"type\":\"string\",\"source\":\"element SCBBookingEntity\"},{\"name\":\"SCBCountry\",\"type\":\"string\",\"source\":\"element SCBCountry\"},{\"name\":\"ProductGroup\",\"type\":\"double\",\"source\":\"element ProductGroup\"},{\"name\":\"ProductType\",\"type\":\"string\",\"source\":\"element ProductType\"},{\"name\":\"ProductCategory\",\"type\":\"string\",\"source\":\"element ProductCategory\"},{\"name\":\"TradingLocation\",\"type\":\"double\",\"source\":\"element TradingLocation\"},{\"name\":\"SellingLocation\",\"type\":\"double\",\"source\":\"element SellingLocation\"},{\"name\":\"ProductStatus\",\"type\":\"string\",\"source\":\"element ProductStatus\"},{\"name\":\"CreatedDate\",\"type\":\"long\",\"source\":\"element CreatedDate\"},{\"name\":\"CreatedBy\",\"type\":\"string\",\"source\":\"element CreatedBy\"},{\"name\":\"LastUpdatedDate\",\"type\":\"long\",\"source\":\"element LastUpdatedDate\"},{\"name\":\"LastUpdatedBy\",\"type\":\"string\",\"source\":\"element LastUpdatedBy\"},{\"name\":\"RowVersion\",\"type\":\"string\",\"source\":\"element RowVersion\"}]}},\"source\":\"element Product\"}]},\"source\":\"element Products\"},{\"name\":\"DMUIdentifiers\",\"type\":{\"type\":\"record\",\"name\":\"AnonType_DMUIdentifiersTradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"DMUIdentifier\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"AnonType_DMUIdentifierDMUIdentifiersTradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"SequenceNumber\",\"type\":\"string\",\"source\":\"element SequenceNumber\"},{\"name\":\"SourceSystem\",\"type\":\"string\",\"source\":\"element SourceSystem\"},{\"name\":\"TargetSystem\",\"type\":\"string\",\"source\":\"element TargetSystem\"},{\"name\":\"BusinessEntityType\",\"type\":\"string\",\"source\":\"element BusinessEntityType\"},{\"name\":\"TargetSystemID\",\"type\":\"string\",\"source\":\"element TargetSystemID\"},{\"name\":\"TargetSystemSubID\",\"type\":\"string\",\"source\":\"element TargetSystemSubID\"},{\"name\":\"IsPrimary\",\"type\":\"string\",\"source\":\"element IsPrimary\"},{\"name\":\"CreatedBy\",\"type\":\"string\",\"source\":\"element CreatedBy\"},{\"name\":\"CreatedDate\",\"type\":\"long\",\"source\":\"element CreatedDate\"},{\"name\":\"LastUpdatedBy\",\"type\":\"string\",\"source\":\"element LastUpdatedBy\"},{\"name\":\"LastUpdatedDate\",\"type\":\"long\",\"source\":\"element LastUpdatedDate\"},{\"name\":\"RowVersion\",\"type\":\"string\",\"source\":\"element RowVersion\"}]}},\"source\":\"element DMUIdentifier\"}]},\"source\":\"element DMUIdentifiers\"},{\"name\":\"IMDetails\",\"type\":{\"type\":\"record\",\"name\":\"AnonType_IMDetailsTradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"LegalEntityType\",\"type\":\"string\",\"source\":\"element LegalEntityType\"},{\"name\":\"SegmentCode\",\"type\":\"string\",\"source\":\"element SegmentCode\"},{\"name\":\"SubSegmentCode\",\"type\":\"string\",\"source\":\"element SubSegmentCode\"},{\"name\":\"Addresses\",\"type\":{\"type\":\"record\",\"name\":\"AnonType_AddressesIMDetailsTradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"Address\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"AnonType_AddressAddressesIMDetailsTradingEntityTradingEntitiesMessage\",\"fields\":[{\"name\":\"SequenceNumber\",\"type\":\"string\",\"source\":\"element SequenceNumber\"},{\"name\":\"AddressType\",\"type\":\"string\",\"source\":\"element AddressType\"},{\"name\":\"Line1\",\"type\":\"string\",\"source\":\"element Line1\"},{\"name\":\"Line2\",\"type\":\"string\",\"source\":\"element Line2\"},{\"name\":\"Town\",\"type\":\"string\",\"source\":\"element Town\"},{\"name\":\"State\",\"type\":\"string\",\"source\":\"element State\"},{\"name\":\"ZipCode\",\"type\":\"string\",\"source\":\"element ZipCode\"},{\"name\":\"CountryCode\",\"type\":\"string\",\"source\":\"element CountryCode\"},{\"name\":\"CreatedDate\",\"type\":\"long\",\"source\":\"element CreatedDate\"},{\"name\":\"CreatedBy\",\"type\":\"string\",\"source\":\"element CreatedBy\"},{\"name\":\"LastUpdatedDate\",\"type\":\"long\",\"source\":\"element LastUpdatedDate\"},{\"name\":\"LastUpdatedBy\",\"type\":\"string\",\"source\":\"element LastUpdatedBy\"},{\"name\":\"RowVersion\",\"type\":\"string\",\"source\":\"element RowVersion\"}]}},\"source\":\"element Address\"}]},\"source\":\"element Addresses\"}]},\"source\":\"element IMDetails\"}]}},\"source\":\"element TradingEntity\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<AnonType_TradingEntitiesMessage> ENCODER =
      new BinaryMessageEncoder<AnonType_TradingEntitiesMessage>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<AnonType_TradingEntitiesMessage> DECODER =
      new BinaryMessageDecoder<AnonType_TradingEntitiesMessage>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<AnonType_TradingEntitiesMessage> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<AnonType_TradingEntitiesMessage> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<AnonType_TradingEntitiesMessage> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<AnonType_TradingEntitiesMessage>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this AnonType_TradingEntitiesMessage to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a AnonType_TradingEntitiesMessage from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a AnonType_TradingEntitiesMessage instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static AnonType_TradingEntitiesMessage fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> TradingEntity;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public AnonType_TradingEntitiesMessage() {}

  /**
   * All-args constructor.
   * @param TradingEntity The new value for TradingEntity
   */
  public AnonType_TradingEntitiesMessage(java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> TradingEntity) {
    this.TradingEntity = TradingEntity;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return TradingEntity;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: TradingEntity = (java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'TradingEntity' field.
   * @return The value of the 'TradingEntity' field.
   */
  public java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> getTradingEntity() {
    return TradingEntity;
  }


  /**
   * Sets the value of the 'TradingEntity' field.
   * @param value the value to set.
   */
  public void setTradingEntity(java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> value) {
    this.TradingEntity = value;
  }

  /**
   * Creates a new AnonType_TradingEntitiesMessage RecordBuilder.
   * @return A new AnonType_TradingEntitiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder newBuilder() {
    return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder();
  }

  /**
   * Creates a new AnonType_TradingEntitiesMessage RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new AnonType_TradingEntitiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder newBuilder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder other) {
    if (other == null) {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder();
    } else {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder(other);
    }
  }

  /**
   * Creates a new AnonType_TradingEntitiesMessage RecordBuilder by copying an existing AnonType_TradingEntitiesMessage instance.
   * @param other The existing instance to copy.
   * @return A new AnonType_TradingEntitiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder newBuilder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage other) {
    if (other == null) {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder();
    } else {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder(other);
    }
  }

  /**
   * RecordBuilder for AnonType_TradingEntitiesMessage instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<AnonType_TradingEntitiesMessage>
    implements org.apache.avro.data.RecordBuilder<AnonType_TradingEntitiesMessage> {

    private java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> TradingEntity;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.TradingEntity)) {
        this.TradingEntity = data().deepCopy(fields()[0].schema(), other.TradingEntity);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
    }

    /**
     * Creates a Builder by copying an existing AnonType_TradingEntitiesMessage instance
     * @param other The existing instance to copy.
     */
    private Builder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.TradingEntity)) {
        this.TradingEntity = data().deepCopy(fields()[0].schema(), other.TradingEntity);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'TradingEntity' field.
      * @return The value.
      */
    public java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> getTradingEntity() {
      return TradingEntity;
    }


    /**
      * Sets the value of the 'TradingEntity' field.
      * @param value The value of 'TradingEntity'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder setTradingEntity(java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> value) {
      validate(fields()[0], value);
      this.TradingEntity = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'TradingEntity' field has been set.
      * @return True if the 'TradingEntity' field has been set, false otherwise.
      */
    public boolean hasTradingEntity() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'TradingEntity' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntitiesMessage.Builder clearTradingEntity() {
      TradingEntity = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public AnonType_TradingEntitiesMessage build() {
      try {
        AnonType_TradingEntitiesMessage record = new AnonType_TradingEntitiesMessage();
        record.TradingEntity = fieldSetFlags()[0] ? this.TradingEntity : (java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage>) defaultValue(fields()[0]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<AnonType_TradingEntitiesMessage>
    WRITER$ = (org.apache.avro.io.DatumWriter<AnonType_TradingEntitiesMessage>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<AnonType_TradingEntitiesMessage>
    READER$ = (org.apache.avro.io.DatumReader<AnonType_TradingEntitiesMessage>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    long size0 = this.TradingEntity.size();
    out.writeArrayStart();
    out.setItemCount(size0);
    long actualSize0 = 0;
    for (com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage e0: this.TradingEntity) {
      actualSize0++;
      out.startItem();
      e0.customEncode(out);
    }
    out.writeArrayEnd();
    if (actualSize0 != size0)
      throw new java.util.ConcurrentModificationException("Array-size written was " + size0 + ", but element count was " + actualSize0 + ".");

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      long size0 = in.readArrayStart();
      java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> a0 = this.TradingEntity;
      if (a0 == null) {
        a0 = new SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage>((int)size0, SCHEMA$.getField("TradingEntity").schema());
        this.TradingEntity = a0;
      } else a0.clear();
      SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage>)a0 : null);
      for ( ; 0 < size0; size0 = in.arrayNext()) {
        for ( ; size0 != 0; size0--) {
          com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage e0 = (ga0 != null ? ga0.peek() : null);
          if (e0 == null) {
            e0 = new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage();
          }
          e0.customDecode(in);
          a0.add(e0);
        }
      }

    } else {
      for (int i = 0; i < 1; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          long size0 = in.readArrayStart();
          java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> a0 = this.TradingEntity;
          if (a0 == null) {
            a0 = new SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage>((int)size0, SCHEMA$.getField("TradingEntity").schema());
            this.TradingEntity = a0;
          } else a0.clear();
          SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage>)a0 : null);
          for ( ; 0 < size0; size0 = in.arrayNext()) {
            for ( ; size0 != 0; size0--) {
              com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage e0 = (ga0 != null ? ga0.peek() : null);
              if (e0 == null) {
                e0 = new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_TradingEntityTradingEntitiesMessage();
              }
              e0.customDecode(in);
              a0.add(e0);
            }
          }
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










