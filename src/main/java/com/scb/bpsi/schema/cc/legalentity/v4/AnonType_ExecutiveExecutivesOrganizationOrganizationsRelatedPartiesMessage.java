/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.scb.bpsi.schema.cc.legalentity.v4;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 4353093602192212726L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage\",\"namespace\":\"com.scb.bpsi.schema.cc.legalentity.v4\",\"fields\":[{\"name\":\"SequenceNumber\",\"type\":\"string\",\"source\":\"element SequenceNumber\"},{\"name\":\"NamesOfExecutiveManagers\",\"type\":\"string\",\"source\":\"element NamesOfExecutiveManagers\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> ENCODER =
      new BinaryMessageEncoder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> DECODER =
      new BinaryMessageDecoder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.lang.CharSequence SequenceNumber;
   private java.lang.CharSequence NamesOfExecutiveManagers;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage() {}

  /**
   * All-args constructor.
   * @param SequenceNumber The new value for SequenceNumber
   * @param NamesOfExecutiveManagers The new value for NamesOfExecutiveManagers
   */
  public AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage(java.lang.CharSequence SequenceNumber, java.lang.CharSequence NamesOfExecutiveManagers) {
    this.SequenceNumber = SequenceNumber;
    this.NamesOfExecutiveManagers = NamesOfExecutiveManagers;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return SequenceNumber;
    case 1: return NamesOfExecutiveManagers;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: SequenceNumber = (java.lang.CharSequence)value$; break;
    case 1: NamesOfExecutiveManagers = (java.lang.CharSequence)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'SequenceNumber' field.
   * @return The value of the 'SequenceNumber' field.
   */
  public java.lang.CharSequence getSequenceNumber() {
    return SequenceNumber;
  }


  /**
   * Sets the value of the 'SequenceNumber' field.
   * @param value the value to set.
   */
  public void setSequenceNumber(java.lang.CharSequence value) {
    this.SequenceNumber = value;
  }

  /**
   * Gets the value of the 'NamesOfExecutiveManagers' field.
   * @return The value of the 'NamesOfExecutiveManagers' field.
   */
  public java.lang.CharSequence getNamesOfExecutiveManagers() {
    return NamesOfExecutiveManagers;
  }


  /**
   * Sets the value of the 'NamesOfExecutiveManagers' field.
   * @param value the value to set.
   */
  public void setNamesOfExecutiveManagers(java.lang.CharSequence value) {
    this.NamesOfExecutiveManagers = value;
  }

  /**
   * Creates a new AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder.
   * @return A new AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder newBuilder() {
    return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder();
  }

  /**
   * Creates a new AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder newBuilder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder other) {
    if (other == null) {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder();
    } else {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder(other);
    }
  }

  /**
   * Creates a new AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder by copying an existing AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage instance.
   * @param other The existing instance to copy.
   * @return A new AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder newBuilder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage other) {
    if (other == null) {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder();
    } else {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder(other);
    }
  }

  /**
   * RecordBuilder for AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>
    implements org.apache.avro.data.RecordBuilder<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> {

    private java.lang.CharSequence SequenceNumber;
    private java.lang.CharSequence NamesOfExecutiveManagers;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.SequenceNumber)) {
        this.SequenceNumber = data().deepCopy(fields()[0].schema(), other.SequenceNumber);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.NamesOfExecutiveManagers)) {
        this.NamesOfExecutiveManagers = data().deepCopy(fields()[1].schema(), other.NamesOfExecutiveManagers);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
    }

    /**
     * Creates a Builder by copying an existing AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage instance
     * @param other The existing instance to copy.
     */
    private Builder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.SequenceNumber)) {
        this.SequenceNumber = data().deepCopy(fields()[0].schema(), other.SequenceNumber);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.NamesOfExecutiveManagers)) {
        this.NamesOfExecutiveManagers = data().deepCopy(fields()[1].schema(), other.NamesOfExecutiveManagers);
        fieldSetFlags()[1] = true;
      }
    }

    /**
      * Gets the value of the 'SequenceNumber' field.
      * @return The value.
      */
    public java.lang.CharSequence getSequenceNumber() {
      return SequenceNumber;
    }


    /**
      * Sets the value of the 'SequenceNumber' field.
      * @param value The value of 'SequenceNumber'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder setSequenceNumber(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.SequenceNumber = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'SequenceNumber' field has been set.
      * @return True if the 'SequenceNumber' field has been set, false otherwise.
      */
    public boolean hasSequenceNumber() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'SequenceNumber' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder clearSequenceNumber() {
      SequenceNumber = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'NamesOfExecutiveManagers' field.
      * @return The value.
      */
    public java.lang.CharSequence getNamesOfExecutiveManagers() {
      return NamesOfExecutiveManagers;
    }


    /**
      * Sets the value of the 'NamesOfExecutiveManagers' field.
      * @param value The value of 'NamesOfExecutiveManagers'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder setNamesOfExecutiveManagers(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.NamesOfExecutiveManagers = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'NamesOfExecutiveManagers' field has been set.
      * @return True if the 'NamesOfExecutiveManagers' field has been set, false otherwise.
      */
    public boolean hasNamesOfExecutiveManagers() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'NamesOfExecutiveManagers' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.v4.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder clearNamesOfExecutiveManagers() {
      NamesOfExecutiveManagers = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage build() {
      try {
        AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage record = new AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage();
        record.SequenceNumber = fieldSetFlags()[0] ? this.SequenceNumber : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.NamesOfExecutiveManagers = fieldSetFlags()[1] ? this.NamesOfExecutiveManagers : (java.lang.CharSequence) defaultValue(fields()[1]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>
    WRITER$ = (org.apache.avro.io.DatumWriter<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>
    READER$ = (org.apache.avro.io.DatumReader<AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeString(this.SequenceNumber);

    out.writeString(this.NamesOfExecutiveManagers);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.SequenceNumber = in.readString(this.SequenceNumber instanceof Utf8 ? (Utf8)this.SequenceNumber : null);

      this.NamesOfExecutiveManagers = in.readString(this.NamesOfExecutiveManagers instanceof Utf8 ? (Utf8)this.NamesOfExecutiveManagers : null);

    } else {
      for (int i = 0; i < 2; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.SequenceNumber = in.readString(this.SequenceNumber instanceof Utf8 ? (Utf8)this.SequenceNumber : null);
          break;

        case 1:
          this.NamesOfExecutiveManagers = in.readString(this.NamesOfExecutiveManagers instanceof Utf8 ? (Utf8)this.NamesOfExecutiveManagers : null);
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










