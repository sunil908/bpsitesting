package com.scb.bpsi.schema.cc.legalentity.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.schema.cc.legalentity.AnonType_HeaderMessage;

public class AnonType_HeaderMessage_EventsGenerator implements EventsProduction {

	@Override
	@SuppressWarnings("unchecked")
	public List<AnonType_HeaderMessage> eventsGenerator(int numOfEvents) {
		
		List<AnonType_HeaderMessage> events=new ArrayList<>();
		
		EventGenerationImpl event = new EventGenerationImpl();
	
    	IntStream.range(0,numOfEvents)
		.forEach(i -> {
						events.add(event.generateHeaderMessage());
						});

    	return events;
	}
}
