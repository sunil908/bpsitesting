/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.scb.bpsi.schema.cc.legalentity.v4;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class AnonType_IdentifiersExternalIdentifierLegalEntityMessage extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 8724037632380746145L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"AnonType_IdentifiersExternalIdentifierLegalEntityMessage\",\"namespace\":\"com.scb.bpsi.schema.cc.legalentity.v4\",\"fields\":[{\"name\":\"Identifier\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage\",\"fields\":[{\"name\":\"SequenceNumber\",\"type\":\"string\",\"source\":\"element SequenceNumber\"},{\"name\":\"SystemName\",\"type\":\"string\",\"source\":\"element SystemName\"},{\"name\":\"SystemRefID\",\"type\":\"string\",\"source\":\"element SystemRefID\"},{\"name\":\"CreatedDate\",\"type\":\"long\",\"source\":\"element CreatedDate\"},{\"name\":\"CreatedBy\",\"type\":\"string\",\"source\":\"element CreatedBy\"},{\"name\":\"LastUpdatedDate\",\"type\":\"long\",\"source\":\"element LastUpdatedDate\"},{\"name\":\"LastUpdatedBy\",\"type\":\"string\",\"source\":\"element LastUpdatedBy\"},{\"name\":\"RowVersion\",\"type\":\"string\",\"source\":\"element RowVersion\"}]}},\"source\":\"element Identifier\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage> ENCODER =
      new BinaryMessageEncoder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage> DECODER =
      new BinaryMessageDecoder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this AnonType_IdentifiersExternalIdentifierLegalEntityMessage to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a AnonType_IdentifiersExternalIdentifierLegalEntityMessage from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a AnonType_IdentifiersExternalIdentifierLegalEntityMessage instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static AnonType_IdentifiersExternalIdentifierLegalEntityMessage fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> Identifier;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public AnonType_IdentifiersExternalIdentifierLegalEntityMessage() {}

  /**
   * All-args constructor.
   * @param Identifier The new value for Identifier
   */
  public AnonType_IdentifiersExternalIdentifierLegalEntityMessage(java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> Identifier) {
    this.Identifier = Identifier;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return Identifier;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: Identifier = (java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'Identifier' field.
   * @return The value of the 'Identifier' field.
   */
  public java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> getIdentifier() {
    return Identifier;
  }


  /**
   * Sets the value of the 'Identifier' field.
   * @param value the value to set.
   */
  public void setIdentifier(java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> value) {
    this.Identifier = value;
  }

  /**
   * Creates a new AnonType_IdentifiersExternalIdentifierLegalEntityMessage RecordBuilder.
   * @return A new AnonType_IdentifiersExternalIdentifierLegalEntityMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder newBuilder() {
    return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder();
  }

  /**
   * Creates a new AnonType_IdentifiersExternalIdentifierLegalEntityMessage RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new AnonType_IdentifiersExternalIdentifierLegalEntityMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder newBuilder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder other) {
    if (other == null) {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder();
    } else {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder(other);
    }
  }

  /**
   * Creates a new AnonType_IdentifiersExternalIdentifierLegalEntityMessage RecordBuilder by copying an existing AnonType_IdentifiersExternalIdentifierLegalEntityMessage instance.
   * @param other The existing instance to copy.
   * @return A new AnonType_IdentifiersExternalIdentifierLegalEntityMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder newBuilder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage other) {
    if (other == null) {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder();
    } else {
      return new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder(other);
    }
  }

  /**
   * RecordBuilder for AnonType_IdentifiersExternalIdentifierLegalEntityMessage instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<AnonType_IdentifiersExternalIdentifierLegalEntityMessage>
    implements org.apache.avro.data.RecordBuilder<AnonType_IdentifiersExternalIdentifierLegalEntityMessage> {

    private java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> Identifier;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.Identifier)) {
        this.Identifier = data().deepCopy(fields()[0].schema(), other.Identifier);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
    }

    /**
     * Creates a Builder by copying an existing AnonType_IdentifiersExternalIdentifierLegalEntityMessage instance
     * @param other The existing instance to copy.
     */
    private Builder(com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.Identifier)) {
        this.Identifier = data().deepCopy(fields()[0].schema(), other.Identifier);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'Identifier' field.
      * @return The value.
      */
    public java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> getIdentifier() {
      return Identifier;
    }


    /**
      * Sets the value of the 'Identifier' field.
      * @param value The value of 'Identifier'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder setIdentifier(java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> value) {
      validate(fields()[0], value);
      this.Identifier = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'Identifier' field has been set.
      * @return True if the 'Identifier' field has been set, false otherwise.
      */
    public boolean hasIdentifier() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'Identifier' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifiersExternalIdentifierLegalEntityMessage.Builder clearIdentifier() {
      Identifier = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public AnonType_IdentifiersExternalIdentifierLegalEntityMessage build() {
      try {
        AnonType_IdentifiersExternalIdentifierLegalEntityMessage record = new AnonType_IdentifiersExternalIdentifierLegalEntityMessage();
        record.Identifier = fieldSetFlags()[0] ? this.Identifier : (java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage>) defaultValue(fields()[0]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<AnonType_IdentifiersExternalIdentifierLegalEntityMessage>
    WRITER$ = (org.apache.avro.io.DatumWriter<AnonType_IdentifiersExternalIdentifierLegalEntityMessage>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<AnonType_IdentifiersExternalIdentifierLegalEntityMessage>
    READER$ = (org.apache.avro.io.DatumReader<AnonType_IdentifiersExternalIdentifierLegalEntityMessage>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    long size0 = this.Identifier.size();
    out.writeArrayStart();
    out.setItemCount(size0);
    long actualSize0 = 0;
    for (com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage e0: this.Identifier) {
      actualSize0++;
      out.startItem();
      e0.customEncode(out);
    }
    out.writeArrayEnd();
    if (actualSize0 != size0)
      throw new java.util.ConcurrentModificationException("Array-size written was " + size0 + ", but element count was " + actualSize0 + ".");

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      long size0 = in.readArrayStart();
      java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> a0 = this.Identifier;
      if (a0 == null) {
        a0 = new SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage>((int)size0, SCHEMA$.getField("Identifier").schema());
        this.Identifier = a0;
      } else a0.clear();
      SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage>)a0 : null);
      for ( ; 0 < size0; size0 = in.arrayNext()) {
        for ( ; size0 != 0; size0--) {
          com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage e0 = (ga0 != null ? ga0.peek() : null);
          if (e0 == null) {
            e0 = new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage();
          }
          e0.customDecode(in);
          a0.add(e0);
        }
      }

    } else {
      for (int i = 0; i < 1; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          long size0 = in.readArrayStart();
          java.util.List<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> a0 = this.Identifier;
          if (a0 == null) {
            a0 = new SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage>((int)size0, SCHEMA$.getField("Identifier").schema());
            this.Identifier = a0;
          } else a0.clear();
          SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage>)a0 : null);
          for ( ; 0 < size0; size0 = in.arrayNext()) {
            for ( ; size0 != 0; size0--) {
              com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage e0 = (ga0 != null ? ga0.peek() : null);
              if (e0 == null) {
                e0 = new com.scb.bpsi.schema.cc.legalentity.v4.AnonType_IdentifierIdentifiersExternalIdentifierLegalEntityMessage();
              }
              e0.customDecode(in);
              a0.add(e0);
            }
          }
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










