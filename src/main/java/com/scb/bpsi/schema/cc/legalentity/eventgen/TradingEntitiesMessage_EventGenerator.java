package com.scb.bpsi.schema.cc.legalentity.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.schema.cc.legalentity.AnonType_TradingEntitiesMessage;


public class TradingEntitiesMessage_EventGenerator implements EventsProduction {

	@SuppressWarnings("unchecked")
	@Override
	public List<AnonType_TradingEntitiesMessage> eventsGenerator(int numOfEvents) {
		
		List<AnonType_TradingEntitiesMessage> events=new ArrayList<>();
		
		EventGenerationImpl event = new EventGenerationImpl();
	
    	IntStream.range(0,numOfEvents)
		.forEach(i -> {
						events.add(event.generateTradingEntitiesMessage());
						});

    	return events;
	}
}
