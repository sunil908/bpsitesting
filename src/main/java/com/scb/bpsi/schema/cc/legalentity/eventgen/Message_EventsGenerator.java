package com.scb.bpsi.schema.cc.legalentity.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.schema.cc.legalentity.*;

public class Message_EventsGenerator implements EventsProduction {

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> eventsGenerator(int numOfEvents) {
		List<Message> events=new ArrayList<>();
		
		EventGenerationImpl event = new EventGenerationImpl();
	
    	IntStream.range(0,numOfEvents)
		.forEach(i -> {
						events.add(event.generateMessage());
						});

    	return events;
	}
	
}
