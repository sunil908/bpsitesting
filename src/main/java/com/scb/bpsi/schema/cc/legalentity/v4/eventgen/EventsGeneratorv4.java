package com.scb.bpsi.schema.cc.legalentity.v4.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.messagegenerator.event.EventsProductionFactory;
import com.scb.bpsi.schema.cc.legalentity.v4.*;

public class EventsGeneratorv4 implements EventsProduction {

	
	@SuppressWarnings("unchecked")
	@Override
	public List<AnonType_Message> eventsGenerator(int numOfEvents) {
		List<AnonType_Message> events=new ArrayList<>();
		
		EventGenerationImpl event = new EventGenerationImpl();
	
    	IntStream.range(0,numOfEvents)
		.forEach(i -> {
						events.add(event.generateMessage());
						});
    	return events;
	}
	
}
