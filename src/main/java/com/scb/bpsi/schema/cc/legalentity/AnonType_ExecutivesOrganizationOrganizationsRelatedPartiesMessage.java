/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.scb.bpsi.schema.cc.legalentity;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 4487225230804961348L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage\",\"namespace\":\"com.scb.bpsi.schema.cc.legalentityv4\",\"fields\":[{\"name\":\"Executive\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage\",\"fields\":[{\"name\":\"SequenceNumber\",\"type\":\"string\",\"source\":\"element SequenceNumber\"},{\"name\":\"NamesOfExecutiveManagers\",\"type\":\"string\",\"source\":\"element NamesOfExecutiveManagers\"}]}},\"source\":\"element Executive\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage> ENCODER =
      new BinaryMessageEncoder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage> DECODER =
      new BinaryMessageDecoder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> Executive;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage() {}

  /**
   * All-args constructor.
   * @param Executive The new value for Executive
   */
  public AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage(java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> Executive) {
    this.Executive = Executive;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return Executive;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: Executive = (java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'Executive' field.
   * @return The value of the 'Executive' field.
   */
  public java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> getExecutive() {
    return Executive;
  }


  /**
   * Sets the value of the 'Executive' field.
   * @param value the value to set.
   */
  public void setExecutive(java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> value) {
    this.Executive = value;
  }

  /**
   * Creates a new AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder.
   * @return A new AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder newBuilder() {
    return new com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder();
  }

  /**
   * Creates a new AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder newBuilder(com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder other) {
    if (other == null) {
      return new com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder();
    } else {
      return new com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder(other);
    }
  }

  /**
   * Creates a new AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder by copying an existing AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage instance.
   * @param other The existing instance to copy.
   * @return A new AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage RecordBuilder
   */
  public static com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder newBuilder(com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage other) {
    if (other == null) {
      return new com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder();
    } else {
      return new com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder(other);
    }
  }

  /**
   * RecordBuilder for AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage>
    implements org.apache.avro.data.RecordBuilder<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage> {

    private java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> Executive;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.Executive)) {
        this.Executive = data().deepCopy(fields()[0].schema(), other.Executive);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
    }

    /**
     * Creates a Builder by copying an existing AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage instance
     * @param other The existing instance to copy.
     */
    private Builder(com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.Executive)) {
        this.Executive = data().deepCopy(fields()[0].schema(), other.Executive);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'Executive' field.
      * @return The value.
      */
    public java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> getExecutive() {
      return Executive;
    }


    /**
      * Sets the value of the 'Executive' field.
      * @param value The value of 'Executive'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder setExecutive(java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> value) {
      validate(fields()[0], value);
      this.Executive = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'Executive' field has been set.
      * @return True if the 'Executive' field has been set, false otherwise.
      */
    public boolean hasExecutive() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'Executive' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage.Builder clearExecutive() {
      Executive = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage build() {
      try {
        AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage record = new AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage();
        record.Executive = fieldSetFlags()[0] ? this.Executive : (java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>) defaultValue(fields()[0]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage>
    WRITER$ = (org.apache.avro.io.DatumWriter<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage>
    READER$ = (org.apache.avro.io.DatumReader<AnonType_ExecutivesOrganizationOrganizationsRelatedPartiesMessage>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    long size0 = this.Executive.size();
    out.writeArrayStart();
    out.setItemCount(size0);
    long actualSize0 = 0;
    for (com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage e0: this.Executive) {
      actualSize0++;
      out.startItem();
      e0.customEncode(out);
    }
    out.writeArrayEnd();
    if (actualSize0 != size0)
      throw new java.util.ConcurrentModificationException("Array-size written was " + size0 + ", but element count was " + actualSize0 + ".");

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      long size0 = in.readArrayStart();
      java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> a0 = this.Executive;
      if (a0 == null) {
        a0 = new SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>((int)size0, SCHEMA$.getField("Executive").schema());
        this.Executive = a0;
      } else a0.clear();
      SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>)a0 : null);
      for ( ; 0 < size0; size0 = in.arrayNext()) {
        for ( ; size0 != 0; size0--) {
          com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage e0 = (ga0 != null ? ga0.peek() : null);
          if (e0 == null) {
            e0 = new com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage();
          }
          e0.customDecode(in);
          a0.add(e0);
        }
      }

    } else {
      for (int i = 0; i < 1; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          long size0 = in.readArrayStart();
          java.util.List<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> a0 = this.Executive;
          if (a0 == null) {
            a0 = new SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>((int)size0, SCHEMA$.getField("Executive").schema());
            this.Executive = a0;
          } else a0.clear();
          SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage>)a0 : null);
          for ( ; 0 < size0; size0 = in.arrayNext()) {
            for ( ; size0 != 0; size0--) {
              com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage e0 = (ga0 != null ? ga0.peek() : null);
              if (e0 == null) {
                e0 = new com.scb.bpsi.schema.cc.legalentity.AnonType_ExecutiveExecutivesOrganizationOrganizationsRelatedPartiesMessage();
              }
              e0.customDecode(in);
              a0.add(e0);
            }
          }
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










