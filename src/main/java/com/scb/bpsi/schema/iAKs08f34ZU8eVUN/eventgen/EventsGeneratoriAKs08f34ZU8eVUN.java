package com.scb.bpsi.schema.iAKs08f34ZU8eVUN.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.schema.iAKs08f34ZU8eVUN.Test_Addresses;


public class EventsGeneratoriAKs08f34ZU8eVUN implements EventsProduction {

	@SuppressWarnings("unchecked")
	@Override
	public List<Test_Addresses> eventsGenerator(int numOfEvents) {
		List<Test_Addresses> events=new ArrayList<>();
		
		EventGenerationImpl event = new EventGenerationImpl();
	
    	IntStream.range(0,numOfEvents)
		.forEach(i -> {
						events.add(event.Test_Addresses_generateMessage());
						});
    	return events;
	}
	
}
