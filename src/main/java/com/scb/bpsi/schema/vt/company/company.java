/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.scb.bpsi.schema.vt.company;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class company extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -6468707341726993731L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"company\",\"namespace\":\"com.scb.bpsi.schema.vt.company\",\"fields\":[{\"name\":\"company\",\"type\":\"string\"},{\"name\":\"address\",\"type\":{\"type\":\"record\",\"name\":\"address\",\"fields\":[{\"name\":\"street\",\"type\":\"string\"}]}},{\"name\":\"headers\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"headers_record\",\"fields\":[{\"name\":\"phase\",\"type\":\"string\"},{\"name\":\"header\",\"type\":{\"type\":\"record\",\"name\":\"header\",\"fields\":[{\"name\":\"records\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"records_record\",\"fields\":[{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"role\",\"type\":\"string\"},{\"name\":\"grade\",\"type\":\"string\"}]}}}]}}]}}},{\"name\":\"body\",\"type\":{\"type\":\"record\",\"name\":\"body\",\"fields\":[{\"name\":\"classification\",\"type\":\"string\"}]}}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<company> ENCODER =
      new BinaryMessageEncoder<company>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<company> DECODER =
      new BinaryMessageDecoder<company>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<company> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<company> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<company> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<company>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this company to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a company from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a company instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static company fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.lang.CharSequence company;
   private com.scb.bpsi.schema.vt.company.address address;
   private java.util.List<com.scb.bpsi.schema.vt.company.headers_record> headers;
   private com.scb.bpsi.schema.vt.company.body body;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public company() {}

  /**
   * All-args constructor.
   * @param company The new value for company
   * @param address The new value for address
   * @param headers The new value for headers
   * @param body The new value for body
   */
  public company(java.lang.CharSequence company, com.scb.bpsi.schema.vt.company.address address, java.util.List<com.scb.bpsi.schema.vt.company.headers_record> headers, com.scb.bpsi.schema.vt.company.body body) {
    this.company = company;
    this.address = address;
    this.headers = headers;
    this.body = body;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return company;
    case 1: return address;
    case 2: return headers;
    case 3: return body;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: company = (java.lang.CharSequence)value$; break;
    case 1: address = (com.scb.bpsi.schema.vt.company.address)value$; break;
    case 2: headers = (java.util.List<com.scb.bpsi.schema.vt.company.headers_record>)value$; break;
    case 3: body = (com.scb.bpsi.schema.vt.company.body)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'company' field.
   * @return The value of the 'company' field.
   */
  public java.lang.CharSequence getCompany() {
    return company;
  }


  /**
   * Sets the value of the 'company' field.
   * @param value the value to set.
   */
  public void setCompany(java.lang.CharSequence value) {
    this.company = value;
  }

  /**
   * Gets the value of the 'address' field.
   * @return The value of the 'address' field.
   */
  public com.scb.bpsi.schema.vt.company.address getAddress() {
    return address;
  }


  /**
   * Sets the value of the 'address' field.
   * @param value the value to set.
   */
  public void setAddress(com.scb.bpsi.schema.vt.company.address value) {
    this.address = value;
  }

  /**
   * Gets the value of the 'headers' field.
   * @return The value of the 'headers' field.
   */
  public java.util.List<com.scb.bpsi.schema.vt.company.headers_record> getHeaders() {
    return headers;
  }


  /**
   * Sets the value of the 'headers' field.
   * @param value the value to set.
   */
  public void setHeaders(java.util.List<com.scb.bpsi.schema.vt.company.headers_record> value) {
    this.headers = value;
  }

  /**
   * Gets the value of the 'body' field.
   * @return The value of the 'body' field.
   */
  public com.scb.bpsi.schema.vt.company.body getBody() {
    return body;
  }


  /**
   * Sets the value of the 'body' field.
   * @param value the value to set.
   */
  public void setBody(com.scb.bpsi.schema.vt.company.body value) {
    this.body = value;
  }

  /**
   * Creates a new company RecordBuilder.
   * @return A new company RecordBuilder
   */
  public static com.scb.bpsi.schema.vt.company.company.Builder newBuilder() {
    return new com.scb.bpsi.schema.vt.company.company.Builder();
  }

  /**
   * Creates a new company RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new company RecordBuilder
   */
  public static com.scb.bpsi.schema.vt.company.company.Builder newBuilder(com.scb.bpsi.schema.vt.company.company.Builder other) {
    if (other == null) {
      return new com.scb.bpsi.schema.vt.company.company.Builder();
    } else {
      return new com.scb.bpsi.schema.vt.company.company.Builder(other);
    }
  }

  /**
   * Creates a new company RecordBuilder by copying an existing company instance.
   * @param other The existing instance to copy.
   * @return A new company RecordBuilder
   */
  public static com.scb.bpsi.schema.vt.company.company.Builder newBuilder(com.scb.bpsi.schema.vt.company.company other) {
    if (other == null) {
      return new com.scb.bpsi.schema.vt.company.company.Builder();
    } else {
      return new com.scb.bpsi.schema.vt.company.company.Builder(other);
    }
  }

  /**
   * RecordBuilder for company instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<company>
    implements org.apache.avro.data.RecordBuilder<company> {

    private java.lang.CharSequence company;
    private com.scb.bpsi.schema.vt.company.address address;
    private com.scb.bpsi.schema.vt.company.address.Builder addressBuilder;
    private java.util.List<com.scb.bpsi.schema.vt.company.headers_record> headers;
    private com.scb.bpsi.schema.vt.company.body body;
    private com.scb.bpsi.schema.vt.company.body.Builder bodyBuilder;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(com.scb.bpsi.schema.vt.company.company.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.company)) {
        this.company = data().deepCopy(fields()[0].schema(), other.company);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.address)) {
        this.address = data().deepCopy(fields()[1].schema(), other.address);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (other.hasAddressBuilder()) {
        this.addressBuilder = com.scb.bpsi.schema.vt.company.address.newBuilder(other.getAddressBuilder());
      }
      if (isValidValue(fields()[2], other.headers)) {
        this.headers = data().deepCopy(fields()[2].schema(), other.headers);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
      if (isValidValue(fields()[3], other.body)) {
        this.body = data().deepCopy(fields()[3].schema(), other.body);
        fieldSetFlags()[3] = other.fieldSetFlags()[3];
      }
      if (other.hasBodyBuilder()) {
        this.bodyBuilder = com.scb.bpsi.schema.vt.company.body.newBuilder(other.getBodyBuilder());
      }
    }

    /**
     * Creates a Builder by copying an existing company instance
     * @param other The existing instance to copy.
     */
    private Builder(com.scb.bpsi.schema.vt.company.company other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.company)) {
        this.company = data().deepCopy(fields()[0].schema(), other.company);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.address)) {
        this.address = data().deepCopy(fields()[1].schema(), other.address);
        fieldSetFlags()[1] = true;
      }
      this.addressBuilder = null;
      if (isValidValue(fields()[2], other.headers)) {
        this.headers = data().deepCopy(fields()[2].schema(), other.headers);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.body)) {
        this.body = data().deepCopy(fields()[3].schema(), other.body);
        fieldSetFlags()[3] = true;
      }
      this.bodyBuilder = null;
    }

    /**
      * Gets the value of the 'company' field.
      * @return The value.
      */
    public java.lang.CharSequence getCompany() {
      return company;
    }


    /**
      * Sets the value of the 'company' field.
      * @param value The value of 'company'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.vt.company.company.Builder setCompany(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.company = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'company' field has been set.
      * @return True if the 'company' field has been set, false otherwise.
      */
    public boolean hasCompany() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'company' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.vt.company.company.Builder clearCompany() {
      company = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'address' field.
      * @return The value.
      */
    public com.scb.bpsi.schema.vt.company.address getAddress() {
      return address;
    }


    /**
      * Sets the value of the 'address' field.
      * @param value The value of 'address'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.vt.company.company.Builder setAddress(com.scb.bpsi.schema.vt.company.address value) {
      validate(fields()[1], value);
      this.addressBuilder = null;
      this.address = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'address' field has been set.
      * @return True if the 'address' field has been set, false otherwise.
      */
    public boolean hasAddress() {
      return fieldSetFlags()[1];
    }

    /**
     * Gets the Builder instance for the 'address' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public com.scb.bpsi.schema.vt.company.address.Builder getAddressBuilder() {
      if (addressBuilder == null) {
        if (hasAddress()) {
          setAddressBuilder(com.scb.bpsi.schema.vt.company.address.newBuilder(address));
        } else {
          setAddressBuilder(com.scb.bpsi.schema.vt.company.address.newBuilder());
        }
      }
      return addressBuilder;
    }

    /**
     * Sets the Builder instance for the 'address' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public com.scb.bpsi.schema.vt.company.company.Builder setAddressBuilder(com.scb.bpsi.schema.vt.company.address.Builder value) {
      clearAddress();
      addressBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'address' field has an active Builder instance
     * @return True if the 'address' field has an active Builder instance
     */
    public boolean hasAddressBuilder() {
      return addressBuilder != null;
    }

    /**
      * Clears the value of the 'address' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.vt.company.company.Builder clearAddress() {
      address = null;
      addressBuilder = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'headers' field.
      * @return The value.
      */
    public java.util.List<com.scb.bpsi.schema.vt.company.headers_record> getHeaders() {
      return headers;
    }


    /**
      * Sets the value of the 'headers' field.
      * @param value The value of 'headers'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.vt.company.company.Builder setHeaders(java.util.List<com.scb.bpsi.schema.vt.company.headers_record> value) {
      validate(fields()[2], value);
      this.headers = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'headers' field has been set.
      * @return True if the 'headers' field has been set, false otherwise.
      */
    public boolean hasHeaders() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'headers' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.vt.company.company.Builder clearHeaders() {
      headers = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'body' field.
      * @return The value.
      */
    public com.scb.bpsi.schema.vt.company.body getBody() {
      return body;
    }


    /**
      * Sets the value of the 'body' field.
      * @param value The value of 'body'.
      * @return This builder.
      */
    public com.scb.bpsi.schema.vt.company.company.Builder setBody(com.scb.bpsi.schema.vt.company.body value) {
      validate(fields()[3], value);
      this.bodyBuilder = null;
      this.body = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'body' field has been set.
      * @return True if the 'body' field has been set, false otherwise.
      */
    public boolean hasBody() {
      return fieldSetFlags()[3];
    }

    /**
     * Gets the Builder instance for the 'body' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public com.scb.bpsi.schema.vt.company.body.Builder getBodyBuilder() {
      if (bodyBuilder == null) {
        if (hasBody()) {
          setBodyBuilder(com.scb.bpsi.schema.vt.company.body.newBuilder(body));
        } else {
          setBodyBuilder(com.scb.bpsi.schema.vt.company.body.newBuilder());
        }
      }
      return bodyBuilder;
    }

    /**
     * Sets the Builder instance for the 'body' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public com.scb.bpsi.schema.vt.company.company.Builder setBodyBuilder(com.scb.bpsi.schema.vt.company.body.Builder value) {
      clearBody();
      bodyBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'body' field has an active Builder instance
     * @return True if the 'body' field has an active Builder instance
     */
    public boolean hasBodyBuilder() {
      return bodyBuilder != null;
    }

    /**
      * Clears the value of the 'body' field.
      * @return This builder.
      */
    public com.scb.bpsi.schema.vt.company.company.Builder clearBody() {
      body = null;
      bodyBuilder = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public company build() {
      try {
        company record = new company();
        record.company = fieldSetFlags()[0] ? this.company : (java.lang.CharSequence) defaultValue(fields()[0]);
        if (addressBuilder != null) {
          try {
            record.address = this.addressBuilder.build();
          } catch (org.apache.avro.AvroMissingFieldException e) {
            e.addParentField(record.getSchema().getField("address"));
            throw e;
          }
        } else {
          record.address = fieldSetFlags()[1] ? this.address : (com.scb.bpsi.schema.vt.company.address) defaultValue(fields()[1]);
        }
        record.headers = fieldSetFlags()[2] ? this.headers : (java.util.List<com.scb.bpsi.schema.vt.company.headers_record>) defaultValue(fields()[2]);
        if (bodyBuilder != null) {
          try {
            record.body = this.bodyBuilder.build();
          } catch (org.apache.avro.AvroMissingFieldException e) {
            e.addParentField(record.getSchema().getField("body"));
            throw e;
          }
        } else {
          record.body = fieldSetFlags()[3] ? this.body : (com.scb.bpsi.schema.vt.company.body) defaultValue(fields()[3]);
        }
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<company>
    WRITER$ = (org.apache.avro.io.DatumWriter<company>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<company>
    READER$ = (org.apache.avro.io.DatumReader<company>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeString(this.company);

    this.address.customEncode(out);

    long size0 = this.headers.size();
    out.writeArrayStart();
    out.setItemCount(size0);
    long actualSize0 = 0;
    for (com.scb.bpsi.schema.vt.company.headers_record e0: this.headers) {
      actualSize0++;
      out.startItem();
      e0.customEncode(out);
    }
    out.writeArrayEnd();
    if (actualSize0 != size0)
      throw new java.util.ConcurrentModificationException("Array-size written was " + size0 + ", but element count was " + actualSize0 + ".");

    this.body.customEncode(out);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.company = in.readString(this.company instanceof Utf8 ? (Utf8)this.company : null);

      if (this.address == null) {
        this.address = new com.scb.bpsi.schema.vt.company.address();
      }
      this.address.customDecode(in);

      long size0 = in.readArrayStart();
      java.util.List<com.scb.bpsi.schema.vt.company.headers_record> a0 = this.headers;
      if (a0 == null) {
        a0 = new SpecificData.Array<com.scb.bpsi.schema.vt.company.headers_record>((int)size0, SCHEMA$.getField("headers").schema());
        this.headers = a0;
      } else a0.clear();
      SpecificData.Array<com.scb.bpsi.schema.vt.company.headers_record> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<com.scb.bpsi.schema.vt.company.headers_record>)a0 : null);
      for ( ; 0 < size0; size0 = in.arrayNext()) {
        for ( ; size0 != 0; size0--) {
          com.scb.bpsi.schema.vt.company.headers_record e0 = (ga0 != null ? ga0.peek() : null);
          if (e0 == null) {
            e0 = new com.scb.bpsi.schema.vt.company.headers_record();
          }
          e0.customDecode(in);
          a0.add(e0);
        }
      }

      if (this.body == null) {
        this.body = new com.scb.bpsi.schema.vt.company.body();
      }
      this.body.customDecode(in);

    } else {
      for (int i = 0; i < 4; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.company = in.readString(this.company instanceof Utf8 ? (Utf8)this.company : null);
          break;

        case 1:
          if (this.address == null) {
            this.address = new com.scb.bpsi.schema.vt.company.address();
          }
          this.address.customDecode(in);
          break;

        case 2:
          long size0 = in.readArrayStart();
          java.util.List<com.scb.bpsi.schema.vt.company.headers_record> a0 = this.headers;
          if (a0 == null) {
            a0 = new SpecificData.Array<com.scb.bpsi.schema.vt.company.headers_record>((int)size0, SCHEMA$.getField("headers").schema());
            this.headers = a0;
          } else a0.clear();
          SpecificData.Array<com.scb.bpsi.schema.vt.company.headers_record> ga0 = (a0 instanceof SpecificData.Array ? (SpecificData.Array<com.scb.bpsi.schema.vt.company.headers_record>)a0 : null);
          for ( ; 0 < size0; size0 = in.arrayNext()) {
            for ( ; size0 != 0; size0--) {
              com.scb.bpsi.schema.vt.company.headers_record e0 = (ga0 != null ? ga0.peek() : null);
              if (e0 == null) {
                e0 = new com.scb.bpsi.schema.vt.company.headers_record();
              }
              e0.customDecode(in);
              a0.add(e0);
            }
          }
          break;

        case 3:
          if (this.body == null) {
            this.body = new com.scb.bpsi.schema.vt.company.body();
          }
          this.body.customDecode(in);
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










