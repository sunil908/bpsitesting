package com.scb.bpsi.schema.vt.company.eventgen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.messagegenerator.event.EventsProductionFactory;
import com.scb.bpsi.schema.vt.company.company;

public class EventsGeneratorCompany implements EventsProduction {

	
	@SuppressWarnings("unchecked")
	@Override
	public List<company> eventsGenerator(int numOfEvents) {
		List<company> events=new ArrayList<>();
		
		EventGenerationImpl event = new EventGenerationImpl();
	
    	IntStream.range(0,numOfEvents)
		.forEach(i -> {
						events.add(event.company_generateMessage());
						});
    	return events;
	}
	
}
