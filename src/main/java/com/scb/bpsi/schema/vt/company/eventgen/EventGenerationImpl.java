package com.scb.bpsi.schema.vt.company.eventgen;

import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import com.scb.bpsi.messagegenerator.event.EventGenerator;
import com.scb.bpsi.messagegenerator.refdata.enums.*;
import com.scb.bpsi.schema.vt.company.*;



public class EventGenerationImpl implements EventGenerator {
	
	private Faker faker = new Faker();
	

	public company company_generateMessage() {
		
		List<headers_record> headers = new ArrayList<headers_record>();
		IntStream.range(0,(new Random()).nextInt(3)+1)
		.forEach(i -> headers.add(headers_record_generateMessage()));
		
		return company.newBuilder()
				.setAddress(address_generateMessage())
				.setBody(body_generateMessage())
				.setCompany(faker.company().name())
				.setHeaders(headers)
				.build();
	}
	
	
	public address address_generateMessage() {
		return address.newBuilder()
				.setStreet(faker.address().streetAddress())
				.build();
	}
	
	public body body_generateMessage() {
		return body.newBuilder()
				.setClassification("final")
				.build();
	}


	public headers_record headers_record_generateMessage() {
		
	return headers_record.newBuilder()
			.setHeader(header_generateMessage())
			.setPhase(faker.options().option(Phases.class).name())
			.build();
	}
	
	public header header_generateMessage()	{
		
	List<records_record> records = new ArrayList<records_record>();
	IntStream.range(0,(new Random()).nextInt(20)+150)
	.forEach(i -> records.add(records_record_generateMessage()));
	
	return header.newBuilder()
			.setRecords(records)
			.build();
}
	
	public records_record records_record_generateMessage() {
		
		return records_record.newBuilder()
				.setName(faker.superhero().name())
				.setRole(faker.options().option(Roles.class).name())
				.setGrade(faker.options().option(Grade.class).name())
				.build();
	}
	
}


