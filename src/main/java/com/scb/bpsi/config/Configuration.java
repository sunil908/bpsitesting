package com.scb.bpsi.config;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scb.bpsi.messagegenerator.event.IngestMetric;
import com.scb.bpsi.messagegenerator.fileutil.FileReader;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class Configuration  {

	
	private static Configuration config = null;
	
	public Properties keyProperties = new Properties();
	private Map<String, Boolean> exeTestcase = new HashMap<>();
	private Map<String, String> schemaInferProperties = new HashMap<>();
	private Map<String, String> schemaDefProperties = new HashMap<>();
	private Map<String, String> ingestSingleTest = new HashMap<>();
	private Map<String, String> ingestPerfProperties = new HashMap<>();
	private Map<String, String> urlsProperties = new HashMap<>();
    private List<Map<String, String>> ingestBatchTest =  new ArrayList<>();
    /**
     * FILENAME is the file location of the configuration JSON file
     *
     * @TODO Potentially make this configurable
     */
	public static String FILENAME = "./bpsi.properties";
	
    /**
     * Returns an instance of the Configuration specified at configFileName
     *
     * @param configFileName
     */
	public static Configuration getInstance() {
        if (Configuration.config == null) {
        	Configuration.config = new Configuration();
        }
        
        return Configuration.config;
    }


    /**
     * Load the Configuration specified at fileName
     *
     * @param fileName
     * @return boolean did this load succeed?
     */

	public boolean loadConfig(String configFileName) throws JsonProcessingException {
		try {
			
			FILENAME = configFileName;
			
			//JsonNode jsonNode= FileReader.readJsonFile(configFileName, Configuration.class);
			JsonNode jsonNode= FileReader.readJsonFile(configFileName);
			
			ObjectMapper mapper = new ObjectMapper();
			//mapper.setSerializationInclusion(Include.NON_NULL);
			//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			
			if(jsonNode.has("schemaInferTest")) {
				this.schemaInferProperties.putAll(mapper.convertValue(jsonNode.get("schemaInferTest"), new TypeReference<Map<String, String>>() {}));
			}
			
			if(jsonNode.has("schemaDefTest")) {
				this.schemaDefProperties.putAll(mapper.convertValue(jsonNode.get("schemaDefTest"), new TypeReference<Map<String, String>>() {}));
			}
			if(jsonNode.has("ingestSingleTest")) {
				this.ingestSingleTest.putAll(mapper.convertValue(jsonNode.get("ingestSingleTest"), new TypeReference<Map<String, String>>() {}));
			}
			if(jsonNode.has("ingestPerfTest")) {
				this.ingestPerfProperties.putAll(mapper.convertValue(jsonNode.get("ingestPerfTest"), new TypeReference<Map<String, String>>() {}));
			}
			if(jsonNode.has("urls")) {
				this.urlsProperties.putAll(mapper.convertValue(jsonNode.get("urls"), new TypeReference<Map<String, String>>() {}));
			}
			if(jsonNode.has("keyProperties")) {
				this.keyProperties = mapper.convertValue(jsonNode.get("keyProperties"), new TypeReference<Properties>(){});
			}
			if(jsonNode.has("ingestBatchTest")) {
				this.ingestBatchTest = mapper.convertValue(jsonNode.get("ingestBatchTest"), new TypeReference<List<Map<String, String>>>(){});
			}
			
			List<Map<String, Boolean>> exeTestcaseCollection = null; 

			exeTestcaseCollection = mapper.convertValue(jsonNode.get("exeTestcase"), new TypeReference<List<Map<String, Boolean>>>() {});
			
			exeTestcaseCollection.stream().forEach( map -> {
				this.exeTestcase.putAll(map.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue())));
								  });
			
			log.info("Properties File Location: "+ configFileName);
			log.info("Properties Content: \n"+ jsonNode.toPrettyString());
			
			return true;
		} catch (IOException e) {
			log.error("Unable to read the Configuration file " + configFileName);
			log.error(e.getStackTrace().toString());
			return false;
		}
		catch (IllegalArgumentException e) {
			log.error("Unable to read the Configuration file " + configFileName);
			log.error(e.getStackTrace().toString());
			return false;
		}
    }

	public Boolean getExeTestcase(String key) {
		return Optional.ofNullable(exeTestcase.get(key)).orElse(false);
	}

    
	public String getSchemaProperties(String key) {
		return Optional.ofNullable(schemaInferProperties.get(key)).orElse("");
	}

	public String getSchemaDefProperties(String key) {
		return Optional.ofNullable(schemaDefProperties.get(key)).orElse("");
	}
	
	public String getUrlProperties(String key) {
		return Optional.ofNullable(urlsProperties.get(key)).orElse("");
	}

	public String getingestSingleTest(String key) {
		return Optional.ofNullable(ingestSingleTest.get(key)).orElse("");
	}

	public String getIngestPerfProperties(String key) {
		return Optional.ofNullable(ingestPerfProperties.get(key)).orElse("");
		
	}
	
	public String getKeyProperties(String K) {
		return Optional.ofNullable(keyProperties.getProperty(K)).orElse("");
	}
	
	public List<Map<String, String>> getIngestBatchRest() {
		return this.ingestBatchTest;
	}
	
	public void addKeyProperties(String K, String V) {
		keyProperties.put(K, V);
	}

	public void addKeyProperties(String K, int V) {
		keyProperties.put(K, V);
	}
	
	public void addKeyProperties(String K, float V) {
		keyProperties.put(K, V);
	}
	
	@Override
	public String toString() {
		return 	exeTestcase.toString()
				+"\n" + keyProperties.toString()
				+"\n" + schemaInferProperties.toString()
				+"\n" + schemaDefProperties.toString()
				+"\n" + ingestSingleTest.toString()
				+"\n" + urlsProperties.toString();
	}
}
