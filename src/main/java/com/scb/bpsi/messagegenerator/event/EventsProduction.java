package com.scb.bpsi.messagegenerator.event;

import java.util.List;

public interface EventsProduction {
	<T> List<T> eventsGenerator(int numOfEvents);
}
