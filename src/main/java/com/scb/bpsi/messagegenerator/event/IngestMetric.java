package com.scb.bpsi.messagegenerator.event;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.mortbay.log.Log;


public final class IngestMetric {

	
	private static IngestMetric eventPerfMetrics;
	
	 private ZonedDateTime initDateTime;
	 private ZonedDateTime endDateTime;
	 private Duration duration;
	 private int numOfEvents;
	 private int messageByteSize;

	 
	public IngestMetric() {
		 this.initDateTime = ZonedDateTime.now(ZoneId.of("UTC"));
	 }

	 public IngestMetric(ZonedDateTime initTime, int numOfEvents) {
		 this.initDateTime = initTime;
		 this.numOfEvents = numOfEvents;
	 }

	 public void setNumOfEvents(int numOfEvents) {
		 this.numOfEvents = numOfEvents;
	 }
	 
	 public static IngestMetric getInstance() {
		 if(eventPerfMetrics==null) {
			 eventPerfMetrics = new IngestMetric(); 
		 }
		 return eventPerfMetrics;
	 }
	 
	 public int getMessageByteSize() {
			return messageByteSize;
		}

		public void setMessageByteSize(int messageByteSize) {
			this.messageByteSize = messageByteSize;
		}

		public int getNumOfEvents() {
			return numOfEvents;
		}

		public static void setEventPerfMetrics(IngestMetric eventPerfMetrics) {
			IngestMetric.eventPerfMetrics = eventPerfMetrics;
		}

		public void setDuration(Duration duration) {
			this.duration = duration;
		}
	 
	 public long getSeconds()  {
		 return duration.getSeconds();
	 }

	 public long getMilli() {
		 return duration.toMillis();
	 }
	 
	public ZonedDateTime getInitDateTime() {
		return initDateTime;
	}

	public void setInitDateTime(ZonedDateTime initDateTime) {
		this.initDateTime = ZonedDateTime.now(ZoneId.of("UTC"));
	}

	public ZonedDateTime getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(ZonedDateTime endDateTime) {
		this.endDateTime = endDateTime;
		this.duration = Duration.between( initDateTime , this.endDateTime );
	}
	 
	public void stopClock() {
		setEndDateTime(ZonedDateTime.now(ZoneId.of("UTC")));
	}
	
	
	public void printSummary() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy - HH:mm:ss:SSSSSSSSS");
		
		System.out.println("Num of Events: " +  getNumOfEvents() + "\nInitTimeStamp: "+ initDateTime.format(formatter) + "\nEndTimeStamp: " +endDateTime.format(formatter) +
				"\nElapsedTimeSec: " + getSeconds() +
				"\nElapsedTimeMillis: " + getMilli() + 
				"\nMessage Size (byte): " + messageByteSize + 
				"\nTotal Message Size (Kb): " + (messageByteSize*numOfEvents)/1024);
		
		
		Log.info("OPERATION SUMMARY \nNum of Events: " +  getNumOfEvents() + "\nInitTimeStamp: "+ initDateTime.format(formatter) + "\nEndTimeStamp: " +endDateTime.format(formatter) +
				"\nElapsedTimeSec: " + getSeconds() +
				"\nElapsedTimeMillis: " + getMilli() + 
				"\nMessage Size (byte): " + messageByteSize + 
				"\nTotal Message Size (Kb): " + (messageByteSize*numOfEvents)/1024);
	}
}
