package com.scb.bpsi.messagegenerator.event;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class EventsProductionFactory {
	static Map<String, EventsProduction> operationMap = new HashMap<>();

//    static {
//        operationMap.put("com.scb.bpsi.schema.cc.legalentity", new CCLegalEntityEventsGenerator());
//    }
 
    public static Optional<EventsProduction> getOperation(String schemaName) {
        return Optional.ofNullable(operationMap.get(schemaName));
    }
    
    public static void register(String schemaName, EventsProduction producer ) {
    	operationMap.put(schemaName, producer);
    }
}
