package com.scb.bpsi.messagegenerator.enums;

import java.util.EnumMap;

public class FileFormatEnumMap {
	public static final EnumMap<FileFormatType, Integer> fileFormatTypeMap = new EnumMap<>(FileFormatType.class);
	
	static {
		fileFormatTypeMap.put(FileFormatType.XML,10);
		fileFormatTypeMap.put(FileFormatType.JSON,20);
	}
}
