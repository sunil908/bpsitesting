package com.scb.bpsi.messagegenerator.enums;

public enum TargetType {
 RESTURI, WRITEFILE, CLIENT, RESTBATCH
}
