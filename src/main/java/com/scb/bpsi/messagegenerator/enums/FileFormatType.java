package com.scb.bpsi.messagegenerator.enums;

public enum FileFormatType {
	XML, JSON;
}
