package com.scb.bpsi.messagegenerator.enums;

public enum TestResult {
 FAILURE, SUCCESS, SKIPPED
}
