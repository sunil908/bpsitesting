package com.scb.bpsi.messagegenerator.enums;

import java.util.ArrayList;
import java.util.List;


public class TestValidationType {
	
	public static List<String> testValidationTypes = new ArrayList<>();
	
	static {
		testValidationTypes.add("IS_STATUSOK");
		testValidationTypes.add("IS_NONEMPTY");
		testValidationTypes.add("IS_HTTPOK");
	}
	
}
