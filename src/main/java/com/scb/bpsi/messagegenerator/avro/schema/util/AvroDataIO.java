package com.scb.bpsi.messagegenerator.avro.schema.util;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.scb.bpsi.messagegenerator.enums.FileFormatType;
import com.scb.bpsi.messagegenerator.event.EventsProduction;
import com.scb.bpsi.messagegenerator.event.EventsProductionFactory;


public class AvroDataIO {

	public static byte[] serializer(Schema schema, GenericRecord genericRecord) throws IOException {
	    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	    SpecificDatumWriter<GenericRecord> writer = new SpecificDatumWriter<>(schema);
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(outputStream, null);
        writer.write(genericRecord, encoder);
        encoder.flush();
        return outputStream.toByteArray();
	}

	public static GenericRecord deserialize(Schema schema, byte[] data) throws IOException {
	    InputStream is = new ByteArrayInputStream(data);
	    Decoder decoder = DecoderFactory.get().binaryDecoder(is, null);
	    DatumReader<GenericRecord> reader = new SpecificDatumReader<>(schema);
	    return reader.read(null, decoder);
	}
	
	public static <T> List<String> decodeToJson(Schema schema, List<T> messages) {
		List<String> jsonMessages = new ArrayList<>();
		messages.forEach( msg -> 
			{
				try {
					
					jsonMessages.add(
							jsonParser(AvroDataIO.deserialize(schema, AvroDataIO.serializer(schema, (GenericRecord)msg)).toString()).toString()
							);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		);
		return jsonMessages;
	}
	
	
	public static <T> List<String> decodeToXML(Schema schema, List<T> messages, String rootTagName) {
		List<String> xmlMessages = new ArrayList<>();
		messages.forEach( msg -> 
			{
				try {
					String xmlMessage=null;
					xmlMessage = convertJSONToXML(jsonParser(
									AvroDataIO.deserialize(schema, AvroDataIO.serializer(schema, (GenericRecord)msg)).toString()
									), rootTagName);
					
					xmlMessages.add(xmlMessage);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		);
		return xmlMessages;
	}

	public static <T> List<T> produceEvents(int numOfEvents, String schemaName) {
		EventsProduction targetOperation = EventsProductionFactory 
			      .getOperation(schemaName)
			      .orElseThrow(() -> new IllegalArgumentException("Invalid Operator"));
			    return targetOperation.eventsGenerator(numOfEvents);
	}
	
	
	public static List<String> decodeDomainMessages(int numOfEvents, String schemaName, FileFormatType fileFormatType) {

		List<String> decodedMessages = new ArrayList<>();
		List<Object> eventObjects = produceEvents(numOfEvents, schemaName);
		
		
		
		switch(fileFormatType) {
		case JSON: 
					decodedMessages = AvroDataIO.decodeToJson(ReflectData.get().getSchema(eventObjects.get(0).getClass()), eventObjects); 
					break;
		case XML:  
					decodedMessages = AvroDataIO.decodeToXML(ReflectData.get().getSchema(eventObjects.get(0).getClass()), eventObjects,"Message"); 
					break;
		default: break;
		}
		return decodedMessages;
	}
	
	public boolean isValidJSON(String jsonString) 
			  throws JsonParseException, IOException {
			    ObjectMapper mapper = new ObjectMapper();
			    JsonNode actualObj = mapper.readTree(jsonString);
			    return !actualObj.isNull();
			}
	
	public static JsonNode jsonParser(String jsonString) throws JsonParseException, IOException {
		    ObjectMapper mapper = new ObjectMapper();
		    JsonFactory factory = mapper.getFactory();
		    JsonParser parser = factory.createParser(jsonString);
		    JsonNode actualObj = mapper.readTree(parser);
		    assertNotNull(actualObj);
		    return actualObj;
	}
	
	public static String convertJSONToXML(JsonNode node,String rootTagName) throws JsonProcessingException {
		ObjectMapper xmlMapper = new XmlMapper();
		//xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
		String xml = xmlMapper.writer()
	    		 .withRootName(rootTagName)
	    		 //.writeValueAsString(node.get(node.fieldNames().next()));
	    		 .writeValueAsString(node);
	     return xml;
	}
	
	public static String convertJSONToXML(JsonNode node) throws JsonProcessingException {
		ObjectMapper xmlMapper = new XmlMapper();
		//xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
	    String xml = xmlMapper.writer()
	    		 .writeValueAsString(node);
	     return xml;
	}
	
	

}

