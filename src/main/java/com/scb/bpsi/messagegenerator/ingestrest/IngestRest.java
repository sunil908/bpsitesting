package com.scb.bpsi.messagegenerator.ingestrest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.mortbay.log.Log;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.scb.bpsi.config.Configuration;
import com.scb.bpsi.messagegenerator.avro.schema.util.AvroDataIO;
import com.scb.bpsi.messagegenerator.enums.FileFormatEnumMap;
import com.scb.bpsi.messagegenerator.enums.FileFormatType;
import com.scb.bpsi.messagegenerator.event.IngestMetric;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IngestRest {
	
	private static Configuration config = Configuration.getInstance();

	public static List<String> ingestBPSI(String endPointUrl, String schemaName, int dataType, List<String> messages, String sourceIdPath) {
		
		// "com.scb.bpsi.schema.cc.legalentity"
		AtomicInteger counter = new AtomicInteger(1);
		List<String> responses = new ArrayList<>();
	
		// Monitor the metrics
		IngestMetric eventMetrics = IngestMetric.getInstance();
		eventMetrics.setNumOfEvents(messages.size());
		eventMetrics.setMessageByteSize(messages.get(0).getBytes().length);
		
		messages.forEach(
					message -> {
						
						PayloadRest payload = new PayloadRest("cheers"+ counter.getAndIncrement() + System.currentTimeMillis(), schemaName, message, dataType, sourceIdPath);
						responses.add(ingestBPSIPut(endPointUrl,payload).getBody());
						payload =  null;
					}
				);
		// Stop the clock
		eventMetrics.stopClock();
		return responses;
	}
	
	
	public static List<String> ingestBPSIBatch(String endPointUrl, String schemaName, int dataType, List<String> messages, String sourceIdPath) {
		
		// "com.scb.bpsi.schema.cc.legalentity"
		AtomicInteger counter = new AtomicInteger(1);
		List<String> responses = new ArrayList<>();
		List<PayloadRest> payloads = new ArrayList<>();
		
		// Monitor the metrics
		IngestMetric eventMetrics = IngestMetric.getInstance();
		eventMetrics.setNumOfEvents(messages.size());
		eventMetrics.setMessageByteSize(messages.get(0).getBytes().length);
	
		
		messages.forEach(
					message -> {
						
						PayloadRest payload = new PayloadRest("cheers"+ counter.getAndIncrement() + System.currentTimeMillis(), schemaName, message, dataType, sourceIdPath);
						payloads.add(payload);
						payload =  null;
					}
				);
	
		responses.add(ingestBPSIPut(endPointUrl, payloads).getBody());
	
		// Stop the clock
		eventMetrics.stopClock();
		return responses;
	}
	
	
	public static HttpResponse<String> ingestBPSIPut(String endPointUrl, List<PayloadRest> payloads) {
		
		log.info("https PUT batch requested");
		//log.info("payload batch: "+payloads.toString());
		
		//System.out.println("INFO: URI="+endPointUrl);
		//System.out.println("Body:"+payloads.toString());
		//return "testing batch mode";
		//Unirest.setTimeouts(0, 0);
		
		HttpResponse<String> response = null;
		
		
		try {
			   response = Unirest.put(endPointUrl)
	   	      .header("Authorization", "Bearer " + config.getKeyProperties("authToken"))
			  .header("Content-Type", "application/json")
			  .body(payloads.toString())
			  .asString();
			  return response;
			   
		} catch (UnirestException e) {
			log.error("Unsuccessful BPSI PUT request.");
			log.error(e.getStackTrace().toString());
			return null;
		}
		
	}
	
	
	public static HttpResponse<String> ingestBPSIPut(String endPointUrl, PayloadRest payload) {
		
		log.info("https PUT batch requested");
		//log.info("payload message: "+payload.toString());
		//System.out.println("INFO: URI="+endPointUrl);
		//System.out.println("Body:"+payload.toString());
		
		//Unirest.setTimeouts(0, 0);
		
		HttpResponse<String> response = null;
	
		
		try {
			   response = Unirest.put(endPointUrl)
	   	      .header("Authorization", "Bearer " + config.getKeyProperties("authToken"))
			  .header("Content-Type", "application/json")
			  .body(payload.toString())
			  .asString();
			   return response;
			   
		} catch (UnirestException e) {
			log.error("Unsuccessful BPSI PUT request.");
			log.error(e.getStackTrace().toString());
			return null;
		}
		
	}
	
	
	public static HttpResponse<String> ingestBPSIPut(String endPointUrl, String payload) {
		
		//System.out.println("INFO: https request with string payload");
		//System.out.println("INFO: URI="+endPointUrl);
		
		
		HttpResponse<String> response=null;
		try {
			response = Unirest.put(endPointUrl)
			  .header("Authorization", "Bearer " + config.getKeyProperties("authToken"))
			  .header("Content-Type", "application/json")
			  .body(payload)
			  .asString();
		} catch (UnirestException e) {
			log.error("Unsuccessful BPSI PUT request.");
			log.error(e.getStackTrace().toString());
			return null;
			
		}
		return response;
	}
	
	
	public static List<String> ingestRestMessages(String endPointUrl, int numOfEvents, String schemaName, FileFormatType fileFormatType, String sourceIdPath) {
		return IngestRest.ingestBPSI(
				  endPointUrl
				, schemaName
				, FileFormatEnumMap.fileFormatTypeMap.get(fileFormatType)
				, AvroDataIO.decodeDomainMessages(numOfEvents, schemaName, fileFormatType)
				, sourceIdPath
				);
	}
	public static List<String> ingestRestMessagesBatch(String endPointUrl, int numOfEvents, String schemaName, FileFormatType fileFormatType, String sourceIdPath) {
		return IngestRest.ingestBPSIBatch(
				  endPointUrl
				, schemaName
				, FileFormatEnumMap.fileFormatTypeMap.get(fileFormatType)
				, AvroDataIO.decodeDomainMessages(numOfEvents, schemaName, fileFormatType)
				, sourceIdPath
				); 
	}
	
	public static HttpResponse<String> bpsiRestPost(String endPointUrl, String payload) {
		
		log.info("https post method");
		log.info("https request to "+endPointUrl);
		//log.info("payload => "+ payload.toString());
	
		
		HttpResponse<String> response = null;
		
		try {
			   response = Unirest.post(endPointUrl)
	   	      .header("Authorization", "Bearer " + config.getKeyProperties("authToken"))
			  .header("Content-Type", "application/json")
			  .body(payload.toString())
			  .asString();
			  return response;
			   
		} catch (UnirestException e) {
			System.err.println("ERROR: Unsuccessful BPSI Rest Post request.");
			e.printStackTrace();
			return null;
		}
	}


	public static HttpResponse<String> bpsiRestPut(String endPointUrl, String payload) {
	
	log.info("https put method");
	log.info("https request to "+endPointUrl);
	log.info("payload =>\n "+payload);
	
		HttpResponse<String> response = null;
		
		try {
			   response = Unirest.put(endPointUrl)
	   	      .header("Authorization", "Bearer " + config.getKeyProperties("authToken"))
			  .header("Content-Type", "application/json")
			  .body(payload.toString())
			  .asString();
			  return response;
			   
		} catch (UnirestException e) {
			log.error("Unsuccessful BPSI PUT request.");
			log.error(e.getStackTrace().toString());
			return null;
		}
	}
	
	public static HttpResponse<String> bpsiRestGet(String endPointUrl) {
		
		HttpResponse<String> response = null;
		
		try {
			
			response = Unirest.get(endPointUrl)
					  .header("Authorization", "Bearer " + config.getKeyProperties("authToken"))
					  .asString(); 
			  return response;
			   
		} catch (UnirestException e) {
			log.error("Unsuccessful BPSI GET request.");
			log.error(e.getStackTrace().toString());
			return null;
		}
		
	}



}