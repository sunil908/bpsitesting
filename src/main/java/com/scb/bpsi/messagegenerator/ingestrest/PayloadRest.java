package com.scb.bpsi.messagegenerator.ingestrest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PayloadRest {
	
	private Map<String, Object> payLoadMap= new HashMap<>();
	private Map<String,Object> metaData = new HashMap<>();
	
	public PayloadRest(String correlationId, String schemaName,String data, int dataType, String sourceIdPath) {
	
	// TODO : lookup to the schema registry to find the schemaId
	
		
		/* Interface Definition as per the API Doc
		 * 
		 *  
		{
			"correlationId":"string",
			"payloadRest":{
					"data":"string",
					"entityId":"string",
					"entityName":"string"
			}
			} */
		
		ObjectMapper mapper = new ObjectMapper();
	    JsonFactory factory = mapper.getFactory();
	    JsonParser parser;
	    String sourceId = null;
		try {
			
			parser = factory.createParser(data);
			JsonNode jsonData = mapper.readTree(parser);
			sourceId = jsonData.at("/" + sourceIdPath.replace(".", "/")).asText();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
		ObjectNode emptyNode = mapper.createObjectNode();
	
		////////// Building the payloadRest //////////
		//metaData.put("correlationId",correlationId);
		metaData.put("contributor", "SampleIngestSunil");
		metaData.put("contributorRef", "ITAM-SampleIngestSunilApp");
		metaData.put("dateMeta",emptyNode);
		metaData.put("intMeta",emptyNode);
		metaData.put("numberMeta",emptyNode);
		metaData.put("textMeta",emptyNode);
		metaData.put("sourceId", sourceId);

		// Removed due to interface not having an option right now
		//payloadRest.put("dataType", dataType);
		
		////////// Building the Body //////////
		payLoadMap.put("data",data);
		payLoadMap.put("schemaId", schemaName);
		payLoadMap.put("metaData", metaData);
	}
	
	
	@Override
	public String toString() {
		ObjectMapper mapperObj = new ObjectMapper();
		String payLoad = null;
		try {
			payLoad = mapperObj.writeValueAsString(this.payLoadMap);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		//System.out.println("Payload:" + payLoad);
		return payLoad;
	}
	
}