package com.scb.bpsi.messagegenerator.model.demo;

public class UserTest {
	public String userName;
	public String userDepartment;
	public String[] subjects = {"Test1","Test2","Test3"};
		
	public UserTest(String userName, String userDepartment, String[] subjects ){
	this.userName = userName;
	this.userDepartment = userDepartment;
	this.subjects = subjects;
}
}