package com.scb.bpsi.messagegenerator.ingestgrpc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.scb.bpsi.ingest.client.BPSIIngestClient;
import com.scb.bpsi.messagegenerator.avro.schema.util.AvroDataIO;
import com.scb.bpsi.messagegenerator.enums.FileFormatEnumMap;
import com.scb.bpsi.messagegenerator.enums.FileFormatType;
import com.scb.bpsi.messagegenerator.event.IngestMetric;
import com.scb.bpsi.proto.ingest.Event;
import com.scb.bpsi.proto.ingest.EventResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IngestGrpc {

	public static List<String> ingestEvents(BPSIIngestClient client, int numOfEvents, String schemaName, FileFormatType fileFormatType, String sourceIdPath) {
		return IngestGrpc.ingestBPSI(
				  client
				, schemaName
				, FileFormatEnumMap.fileFormatTypeMap.get(fileFormatType)
				, AvroDataIO.decodeDomainMessages(numOfEvents, schemaName, fileFormatType)
				, sourceIdPath
				); 
	}

	
	public static List<String> ingestBPSI(BPSIIngestClient client, String schemaName, int dataType, List<String> messages, String sourceIdPath) {
	
		AtomicInteger counter = new AtomicInteger(1);
		List<String> responses = new ArrayList<>();
		
		// Monitor the metrics
		IngestMetric eventMetrics = IngestMetric.getInstance();
		eventMetrics.setNumOfEvents(messages.size());
		eventMetrics.setMessageByteSize(messages.get(0).getBytes().length);
		
		messages.forEach(
					message -> {
						PayloadGrpc payload = new PayloadGrpc("cheers"+ counter.getAndIncrement() + System.currentTimeMillis(), schemaName, message, dataType, sourceIdPath);
						responses.add(ingestBPSI(client,payload.getEvent()));
						payload =  null;
					}
				);
		
		eventMetrics.stopClock();
		return responses;
		
	}


	public static String ingestBPSI(BPSIIngestClient client, Event payload) {
	
		log.info("Ingest via grpc client");
		log.info("Event:" + payload.toString());
		
		EventResponse response = client.sendEvent(payload);
	    //System.out.println("Result of sendEvent is [" + response + "]");
	    return response.toString();
	}


}