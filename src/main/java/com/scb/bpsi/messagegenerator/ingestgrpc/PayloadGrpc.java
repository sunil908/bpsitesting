package com.scb.bpsi.messagegenerator.ingestgrpc;



import com.google.protobuf.ByteString;
import com.scb.bpsi.proto.ingest.Event;
import com.scb.bpsi.proto.ingest.Payload;
import com.scb.bpsi.services.engines.common.DateTimeWithZone;
import com.scb.bpsi.services.engines.common.MetaData;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public class PayloadGrpc {
	
	
	public class NumberMeta {
		String keyAttribute;
		
	}
	
	
	
	private Event event;
	
		public PayloadGrpc(String correlationId, String schemaName,String data, int dataType, String sourceIdPath) throws IllegalArgumentException{
		
			
		log.info("Calling: payload with sourceIdPath");
		Map<String,Object> metaData = new HashMap<>();		
			
		////////// Building the grpc payloadRest //////////
		
		String sourceId = null;
		ObjectMapper mapper = new ObjectMapper();
	    JsonFactory factory = mapper.getFactory();
	    JsonParser parser;
	    
		try {
			
			parser = factory.createParser(data);
			JsonNode jsonData = mapper.readTree(parser);
			sourceId = jsonData.at("/" + sourceIdPath.replace(".", "/")).asText();
			
		} catch (JsonProcessingException e) {
			log.error("ERROR: Unable to parse the SourceId Path");
			
		} catch (IOException e) {
			log.error("ERROR: Unable to parse the SourceId Path");
		}
		
		
		// TODO : lookup to the schema registry to find the schemaId
		
		Payload payloadData = Payload.newBuilder()
        		.setCorrelationId(correlationId)
                .setDataPayload(ByteString.copyFrom(data.getBytes()))
                .build();
		
		int emptyArray[]={};

		//metaData.put("correlationId",correlationId);
		
        // Build the date meta
		Map<String, DateTimeWithZone> dateMeta = new HashMap<>();
        dateMeta.put("createDateTimeStamp", DateTimeWithZone.newBuilder().setInstantMillis(System.currentTimeMillis()).setZoneIdCode("Z").build());

        // Build the text meta
        Map<String, String> textMeta = new HashMap<>();
        textMeta.put(sourceIdPath, sourceId);
        
        // Others will point to empty values
		metaData.put("intMeta",emptyArray);
		
		metaData.put("numberMeta",emptyArray);
		
		this.event = Event.newBuilder()
                .setMeta(MetaData.newBuilder()
                        .setDomainUniqueIdentifier(sourceId)
                        .setKvpairs(
                                MetaData.KVPairs.newBuilder()
                                        .putAllMapOfStrings(textMeta)
                                        .putAllMapOfDates(dateMeta)
                                        .build()
                        )
                        .build())
                .setSchemaId (schemaName)
                .setData(payloadData)
                .build();
	}
		
		public PayloadGrpc(String correlationId, String schemaName,String data, String metaDataJson) throws IllegalArgumentException, JsonMappingException, JsonProcessingException {

			log.debug("Calling: payload with meta only");
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);
			
			ObjectNode metaDataNode = null;

			
			if(!metaDataJson.isEmpty()){
				metaDataNode = (ObjectNode)mapper.readTree(metaDataJson);
			}
			
			Map<String, String> textMeta = null;
			Map<String, DateTimeWithZone> dateMeta = null;
			Map<String, Long> intMeta = null;
			Map<String, Double> numberMeta = null;
			
			String sourceId = null;
			String contributor = null;
			String contributorRef = null;
			
			// Extract the meta tags from the json config
			
			try {
				
				// TODO: Unable to read the DateTimeZone map. Deserializer missing from the class
				// dateMeta =	mapper.convertValue(metaDataNode.get("dateMeta"), new TypeReference<List<Map<String,DateTimeWithZone>>>(){});
				sourceId = metaDataNode.get("sourceId").asText();
				contributor = metaDataNode.get("contributor").asText();
				contributorRef = metaDataNode.get("contributorRef").asText();
				numberMeta =	mapper.convertValue(metaDataNode.get("numberMeta"), new TypeReference<Map<String,Double>>(){});
				textMeta =	mapper.convertValue(metaDataNode.get("textMeta"), new TypeReference<Map<String,String>>(){});
				intMeta =	mapper.convertValue(metaDataNode.get("intMeta"), new TypeReference<Map<String,Long>>(){});
			}
			catch (IllegalArgumentException e) {
				log.error("Exception found in reading parameters");
				throw new IllegalArgumentException();
			}
	

			Payload payloadData = Payload.newBuilder()
	        		.setCorrelationId(correlationId)
	                .setDataPayload(ByteString.copyFrom(data.getBytes()))
	                .build();
		
			//System.out.println("textMeta:"+ textMetaResult);
			//System.out.println("intMeta:"+ intMetaResult);
			//System.out.println("numberMeta:"+ numberMetaResult);
			
			this.event = Event.newBuilder()
	                .setMeta(MetaData.newBuilder()
	                        .setDomainUniqueIdentifier(sourceId)
	                        .setContributor(contributor)
	                        .setContributorRef(contributorRef)
	                        .setKvpairs(
	                                MetaData.KVPairs.newBuilder()
	                                        .putAllMapOfStrings(textMeta)
	                                        .putAllMapOfDates(Collections.emptyMap())
	                                        .putAllMapOfIntegers(intMeta)
	                                        .putAllMapOfNumbers(numberMeta)
	                                        .build()
	                        )
	                        .build())
	                .setSchemaId (schemaName)
	                .setData(payloadData)
	                .build();
		
		}
		
	
	public PayloadGrpc(String correlationId, String schemaName,Payload payload, MetaData metaData) {
			
			this.event = Event.newBuilder()
	                .setMeta(metaData)
	                .setSchemaId (schemaName)
	                .setData(payload)
	                .build();
		}
		
		
	@Override
	public String toString() {
		ObjectMapper mapperObj = new ObjectMapper();
		String payLoadGrpc = null;
		try {
			payLoadGrpc = mapperObj.writeValueAsString(this.event);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return payLoadGrpc;
	}
	
}