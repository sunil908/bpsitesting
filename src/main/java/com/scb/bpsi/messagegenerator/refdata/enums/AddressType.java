package com.scb.bpsi.messagegenerator.refdata.enums;

public enum AddressType {
 OFFICIAL, LOCAL, OPERATION, HEADOFFICE
}
