package com.scb.bpsi.messagegenerator.refdata.enums;

public enum OrganizationUnitType {
ORGANIZATION, INDIVIDUAL
}
