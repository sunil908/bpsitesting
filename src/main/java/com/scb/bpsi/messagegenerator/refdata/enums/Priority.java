package com.scb.bpsi.messagegenerator.refdata.enums;

public enum Priority {
 HIGH, MEDIUM, LOW, VERY_LOW
}
