package com.scb.bpsi.messagegenerator.refdata.enums;

public enum WorkflowStatus {
APPROVE, PENDING, CLOSED, REJECTED, VALIDATION
}
