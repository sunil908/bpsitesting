package com.scb.bpsi.messagegenerator.refdata.enums;

public enum SegmentCode {
 SEGMENT1, SEGMENT2, SEGMENT3, SEGMENT4;
}
