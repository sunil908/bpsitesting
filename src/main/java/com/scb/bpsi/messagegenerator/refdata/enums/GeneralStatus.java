package com.scb.bpsi.messagegenerator.refdata.enums;

public enum GeneralStatus {
 ACTIVE, INACTIVE, PENDING
}
