package com.scb.bpsi.messagegenerator.fileutil;
 
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.mortbay.log.Log;

import com.scb.bpsi.messagegenerator.avro.schema.util.AvroDataIO;
import com.scb.bpsi.messagegenerator.enums.FileFormatEnumMap;
import com.scb.bpsi.messagegenerator.enums.FileFormatType;
import com.scb.bpsi.messagegenerator.event.IngestMetric;
import com.scb.bpsi.messagegenerator.ingestrest.PayloadRest;


public class FileWriter {

	public static String writeToFile(Path path, String message) throws IOException {
		Files.write(path,message.getBytes());
		return "{\"result\":\"success\",\"message\":\"payload written to " + path.toString() + "\"}";
	}
	
	public static List<String> writeToFile(FileFormatType fileFormatType, int numOfEvents, String fileLocation, String schemaName, String sourceIdPath) {
		
		AtomicInteger counter = new AtomicInteger(1);
		
		List<String> messages = AvroDataIO.decodeDomainMessages(numOfEvents, schemaName, fileFormatType);
		List<String> responses = new ArrayList<>();
		 
		// Monitor the metrics
		IngestMetric eventMetrics = IngestMetric.getInstance();
		eventMetrics.setNumOfEvents(numOfEvents);
		
		Random rand = new Random();
		
		messages.forEach(
				message -> {
					PayloadRest payload = new PayloadRest("cheers"+ counter.getAndIncrement() + "_" + rand.nextLong() + System.currentTimeMillis(), schemaName, message, FileFormatEnumMap.fileFormatTypeMap.get(fileFormatType), sourceIdPath);
					try { 
						responses.add(writeToFile(Paths.get(fileLocation + "\\" + schemaName + "_" + rand.nextLong() + System.currentTimeMillis() + counter.getAndIncrement() + "." + fileFormatType.toString().toLowerCase()),payload.toString()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					payload =  null;
				}
			);

		// Stop the clock
		eventMetrics.stopClock();	
		Log.info("\nWritten {} files to {}",messages.size(), fileLocation);
		return responses;
	}
	
	
}
