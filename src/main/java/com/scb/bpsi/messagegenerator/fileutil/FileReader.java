package com.scb.bpsi.messagegenerator.fileutil;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;



public class FileReader {

	public static JsonNode readJsonFile(String fileName, Class clazz) throws IOException	 {
		
		InputStream inputDataStream = clazz.getClass().getResourceAsStream(fileName);
		if( inputDataStream == null) {
			throw new IOException("Resoource not found: " + fileName);
		}
		
	    String bpsiProperties = new BufferedReader(new InputStreamReader(inputDataStream, StandardCharsets.UTF_8))
	      .lines()
	      .collect(Collectors.joining("\n"));
	 
	    //System.out.println("Properties Read....\n" + bpsiProperties);
	     
		ObjectMapper mapper = new ObjectMapper();
		assertNotNull(mapper);
		
		return mapper.readTree(bpsiProperties);
		
		}
	
	
	public static JsonNode readJsonFile(String fileName) throws IOException	 {
		
		InputStream inputDataStream = new FileInputStream(new File(fileName));
		String bpsiProperties = new BufferedReader(new InputStreamReader(inputDataStream, StandardCharsets.UTF_8))
	      .lines()
	      .collect(Collectors.joining("\n"));
	 
	    
	     
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.readTree(bpsiProperties);
		
		}
	
		
}	  

	
