
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.scb.bpsi.config.Configuration;
import com.scb.bpsi.ingest.client.BPSIIngestClient;
import com.scb.bpsi.messagegenerator.enums.*;
import com.scb.bpsi.messagegenerator.event.*;
import com.scb.bpsi.messagegenerator.fileutil.*;
import com.scb.bpsi.messagegenerator.ingestgrpc.IngestGrpc;
import com.scb.bpsi.messagegenerator.ingestrest.*;

//Dependencies for the test schemas
import com.scb.bpsi.schema.H05Gs6Qgflop0dWD.eventgen.EventsGeneratorH05Gs6Qgflop0dWD;
import com.scb.bpsi.schema.cc.legalentity.v4.eventgen.*;
import com.scb.bpsi.schema.companies.eventgen.EventsGeneratorCompanies;
import com.scb.bpsi.schema.iAKs08f34ZU8eVUN.eventgen.EventsGeneratoriAKs08f34ZU8eVUN;
import com.scb.bpsi.schema.vega.legalentitydetails.eventgen.*;
import com.scb.bpsi.schema.vt.company.eventgen.*;

import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Parameters(
		commandNames = {"gen-event" },
		commandDescription = "Running test event generator")
public class MessageGenerator
{

	
	@Parameter(names = "-debug", description = "Debug mode")
	private static boolean debug = false;

	@Parameter(names = "--help", help = true)
	private static boolean help = false;
	
	
	@Parameter(
			  names = { "--schema"},
			  description = "Choose schemaId for whom events is to be generated ",
			  required = true
			)
	private static String schemaName;
	
	
	@Parameter(
			  names = { "--filetype"},
			  description = "Choose file format type for which events are to be generated (xml/json)",
			  required = true
			)
	private static String fileFormatType;
	
	@Parameter(
			  names = { "--num"},
			  description = "Number of events to be generated",
			  required = true
			)
	private static int numOfEvents;

	@Parameter(
			  names = { "--targettype"},
			  description = "Provide target FILE, CLIENT, URI",
			  required = true
			)
	private static String targetType;
	
	@Parameter(
			  names = { "--target"},
			  description = "Provide target File location or hostname or rest URI",
			  required = true
			)
	private static String targetLocation;
	
	
	@Parameter(
			  names = { "--sourceIdPath"},
			  description = "Provide target json path using json dot notation.",
			  required = true
			)
	private static String sourceIdPath;
	
	
	@Parameter(
			  names = { "--threadCount"},
			  description = "Provide number of threads to run for ingestion.",
			  required = false
			)
	private static int threadCount=1;
	
	
	public static void runMessageGenerator() throws InterruptedException, IOException, RestClientException
    {

		/**
		 *  ********************************
		 *  REGISTER YOUR SCHEMA FOR TESTING
		 *  *********************************
		 *  
		 *  Two classes to be implemented for each schema 
		 *  Refer example : com.scb.bpsi.schema.vt.company & com.bpsi.schema.vt.company.eventgen  
		 */
		
		/**
		 * Demo 1 : Full client central message
		 * --filetype json --schema com.scb.bpsi.cc.legalentity --num 1 --targettype uri  --target http://ec2-54-169-229-117.ap-southeast-1.compute.amazonaws.com:32323/v1/ingest
		 * --filetype json --schema com.scb.bpsi.cc.legalentity --num 1 --targettype file --target d:\json_output
		 * 
		 * Demo 2 : Header Schema only
		 * --filetype json --schema com.scb.bpsi.schema.cc.legalentity.HeaderMessage --num 1 --targettype file --target d:\json_output
		 *  
		 * Demo 3 : Generate file 
		 * --filetype json --schema g0b1zm3azanzqjkpwz0d1w --num 1 --num 1 --targettype file --target d:\json_output
		 * 
		 * Demo 4 - Andy's example schema for VT demo
		 * --filetype json --schema wiqpJZEChkL5H3Hz --num 1 --targettype FILE --target d:\json_output --sourceIdPath company
		 * --filetype json --schema wiqpJZEChkL5H3Hz --num 1 --targettype URI --target http://ec2-54-169-229-117.ap-southeast-1.compute.amazonaws.com:32323/v1/ingest --sourceIdPath company
		 * --filetype json --schema wiqpJZEChkL5H3Hz --num 1 --targettype CLIENT --target ec2-54-169-229-117.ap-southeast-1.compute.amazonaws.com:32253 --sourceIdPath company
		 * 
		 * Demo 5 - legalentity master with legalentityid as meta
		 * --filetype json --schema d4jqNZTcdGvzXp7d --num 1 --targettype FILE --target d:\json_output --sourceIdPath LegalEntityID
		 * --filetype json --schema d4jqNZTcdGvzXp7d --num 1 --targettype CLIENT --target ec2-54-169-229-117.ap-southeast-1.compute.amazonaws.com:32253 --sourceIdPath LegalEntityID
		 * --filetype json --schema d4jqNZTcdGvzXp7d --num 1 --targettype URI --target http://ec2-54-169-229-117.ap-southeast-1.compute.amazonaws.com:32323/v1/ingest --sourceIdPath LegalEntityID
		 */
		Configuration config = Configuration.getInstance();
		
		config.addKeyProperties("schemaName", schemaName);
		config.addKeyProperties("filetype", fileFormatType);
		config.addKeyProperties("numOfEvents", numOfEvents);
		config.addKeyProperties("targetType", targetType);
		config.addKeyProperties("sourceIdPath", sourceIdPath);
		config.addKeyProperties("targetLocation", targetLocation);

		
		EventsProductionFactory.register("b89DLic2mP9bzlwP", new EventsGeneratorv4());	
		EventsProductionFactory.register("wiqpJZEChkL5H3Hz", new EventsGeneratorCompany());
		EventsProductionFactory.register("d4jqNZTcdGvzXp7d", new EventsGeneratorLegalEntityDetails());
		EventsProductionFactory.register("com.scb.bpsi.cc.legalentity", new EventsGeneratorv4());
		EventsProductionFactory.register("H05Gs6Qgflop0dWD", new EventsGeneratorH05Gs6Qgflop0dWD());
		EventsProductionFactory.register("aBQ7AeDfVaM3O3KT", new EventsGeneratoriAKs08f34ZU8eVUN());
		EventsProductionFactory.register("z0wz2IgQIjlvqAj2", new EventsGeneratorLegalEntityDetails());
		EventsProductionFactory.register("cILj1JWEza6GcOc8", new EventsGeneratorLegalEntityDetails());
		EventsProductionFactory.register("syw2aocmJVlPwZ3z", new EventsGeneratorCompanies());
		
		
		List<String> responses = new CopyOnWriteArrayList<>();
		
		
		Runnable r = () ->  {
			 try {
				responses.addAll(writeEvents(config));
			} catch (IOException e) {
				e.printStackTrace();
			}	
			System.out.println("Running thread " + Thread.currentThread().getName());
		};
		
		Thread[] threads = new Thread[threadCount];
		
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(r, "t"+i);
			threads[i].start();
		}
			
		for (int i = 0; i < threads.length; i++) {
			threads[i].join();
		}

		log.info("\n====================================== Test Message Generator ======================================");

		responses.forEach(msg -> log.info("Response received: "+msg.toString()));
		
		IngestMetric eventPerformance = IngestMetric.getInstance();
		eventPerformance.printSummary();

		
    }

	public static boolean isPathValid(String path) {

        try {
            Paths.get(path);

        } catch (InvalidPathException ex) {
            return false;
        }

        return true;
    }
	
	public static TargetType getIngestType() {
		
		switch(targetType.toUpperCase()) {
		case "FILE": 
					return TargetType.WRITEFILE;
		case "URI":
					return TargetType.RESTURI;
		case "RESTBATCH":
			return TargetType.RESTBATCH;
		case "CLIENT":
					return TargetType.CLIENT;			
		default:
				 	return null;
		}
		
	}
	
	public static List<String> writeEvents(Configuration config) throws IOException {
		
		switch(getIngestType()) {
		
		case  RESTURI :   return IngestRest.ingestRestMessages(
				   				  	targetLocation
				   				   ,numOfEvents
				   				   ,schemaName
				   				   ,FileFormatType.valueOf(fileFormatType.toUpperCase())
				   				   ,sourceIdPath
						   				);
		
		case  CLIENT : 		BPSIIngestClient client = new BPSIIngestClient(targetLocation, config.getKeyProperties("certLoc"));
							return IngestGrpc.ingestEvents(client, numOfEvents, schemaName , FileFormatType.JSON, sourceIdPath);
							
		case  WRITEFILE :	return FileWriter.writeToFile(
									FileFormatType.valueOf(fileFormatType.toUpperCase())
				   				   ,numOfEvents
				   				   ,targetLocation
				   				   ,schemaName
				   				   ,sourceIdPath
				   				);
		case RESTBATCH:  return IngestRest.ingestRestMessagesBatch(
				    			targetLocation
				    		   ,numOfEvents
							   ,schemaName
							   ,FileFormatType.valueOf(fileFormatType.toUpperCase())
							   ,sourceIdPath
								);
		default: 
							return null;
		}
		
	}
	
 
}

